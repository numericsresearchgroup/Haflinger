@p "projectname" => "convtest"

@p "N" => 5
@p "nodetype" => "gauss-lobatto"
@p "meshmode" => 1
@p "meshfile" => "./examples/CART_HEX_PERIODIC_004_mesh.h5"


# TODO: do we want to have named refstates?
# BCs would then map to named state instead of number,
# which would increase readability
@p "iniexactfunc" => 2
@p "inirefstate" => [ 1  , 0.3, 0, 0, 0.71428571 ]
@p "refstates"   => [
                      1    0.3  0  0  0.74;
                      1.1  0.3  0  0  0.74
                    ]

@p "mu0" => 0.00
#@p "bcs" => [
#             ("bc_z" ,[2,1]),
#             ("bc_y" ,[2,1]),
#            ]

@p "riemann" => "lf"
@p "riemannbc" => "lf"
@p "tend" => 10.0
@p "tanalyze" => 2.50

@p "cflscale" => 0.5
@p "dflscale" => 0.3
