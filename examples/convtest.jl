@p "projectname" => "convtest"

@p "N" => 5
@p "nodetype" => "gauss"
@p "nelemsxyz" => [4,4,4]


# TODO: do we want to have named refstates?
# BCs would then map to named state instead of number,
# which would increase readability
@p "iniexactfunc" => 1
@p "inirefstate" => [ 1  , 0.3, 0, 0, 0.71428571 ]
@p "refstates"   => [ 
                      1    0.3  0  0  0.74;
                      1.1  0.3  0  0  0.74  
                    ]

#@p "bcs" => [
#             ("inflow" ,(2,1)),
#             ("outflow",(2,1)),
#             ("wall"   ,(4,1))
#            ]

@p "riemann" => "lf"
@p "riemannbc" => "lf"
@p "tend" => 1.0
@p "tanalyze" => 0.01

@p "cflscale" => 0.5
@p "dflscale" => 0.5
