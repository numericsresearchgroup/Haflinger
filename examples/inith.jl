include("../src/Haflinger.jl")

using .Haflinger
using BenchmarkTools

initsolver("./convtest.jl","")

#using Haflinger.DG
#using Haflinger.Mesh
#using Haflinger.Global
#using Haflinger.Interpolation
#using Haflinger.Parallel
#using Haflinger.Equation.EquationSystem
#using Haflinger.Equation.EquationSystem.EOS
#using Haflinger.Timedisc
#import Haflinger.DG: fillfluxsurf!
#const NVAR=5
#
#dofs=(5,5,5,64)
#co1,pr1,f,g,h,fx1,fy1,fz1,mf1,mg1,mh1=rand(5,dofs...),rand(6,dofs...),rand(5,dofs...),rand(5,dofs...),rand(5,dofs...),rand(5,dofs...),rand(5,dofs...),rand(5,dofs...),rand(3,dofs...),rand(3,dofs...),rand(3,dofs...)
#
#fl=zeros(NVAR,3)
#m=zeros(3,3)
