using MPI

function sendi(a)
  MPI.Isend(a, 1, 66, comm)
end

function recvi(a)
  MPI.Irecv!(a, 0, 66, comm)
end

const nval = 25
sendarr =  [ i for i=1:45 ]
recvarr =  [ i for i=2:46 ]

MPI.Init()
comm = MPI.COMM_WORLD
size = MPI.Comm_size(comm)
rank = MPI.Comm_rank(comm)

@views begin
  if rank == 0
    req = MPI.Isend(sendarr[1:10], 10, 1, 66, comm)
  else
    req = MPI.Irecv!(recvarr[11:20], 10, 0, 66, comm)
  end
end

stats = MPI.Waitall!([req])

if rank == 1
  print(recvarr)
end

MPI.Finalize()

