files=`find $1 -type f -iname '*.f90'`

for file in $files; do
  sed -i '/^! modules/Id' "$file"
  sed -i '/^!----*/Id' "$file"
  sed -i '/^! input \/ output variables/Id' "$file"
  sed -i '/^! input\/output variables/Id' "$file"
  sed -i '/^! local variables/Id' "$file"
  sed -i 's/!===*/\"\"\"/Ig' "$file"
  sed -i 's/^!> //Ig' "$file"
  sed -i 's/^!>//Ig' "$file"
  sed -i 's/^!/#/Ig' "$file"
  sed -i 's/ !/ #/Ig' "$file"
  sed -i "s/'/bashescapesucks/Ig" "$file"
  sed -i 's/bashescapesucks/\"/Ig' "$file"

  sed -i 's/#ifdef/if/Ig' "$file"
  sed -i 's/^#if/if/Ig' "$file"
  sed -i 's/^#else/else/Ig' "$file"
  sed -i 's/#endif/end/Ig' "$file"

  sed -i 's/enddo/end/Ig' "$file"
  sed -i 's/end do/end/Ig' "$file"
  sed -i 's/endif/end/Ig' "$file"
  sed -i 's/end if/end/Ig' "$file"
  sed -i 's/if (/if /Ig' "$file"
  sed -i 's/if(/if /Ig' "$file"
  sed -i 's/) then//Ig' "$file"
  sed -i 's/)then//Ig' "$file"
  sed -i 's/CALL //g' "$file"

  sed -i 's/\.true\./true/Ig' "$file"
  sed -i 's/\.false\./false/Ig' "$file"
  sed -i 's/\.gt\./ > /Ig' "$file"
  sed -i 's/\.lt\./ < /Ig' "$file"
  sed -i 's/\.eq\./ == /Ig' "$file"
  sed -i 's/\.le\./ \\le /Ig' "$file"
  sed -i 's/\.ge\./ \\ge /Ig' "$file"
  sed -i 's/\.not\./ !/Ig' "$file"
  sed -i 's/\&//Ig' "$file"
  sed -i 's/\.and\./ \&\& /Ig' "$file"
  sed -i 's/\.or\./ || /Ig' "$file"
  sed -i 's/\.ne\./ \\ne /Ig' "$file"

  sed -i 's/DO /for /g' "$file"
  sed -i 's/0,pp_n/1:n/Ig' "$file"

  sed -i 's/end subroutine.*/end/Ig' "$file"
  sed -i 's/end function.*/end/Ig' "$file"
  sed -i 's/end module.*/end/Ig' "$file"

  sed -i 's/subroutine/function/Ig' "$file"
  sed -i 's/module mod_/module /Ig' "$file"

  sed -i 's/swrite(unit_stdout,/print(/Ig' "$file"
  sed -i 's/write(unit_stdout,/print(/Ig' "$file"
  sed -i 's/write(\*,\*)/print("/Ig' "$file"

  sed -i 's/(\//[/Ig' "$file"
  sed -i 's/\/)/]/Ig' "$file"

  sed -i 's/%/\./Ig' "$file"

  sed -i '/implicit none/Id' "$file"
  sed -i '/^private/Id' "$file"
  sed -i '/^save/Id' "$file"
  sed -i '/^use mod_/Id' "$file"
  sed -i '/^interface /Id' "$file"
  sed -i '/end interface/Id' "$file"
  sed -i '/ module procedure/Id' "$file"
  sed -i '/PUBLIC::/Id' "$file"
  sed -i '/PUBLIC ::/Id' "$file"

  sed -i 's/integer,parameter ::/const/Ig' "$file"

  sed -i 's/getlogical(/getparameter(params,/Ig' "$file"
  sed -i 's/getintarray(/getparameter(params,/Ig' "$file"
  sed -i 's/getint(/getparameter(params,/Ig' "$file"
  sed -i 's/getrealarray(/getparameter(params,/Ig' "$file"
  sed -i 's/getreal(/getparameter(params,/Ig' "$file"

  sed -i 's/\/\//\*/Ig' "$file"
  sed -i 's/(:)//Ig' "$file"
  sed -i 's/(:,:)//Ig' "$file"
  sed -i 's/(:,:,:)//Ig' "$file"
  sed -i 's/(:,:,:,:)//Ig' "$file"
  sed -i 's/(:,:,:,:,:)//Ig' "$file"

  sed -i 's/(:/[:/Ig' "$file"
  sed -i 's/trim(//Ig' "$file"
  sed -i 's/\*\*/^/Ig' "$file"
done


