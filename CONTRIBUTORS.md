# Contributing to Haflinger

By contributing code, documentation or test cases to Haflinger, you are agreeing
to release it under the [MIT License](LICENSE.md)

# List of Contributors

This is a (possibly incomplete) list of the people who contributed to Haflinger
in chronological order:

* Thomas Bolemann (2018 - 2019)
* Gregor Gassner (2018 - 2019)
* Alexander Astanin (2018)
