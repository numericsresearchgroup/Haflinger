@reexport module FillParameter

using Crayons.Box
using ..Haflinger

export getparameter

const linelength   = 80
const maxkeylength = 30

"""
Safely retrieves parameter from dict and prints
"""
function getparameter(param::Dict, key::String; default = nothing, required::Bool = false)
    found = haskey(param, key)
    if !found && default == nothing
        str = "Key " * key * " does not exist"
        required ? (@error str) : (@warn str)
    end
    p = get(param, key, default)
    if found && default ≠ nothing
        if isa(default, AbstractArray) && isa(p, AbstractArray)
            @assert eltype(p) <: eltype(default) "Parameter has type $(eltype(p)), expected $(eltype(default))!"
        else
            @assert typeof(p) <: typeof(default) "Parameter has type $(typeof(p)), expected $(typeof(default))!"
        end
    end
    s = max(0,maxkeylength-length(key))
    mprint("|  ", found ? RED_FG("CUSTOM ") : GREEN_FG("DEFAULT"), "  |  ", CYAN_FG(key), " "^s, " |  $(p == nothing ? "Nothing" : p)\n")

    p
end

end
