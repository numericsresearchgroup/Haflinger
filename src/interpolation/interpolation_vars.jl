#  Use GaussQuadrature package instead of FastGaussQudarature.
#  PRO: more quadrature types, higher accuracy if BigFloats are used
#       better syntax for G/GL/GR, drastically faster startup!!!
#  CONS: slower (especially for BigFloats), less accurate baseline (Float64)
begin
  @inline gausslegendre(n)           = gausslegendre(FT,n)
  @inline gausslobatto(n)            = gausslobatto(FT,n)
  @inline gausschebyshev(n)          = gausschebyshev(FT,n)
  @inline gausschebyshevlobatto(n)   = gausschebyshevlobatto(FT,n)
  @inline gausslegendre(T,n)         = legendre( T,n,neither)
  @inline gausslobatto(T,n)          = legendre( T,n,both)
  @inline gausschebyshev(T,n)        = chebyshev(T,n,1,neither)
  @inline gausschebyshevlobatto(T,n) = chebyshev(T,n,1,both)
  #hermite, jacobi, laguerre, ...
end

# normalize legendre polynomials
nlegendre(xi, n)  = Jacobi.legendre(xi,n - 1)  * sqrt(n - 1 + 0.5)
ndlegendre(xi, n) = Jacobi.dlegendre(xi,n - 1) * sqrt(n - 1 + 0.5)

const nodetypes = Dict(
                 "gauss"                   => :nodetypeg,
                 "gauss-lobatto"           => :nodetypegl,
                 "gauss-chebyshev"         => :nodetypec,
                 "gauss-chebyshev-lobatto" => :nodetypecl,
                 "equidistant"             => :nodetypeequi,
                 "equidistant_inner"       => :nodetypeequiinner
                )

# Decision: provide nodes and weights by dict or by dispatching onto constants for performance??
const nodefuncs = Dict(
                 :nodetypeg                => gausslegendre,
                 :nodetypegl               => gausslobatto,
                 :nodetypec                => gausschebyshev,
                 :nodetypecl               => gausschebyshevlobatto,
                 :nodetypeequi             => visunodesandweights,
                 :nodetypeequiinner        => visuinnernodesandweights
                )

interpolationinitisdone  = false

export nodefuncs, nodetypes

struct IPBasis{n, T <: AbstractFloat}
  xgp::SVector{n,T}
  wgp::SVector{n,T}
  wbary::SVector{n,T}
  lminus::SVector{n,T}
  lplus::SVector{n,T}
  vdmleg::SMatrix{n,n,T}
  svdmleg::SMatrix{n,n,T}
  nodetype::Symbol
end
