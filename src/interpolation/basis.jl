export visunodesandweights, visuinnernodesandweights, barycentricweights
export lagrangeinterpolationpolys, polynomialderivativematrix, initializevandermonde

function visunodesandweights(T,n::Integer)
  xip    = collect(T(-1):(T(2)/T(n-1)):T(1))
  # trapezoidal rule for integration
  wip    = [ T(2)/T((n-1)) for i=1:n ]
  wip[1] = wip[1]/2
  wip[n] = wip[n]/2

  xip, wip
end
visunodesandweights(n::Integer) = visunodesandweights(FT,n)

function visuinnernodesandweights(T,n::Integer)
  xip    = [ T(1)/T(n)+T(2*(i-1))/T(n) - 1  for i=1:n ]
  # first order intergration
  wip    = [ T(2)/T(n)                      for i=1:n ]

  xip, wip
end
visuinnernodesandweights(n::Integer) = visuinnernodesandweights(FT,n)

"""
Computes barycentric (interpolation) weights for interpolation polynomial given by set of nodes.
"""
function barycentricweights(x::AbstractVector{F}) where F <: AbstractFloat
  1 ./ map(i -> prod(x[i] .- x[[1:i-1; i+1:end]]), 1:length(x))
end


"""
Computes all Lagrange functions evaluated at position x in [-1,1]
For details see paper Barycentric Lagrange Interpolation by Berrut and Trefethen (SIAM 2004)
Algorithm 34, Kopriva book
"""
function lagrangeinterpolationpolys(x::F,xgp::AbstractVector{F},wbary::AbstractVector{F}) where F <: AbstractFloat
  n=length(xgp)
  l=zeros(eltype(xgp),n)
  
  for igp=1:n
    # if x is equal to a gauss point, l=(0,....,1,....0)
    if isapprox(x,xgp[igp],rtol=eps(F))
      l[igp]=1
      return l
    end
  end
  for i=1:n
    l[i]=wbary[i]/(x-xgp[i])
  end
  denom=sum(l)
  
  for i=1:n
    l[i]=l[i]/denom
  end

  l
end


"""
Computes polynomial differentiation matrix for interpolation polynomial given by set of nodes. (Algorithm 37, Kopriva book)
"""
function polynomialderivativematrix(xgp::AbstractVector{F}) where F <: AbstractFloat
  n=length(xgp)
  wbary=barycentricweights(xgp)
  d=zeros(F,n,n)
  for ilag=1:n; for igp=1:n
    if ilag ≠ igp
      d[igp,ilag]=wbary[ilag] / (wbary[igp] * (xgp[igp]-xgp[ilag]) )
      d[igp,igp] =d[igp,igp]-d[igp,ilag]
    end
  end; end

  d
end


"""
Build a 1D Vandermonde matrix using the Lagrange basis functions,
evaluated at the interpolation points xi_Out
"""
@inline function initializevandermonde!(wbaryin::T,xin::T,xout::T,vdm::AbstractArray{F,2}) where T <: AbstractVector{F} where F <: AbstractFloat
  for i=1:length(xout)
    vdm[i,:]=lagrangeinterpolationpolys(xout[i],xin,wbaryin)
  end
  return
end
function initializevandermonde(wbaryin::T,xin::T,xout::T) where T <: AbstractVector{F} where F <: AbstractFloat
  vdm=zeros(F,length(xout),length(xin))
  initializevandermonde!(wbaryin,xin,xout,vdm)
  vdm
end
