export changebasis1d, changebasis2d, changebasis3d, changebasisvolume, changebasissurf

const chunk = 16
const bufferin  = zeros(10000*chunk)
const bufferout = zeros(10000*chunk)
const buffer1   = zeros(10000*chunk)
const buffer2   = zeros(10000*chunk)

const AA = AbstractArray

@inline function getbuf(t,buf,s)
   if prod(s) > length(buf)
     resize!(buf, prod(s))
   end
   unsafe_wrap(Array{t,length(s)}, pointer(buf), s)
end

"""
Performs tensor product multiplications on 1/2/3 dimensional arrays,
e.g. for interpolating Lagrange polynomials to a different degree or node set.
Vdm is the matrix of size `(nin,nout)`.
"""
function changebasis1d(vdm::V,xi::X) where { V <: AA{T,2}, X <: AA{T,2} } where T
    nvar, ni = s = size(xi)
    xbuf1 = getbuf(T, buffer1, s)
    fill!(xbuf1,0.)
    for ii in ni, io in ni, v in nvar
        xbuf1[v,io] += vdm[io,ii]*xi[v,ii]
    end
    copy!(xi,xbuf1)
    nothing
end
function changebasis1d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,2} } where T
    si, so       = size(xi), size(xo)
    nvar, ni, no = si[1], si[2], so[2]
    if !addtoout
      fill!(xo,0.)
    end
    for ii = 1:ni, io = 1:no, v = 1:nvar
        xo[v,io] += vdm[io,ii]*xi[v,ii]
    end
    nothing
end
# the single var versions
@inline changebasis1d(vdm::V, xi::X)                                where { V <: AA{T,2}, X <: AA{T,1} } where T =
    changebasis1d(vdm, add_diml(xi))
@inline changebasis1d(vdm::V, xi::X, xo::X, addtoout::Bool = false) where { V <: AA{T,2}, X <: AA{T,1} } where T =
    changebasis1d(vdm, add_diml(xi), add_diml(xo), addtoout)
@inline changebasis1d(vdm::V, xi::X, xo::X, addtoout::Bool = false) where { V <: AA{T,2}, X <: AA{T,3} } where T =
    changebasis_pack(changebasis1d, vdm, xi, xo, addtoout)

#function changebasis2d(vdm::V,xi::X) where { V <: AA{T,2}, X <: AA{T,3} } where T
#    fill!(xbuf1,0.)
#    nvar = size(xi)[1]; ni = size(xi)[2]
#    # first direction iI
#    for ji in ni, ii in ni
#      for io in ni, v in nvar
#        xbuf1[v,io,ji] += vdm[io,ii] * xi[v,ii,ji]
#      end
#    end
#    fill!(xi,0.)
#    # second direction ji
#    for ji in ni, jo in ni
#      for io in ni, v in nvar
#        xi[v,io,jo] += vdm[jo,ji] * xbuf1[v,io,ji]
#      end
#    end
#end
function changebasis2d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,3} } where T
    si, so       = size(xi), size(xo)
    nvar, ni, no = si[1], si[2], so[2]
    xbuf1 = getbuf(T, buffer1, (nvar, no, ni))
    fill!(xbuf1,0.)
    # first direction iI
    for ji = 1:ni, ii = 1:ni
      for io = 1:no, v = 1:nvar
        xbuf1[v,io,ji] += vdm[io,ii] * xi[v,ii,ji]
      end
    end
    if !addtoout
      fill!(xo,0.)
    end
    # second direction ji
    for ji = 1:ni, jo = 1:no
      for io = 1:no, v = 1:nvar
        xo[v,io,jo] += vdm[jo,ji] * xbuf1[v,io,ji]
      end
    end
    nothing
end
# More variants: inplace, scalar
#@inline changebasis2d(vdm::V,xi::X)                            where { V <: AA{T,2}, X <: AA{T,2} } where T =
#    changebasis2d(vdm, add_diml(xi))
#@inline changebasis2d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,2} } where T =
#    changebasis2d(vdm, add_diml(xi), add_diml(xo), addtoout)
@inline changebasis2d(vdm::V,xi::X)                            where { V <: AA{T,2}, X <: AA{T,3} } where T =
    changebasis2d(vdm,xi,xi,false)
@inline changebasis2d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,4} } where T =
    changebasis_pack(changebasis2d, vdm, xi, xo, addtoout)
@inline function changebasis2d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,2} } where T
    s, ni, no = size(xi), size(vdm)[2], size(vdm)[1]
    s[2] == ni ? changebasis2d(vdm, add_diml(xi),            add_diml(xo),           addtoout) : 
                 changebasis2d(vdm, reshape(xi, :, ni, ni) , reshape(xo, :, no, no), addtoout)
end


function changebasis3d(vdm::V,xi::X,xo::Y,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,4}, Y <: AA{T,4} } where T
    si, so       = size(xi), size(xo)
    nvar, ni, no = si[1], si[2], so[2]


    # first direction iI
    xbuf1 = getbuf(T, buffer1, (nvar, no, ni, ni))
    for ki=1:ni, ji=1:ni 
      for io=1:no, d=1:nvar
        xbuf1[d,io,ji,ki]  = vdm[io,1 ]*xi[d,1 ,ji,ki]
      end
      for ii=2:ni, io=1:no, d=1:nvar
        xbuf1[d,io,ji,ki] += vdm[io,ii]*xi[d,ii,ji,ki]
      end
    end
    
    # second direction ji
    xbuf2 = getbuf(T, buffer2, (nvar, no, no, ni))
    for ki=1:ni
      for jo=1:no, io=1:no, d=1:nvar
        xbuf2[d,io,jo,ki]  = vdm[jo,1 ]*xbuf1[d,io,1 ,ki]
      end
      for ji=2:ni, jo=1:no, io=1:no, d=1:nvar
        xbuf2[d,io,jo,ki] += vdm[jo,ji]*xbuf1[d,io,ji,ki]
      end
    end
    
    # last direction ki
    if !addtoout
      for ko=1:no, jo=1:no, io=1:no, d=1:nvar
          xo[d,io,jo,ko]  = vdm[ko,1 ]*xbuf2[d,io,jo,1]
      end
      for ki=2:ni, ko=1:no, jo=1:no
        for io=1:no, d=1:nvar
          xo[d,io,jo,ko] += vdm[ko,ki]*xbuf2[d,io,jo,ki]
        end
      end
    else
      for ki=1:ni, ko=1:no, jo=1:no
        for io=1:no, d=1:nvar
          xo[d,io,jo,ko] += vdm[ko,ki]*xbuf2[d,io,jo,ki]
        end
      end
    end
    nothing
end
# More variants: inplace, scalar
@inline changebasis3d(vdm::V,xi::X)                            where { V <: AA{T,2}, X <: AA{T,3} } where T =
    changebasis3d(vdm, add_diml(xi))
@inline changebasis3d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,3} } where T =
    changebasis3d(vdm, add_diml(xi), add_diml(xo), addtoout)
@inline changebasis3d(vdm::V,xi::X)                            where { V <: AA{T,2}, X <: AA{T,4} } where T =
    changebasis3d(vdm,xi,xi,false)
@inline changebasis3d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,5} } where T =
    changebasis_pack(changebasis3d, vdm, reshape(xi, size(xi)[1], :, size(xi)[end]),  
                                         reshape(xo, size(xo)[1], :, size(xo)[end]), addtoout)
@inline function changebasis3d(vdm::V,xi::X,xo::X,addtoout::Bool=false) where { V <: AA{T,2}, X <: AA{T,2} } where T
    ni, no = size(vdm)[2], size(vdm)[1]
    changebasis3d(vdm, reshape(xi, :, ni, ni, ni) , reshape(xo, :, no, no, no), addtoout)
end



"""
Pack the last dimension of an array into the first dimension
"""
@inline function packlast!(x::I, xp::P) where { I <: AA{T,3}, P <: AA{T,2} } where T
    s = size(x)
    @inbounds for l = 1:s[3]
        a = s[1]* (l - 1) + 1
        b = s[1]* l
        for m = 1:s[2], (r1, r2) = zip(a:b, 1:s[1])
            xp[r1,m] = x[r2,m,l]
        end
    end
    nothing
end
@inline packlast!(x::I, xp::P) where { I <: AA, P <: AA} =
 ( s = size(x); packlast!(reshape(x, s[1], :, s[end]), reshape(xp, s[1] * s[end], :)) )

"""
Unpack the first dimension of an array into the last dimension
"""
@inline function unpacklast!(xp::P, x::I) where { I <: AA{T,3}, P <: AA{T,2} } where T
    s = size(x)
    @inbounds for l = 1:s[3]
        a = s[1] * (l - 1) + 1
        b = s[1] * l
        for m = 1:s[2], (r1, r2) = zip(a:b, 1:s[1])
            x[r2, m, l] = xp[r1, m]
        end
    end
    nothing
end
@inline unpacklast!(xp::P, x::I) where { I <: AA, P <: AA} =
 ( s = size(x); unpacklast!(reshape(xp, s[1] * s[end], :), reshape(x, s[1], : , s[end])) )
 

"""
Helper routines for ChangeBasis 2D/3D first packing the arrays
Vdm(0:NIn,0:NIn)         !< 1D Vandermonde In -> Out
TODO: Implement multiple Vandermonde version
"""
function changebasis_pack(cb::Function, vdm::V, xi::X, xo::X, addtoout::Bool=false) where 
    { X <: AA{T,3}, V <: AA{T,2} } where T

    s = size(xi)
    if s[3] ≥ chunk
        bufi = getbuf(T, bufferin,  (s[1]*chunk, s[2]))
        bufo = getbuf(T, bufferout, (s[1]*chunk, size(xo)[2]))
        bufi = getbuf(T, bufferin,  (s[1]*chunk, s[2]))
        bufo = getbuf(T, bufferout, (s[1]*chunk, size(xo)[2]))

        @views @inbounds for i = 1:div(s[3],chunk)
            range = ((i - 1) * chunk + 1 ):(i * chunk)
            packlast!(xi[:,:,range], bufi)
            if addtoout 
              packlast!(xo[:,:,range], bufo)
            end
            cb(vdm, bufi, bufo, addtoout)
            unpacklast!(bufo, xo[:,:,range])
        end
    end
    # the remaining items
    r = s[3] % chunk
    @views if r ≠ 0
        bufi = getbuf(T, bufferin,  (s[1]*r, s[2]))
        bufo = getbuf(T, bufferout, (s[1]*r, size(xo)[2]))
        range = (s[3]-r+1):(s[3])
        packlast!(xi[:,:,range], bufi)
        if addtoout 
          packlast!(xo[:,:,range], bufo)
        end
        cb(vdm, bufi, bufo, addtoout)
        unpacklast!(bufo, xo[:,:,range])
    end
end
@inline function changebasis_pack(cb::Function, vdm::V, xi::X, xo::X, addtoout::Bool=false) where 
    { X <: AA{T,N}, V <: AA{T,2} } where { T,N }
    changebasis_pack(cb, vdm, reshape(xi, size(xi)[1], :, size(xi)[end]),  
                              reshape(xo, size(xo)[1], :, size(xo)[end]), addtoout)
end

function changebasis_pack_inplace(cb::Function, vdm::V, x::X) where 
    { X <: AA{T,3}, V <: AA{T,2} } where T

    s = size(x)
    if s[3] ≥ chunk
        buf = unsafe_wrap(Array{Float64,2}, pointer(bufferin), (s[1]*chunk, s[2])) # seems to be faster
        @views @inbounds for i = 1:div(s[3],chunk)
            range= ((i - 1) * chunk + 1 ):(i * chunk)
            packlast!(x[:,:,range], buf)
            cb(vdm, buf)
            unpacklast!(buf, x[:,:,range])
        end
    end
    # the remaining items
    r = s[3] % chunk
    @views if r ≠ 0
        buf = unsafe_wrap(Array{Float64,2}, pointer(bufferin), (s[1]*r, s[2]))
        range = (s[3]-r+1):(s[3])
        packlast!(x[:,:,range], buf)
        cb(vdm, buf)
        unpacklast!(buf, x[:,:,range])
    end
end
