@reexport module Interpolation

using  GaussQuadrature
import Jacobi
using ..Haflinger

export interpolationinitisdone, initinterpolation
export nodesandweights, getvandermonde, polynomialderivativematrix
export nlegendre, ndlegendre, legendrevdm
export nodetype, IPBasis
export slice!

include("basis.jl")
include("interpolation_vars.jl")
include("changebasis.jl")
# Set volume and surface operations depending on DIM
# Possible alternative: have only one changebasis interface and pass dim as argument?
const changebasisvolume = DIM == 3 ? changebasis3d : changebasis2d
const changebasissurf   = DIM == 3 ? changebasis2d : changebasis1d


function initinterpolation(params)
  initstart("INTERPOLATION")
  global n, nodetype, nodetypestr
  n = getparameter(params,"N"       ; required=true   )
  nodetypestr = lowercase(getparameter(params,"nodetype"; required=false, default="gauss-lobatto"))
  nodetype    = nodetypes[nodetypestr]
  global interpolationinitisdone = true
  initdone("INTERPOLATION")
end


function IPBasis{N,T}(nodetype) where {N,T}
  xgp, wgp, wbary = nodesandweights(BigFloat,N,nodetype)
  lminus = lagrangeinterpolationpolys(BigFloat(-1),xgp,wbary)
  lplus  = lagrangeinterpolationpolys(BigFloat( 1),xgp,wbary)
  vdm = legendrevdm(xgp)
  A = Array{T}
  IPBasis{N,T}(   SVector{N}(A(xgp)),    SVector{N}(A(wgp)),   SVector{N}(A(wbary)),
                  SVector{N}(A(lminus)), SVector{N}(A(lplus)),
                SMatrix{N,N}(A(vdm)),  SMatrix{N,N}(A(inv(vdm))), nodetype )
end


function nodesandweights(T,n,nodetype)
  xgp,wgp = nodefuncs[nodetype](T,n)
  wbary   = barycentricweights(xgp)
  xgp, wgp, wbary
end
@inline nodesandweights(n,nodetype) = nodesandweights(FT,n,nodetype)

"""
Build a 1D Vandermonde matrix from an orthonormal Legendre basis to a nodal basis and reverse
"""
function legendrevdm(xi::Vector{T}) where T <: AbstractFloat
  # Note: As alternative to matrix inversion: Compute inverse Vandermonde directly.
  # See Flexi file basis.f90
  # Direct inversion seems to be more accurate

  n=length(xi)
  vdm = zeros(T,n,n)
  #compute the Vandermonde on xGP
  for i=1:n, j=1:n
    vdm[i,j] = nlegendre(xi[i],j)
  end
  #check vdm^(-1) * vdm == I
  checksum = abs(sum( inv(vdm)*vdm )) - n
  #@assert checksum ≤ n^4*eps(T) "Insufficient accuracy of Legendre Vandermonde"

  vdm
end


"""
Build a Vandermonde-type interpolation or projection matrix
from/to different node types and polynomial degrees.
This is by default conducted in extended accuracy mode (extacc),
where all quantities are computed at higher precision.
"""
function getvandermonde(n_in::I,nodetype_in::Symbol,n_out::I,nodetype_out::Symbol;
                        projection::Bool=false,extacc::Bool=true) where I <: Integer
  # Check if change Basis is needed
  if nodetype_out == nodetype_in && n_in == n_out
    vdm_in_out = eye(n_out,n_in)
  else
    setprecision(BigFloat,1024)
    T = extacc ? BigFloat : Float64
    # input/output points
    xip_in,  = nodefuncs[nodetype_in](T,n_in)
    xip_out, = nodefuncs[nodetype_out](T,n_out)

    # nodal: direct interpolation, projection: projection
    if n_out ≤ n_in && projection
      svdm_leg_in = inv(legendrevdm(xip_in))
      vdm_leg_out =     legendrevdm(xip_out)
      vdm_in_out  = vdm_leg_out[1:n_out,1:n_out]*svdm_leg_in[1:n_out,1:n_in]
    else
      wbary_in    = barycentricweights(xip_in)
      vdm_in_out  = initializevandermonde(wbary_in,xip_in,xip_out)
    end
  end
  Array{FT}(vdm_in_out)
end

#TODO: create container for vol2surf and surf2vol base operations similar to mesh mappings
"""
Interpolates the element volume data stored at Gauss points
"""
function slice!(face::AbstractArray{T,4}, vol::AbstractArray{T,5}, ::Val{V}, ::Val{N}, E) where {V,N,T}
    @inbounds begin
    for q = 1:N, p = 1:N, v = 1:V
        face[v,p,q,XI_MINUS]   = vol[v,1,p,q,E]
        face[v,p,q,ETA_PLUS]   = vol[v,p,N,q,E]
        face[v,p,q,ZETA_MINUS] = vol[v,p,q,1,E]
    end
    for q = 1:N, p = 1:N, v = 1:V
        face[v,p,q,XI_PLUS]    = vol[v,N,p,q,E]
        face[v,p,q,ETA_MINUS]  = vol[v,p,1,q,E]
        face[v,p,q,ZETA_PLUS]  = vol[v,p,q,N,E]
    end
    end
end
@inline function slice!(face::A, vol::B, ::Val{V}, ::Val{N}, E, locside) where
  { A <: AbstractArray{T,3}, B <: AbstractArray{T,5}, V,N} where T
    @inbounds begin
        if     locside == XI_MINUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,1,p,q,E]
            end
        elseif locside == XI_PLUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,N,p,q,E]
            end
        elseif locside == ETA_MINUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,p,1,q,E]
            end
        elseif locside == ETA_PLUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,p,N,q,E]
            end
        elseif locside == ZETA_MINUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,p,q,1,E]
            end
        elseif locside == ZETA_PLUS
            for q = 1:N, p = 1:N, v = 1:V
                face[v,p,q] = vol[v,p,q,N,E]
            end
        end
    end
end
@inline function slice!(face::A, vol::B, v::Val{V}, n::Val{N}, locside) where
  { A <: AbstractArray{T,3}, B <: AbstractArray{T,4}, V,N} where T
   slice!(face, add_dimr(vol), v, n, 1, locside )
end

end
