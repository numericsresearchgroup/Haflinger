
#const docalcsource=true              #< automatically set by calcsource itself
#const iniexactfunc::Function          #< function to initialize
#const inirefstate::Array{MYFLOAT,1} #< RefState for initialization
#const refstate::Array{MYFLOAT,2}    #< refstates in conservative variables
#const refstateadd::Array{MYFLOAT,2}          #< refstates in primitive variables (as read from ini file)
#const bcstatefile::String             #< file containing the reference solution on the boundary to be used as BC
#
## Boundary condition arrays
#const bcdata      #< array with precomputed BC values (conservative)
#const bcdataadd   #< array with precomputed BC values (primitive)
#const nbcbytype   #< number of sides with specific BC type
#const bcsideid

const USEPRIM      = true
const NVAR         = 5
const NVARLIFT     = 6
const NVARE        = 11 # prim + cons
const VARNAMES     = ["Density","MomentumX","MomentumY","MomentumZ","EnergyStagnationDensity"] #< conservative variable names
const VARNAMESLIFT = ["Density","VelocityX","VelocityY","VelocityZ","Pressure","Temperature" ] #< primitive variable names

const eddyviscosity=false
equationinitisdone=false

# TODO use enums, fix 2D

# prim and cons
@enum UCONS  DENS=1 MOM1=2 MOM2=3 MOM3=4 ENER=5
@enum UPRIM PDENS=1 VEL1=2 VEL2=3 VEL3=4 PRES=5 TEMP=6
# the extended vectors combining prim and cons
@enum UEXT  EDENS=1 EMOM1=2 EMOM2=3 EMOM3=4 EENER=5 ESRHO=6 EVEL1=7 EVEL2=8 EVEL3=9 EPRES=10 ETEMP=11
Base.to_index(s::UCONS) = Int(s)
Base.to_index(s::UPRIM) = Int(s)
Base.to_index(s::UEXT)  = Int(s)

const VELV=2:4
const MOMV=2:4

#export DENS, VEL1, VEL2, VELV, PRES, TEMP, MOM1, MOM2, MOMV, ENER
#@d3 export VEL3, MOM3