#"""
#Functions that provide gas properties and conversion between primitive and conservative variables for a ideal gas.
#"""
module Idealgas

using ....Haflinger

export initeos, eosvars
export initexactfunc, initbc, exactfunc, boundaryflux
export primtocons!, constoprim!, speedofsound, speedofsounde
export viscosity, temperature, thermalconductivity

include("exactfunc.jl")
include("boundaryflux.jl")
include("../equation_vars.jl")

mutable struct EOSVars{F <: AbstractFloat}
    μ0::F   # Dynamic viscosity
    Pr::F   # Prandtl  number
    c_p::F  # Specific heat at constant pressure/volume
    c_v::F
    λ0::F   # Thermal conductivity (in case of constant viscosity)
    κ::F    # Heat capacity ratio / isentropic exponent
    R::F    # Specific gas constant
    # Sutherland quantities
    tsuth::F
    csuth::F
    tref::F
    exposuth::F
    EOSVars{F}() where F = new(-1.,-1.,-1.,-1.,-1.,-1.,-1.,-1.,-1.,-1.,-1.)
end
const eosvars = EOSVars{FT}()
eosinitisdone = false

"""
Initialize variables needed by the ideal gas equation of state.
"""
function initeos(params, eos::EOSVars=eosvars)
    initstart("IDEAL GAS")

    usenondimensionaleqn = getparameter(params, "usenondimensionaleqn", required=false, default=false)
    if usenondimensionaleqn
        bulkmach     = getparameter(params, "bulkmach", required=true)
        bulkreynolds = getparameter(params, "bulkreynolds", required=true)
    end

    # Gas constants
    eos.κ = getparameter(params, "kappa", required=false, default=1.4)
    eos.R = usenondimensionaleqn ? 1 / (κ * bulkmach^2) : getparameter(params, "R", required=false, default=287.058)

    eos.c_p = eos.R * eos.κ / (eos.κ - 1)
    eos.c_v = eos.R / (eos.κ - 1)

    if USEPARABOLIC
        eos.Pr   = getparameter(params, "Pr",   required=false, default=0.72)
        eos.μ0   = getparameter(params, "mu0",  required=false, default=1.735E-5)
        visc     = getparameter(params, "visc", required=false, default=0)
        # Viscosity
        if visc == 1  # mu-Sutherland
            eos.tsuth    = getparameter(params, "Ts",       required=false, default=110.4)
            eos.tref = 1 / getparameter(params, "Tref",     required=false, default=280.)
            eos.exposuth = getparameter(params, "ExpoSuth", required=false, default=1.5)
            eos.tsuth    = eos.tsuth * eos.tref
            eos.csuth    = eos.tsuth^eos.exposuth * (1 + eos.tsuth) / (2*eos.tsuth^2)
        elseif visc == 2  # mu power-law
            eos.tref     =  usenondimensionalEqn ? 1 : getparameter(params, "Tref", required=true)
            eos.exposuth = getparameter(params, "ExpoSuth", required=true)
            eos.μ0       = eos.μ0 / eos.tref^eos.exposuth
        end
        eos.λ0 = thermalconductivityvarmu(eos.μ0)
    end

    initdone("IDEAL GAS")
end
# TODO: decide whether to swap prim and cons here, since we always want out=f(in) or f!(in,out)


"""
Transformation from conservative variables to primitive variables
"""
@inline function constoprim!(cons::AbstractVector{T}, prim::AbstractVector{T}) where T
    @inbounds begin
        # density ρ
        prim[1]    = cons[1]
        # velocity
        srho       = 1 / cons[1]
        prim[2]    = cons[2] * srho
        prim[3]    = cons[3] * srho
        prim[4]    = cons[4] * srho
        # pressure
        prim[5]    = (eosvars.κ - 1) * (cons[5] - (cons[2] * prim[2] + cons[3] * prim[3] + cons[4] * prim[4]) / 2)
        # temperature
        prim[6]    = prim[5] * srho / eosvars.R
    end
end
function constoprim!(cons::AbstractArray{T,2}, prim::AbstractArray{T,2}, r) where T
    @inbounds for i in r
        @views constoprim!(cons[:,i], prim[:,i])
    end
end
@inline  constoprim!(cons::AbstractArray{T,2}, prim::AbstractArray{T,2}) where T = constoprim!(cons, prim, 1:size(cons,2))

"""
Transformation from primitive to conservative variables
"""
@inline function primtocons!(prim::AbstractVector{T}, cons::AbstractVector{T}) where T <: AbstractFloat
    @inbounds begin
        # rho
        cons[1] = prim[1]
        # momentum
        cons[2] = prim[2] * prim[1]
        cons[3] = prim[3] * prim[1]
        cons[4] = prim[4] * prim[1]
        # inner energy
    @d3 cons[5] = prim[5] / (eosvars.κ - 1) + (cons[2] * prim[2] +
                                               cons[3] * prim[3] +
                                               cons[4] * prim[4]) / 2
    @d2 cons[5] = prim[5] / (eosvars.κ - 1) + (cons[2] * prim[2] +
                                               cons[3] * prim[3]) / 2
    end
end
function primtocons!(prim::AbstractArray{T,2}, cons::AbstractArray{T,2}, r) where T
    @inbounds for i in r
        @views primtocons!(prim[:,i], cons[:,i])
    end
end
@inline  primtocons!(prim::AbstractArray{T,2}, cons::AbstractArray{T,2}) where T = primtocons!(prim, cons, 1:size(prim,2))


@inline function pressureriemann(prim::AbstractVector{T}) where T <: AbstractFloat
    κ = eosvars.κ
    if prim[2] ≤ 0 # rarefaction
        prp = prim[5] * max(0.00001, (1 + (κ - 1) / 2 * prim[2] / sqrt(κ * prim[5] / prim[1])))^(2κ / (κ - 1))
    else # shock
        ar  = 2 / (κ + 1) / prim[1]
        br  = (κ - 1) / (κ + 1) * prim[5]
        prp = prim[5] + prim[2] / ar * ( prim[2] + sqrt(prim[2]^2 + 4 * ar * (prim[5] + br)) ) / 2
    end
    prp
end

@inline speedofsounde(ue) = sqrt(eosvars.κ * ue[EPRES] * ue[ESRHO])
@inline speedofsound(up)  = sqrt(eosvars.κ * up[PRES]  / up[DENS])

@inline temperature(u::AbstractVector) = (u[ENER] - 0.5*(u[MOM1]^2+u[MOM2]^2+u[MOM3]^2)/u[DENS])/(u[DENS]*eosvars.c_v)

#---------------------------------------------------------------------------------------------------
# We define different methods to compute the dynamic viscosity
"""
Sutherland's formula can be used to derive the dynamic viscosity of an ideal gas as a function of
the temperature. This is only valid for temperatures in the range 0 < T < 555 K.
Temperatures above the Sutherland temperature Ts are computed as
        T >= Ts:    μ = μ0 * (T/Tref)^(expo) *  (Tref+TS)/(T+TS)     (1)
Example values would be Ts=110.4K, Tref 280K and μ0=μ(Tref)=1.735E-5Kg/ms and expo = 3/2

Below Ts a linear dependence is assumed
        T <  Ts:    μ = μ0*T/Tref*c                                  (2)
with c = (Ts/Tref)^exp*(1+(Ts/Tref))/(2(Ts/Tref)²) for steady transition from (1) to (2) at T = Ts.

NOTES: 1) The global variable Tref=1./Tref and Ts=Ts/Tref !!!!!
       2) μ0, Ts, Tref, ExpoSuth and cSuth are global variables
"""
@inline function sutherlandviscosity(T::AbstractFloat)
    e = eosvars
    Tnodim = T * e.Tref
    Tnodim ≥ e.Ts ? (e.μ0 * (Tnodim)^e.exposuth * (1 + e.Ts) / (Tnodim + e.Ts)) : (e.μ0 * Tnodim * e.csuth)
 end
@inline sutherlandviscosity(u::AbstractVector) = sutherlandviscosity(temperature(u))
@inline powerlawviscosity(  T::AbstractFloat)  = eosvars.μ0 * T^eosvars.exposuth
@inline powerlawviscosity(  u::AbstractVector) = powerlawviscosity(temperature(u))
@inline constantviscosity(  u                ) = eosvars.μ0
const viscosity = constantviscosity
#---------------------------------------------------------------------------------------------------

@inline thermalconductivityvarmu(μ::AbstractFloat) = μ*eosvars.c_p/eosvars.Pr
@inline thermalconductivityfixmu(μ)                = eosvars.λ0
const thermalconductivity = thermalconductivityfixmu
#@inline thermalconductivity(u::AbstractVector) = thermalconductivity(viscosity(u))

end
