
const fullboundaryorder=false

"""
Get some parameters needed for exact function
"""
function initexactfunc(params)
  initstart("EXACT FUNCTION")

  iniexnr        = getparameter(params,"iniexactfunc"; required=true)
  global iniexactfunc   = exactfuncs[iniexnr][1]
  global addsourceterms = exactfuncs[iniexnr][2]
  global exactfunc      = iniexactfunc

  # Read in boundary parameters
  if iniexnr in [2,21,3,4,41,42,43] # synthetic test cases
    global advvel          = getparameter(params,"advvel",      required=false,default=[0.3,0.3,0.3])
  elseif iniexnr == 7    # Shu Vortex
    global inicenter       = getparameter(params,"inicenter",   required=false,default=[0.,0.,0.])
    global iniaxis         = getparameter(params,"iniaxis",     required=false,default=[0.,0.,1.])
    global iniamplitude    = getparameter(params,"iniamplitude",required=false,default=0.2)
    global inihalfwidth    = getparameter(params,"inihalfwidth",required=false,default=0.2)
  elseif iniexnr == 8    # couette-poiseuille flow
    global p_parameter     = getparameter(params,"p_parameter", required=false,default=0.0)
    global u_parameter     = getparameter(params,"u_parameter", required=false,default=0.01)
  elseif iniexnr == 10   # shock
    global machshock       = getparameter(params,"machshock",   required=false,default=1.5)
    global preshockdens    = getparameter(params,"preshockdens",required=false,default=1.)
  elseif iniexnr == 1338 # Blasius boundary layer solution
    global delta99_in      = getparameter(params,"delta99_in",  required=true)
    global x_in            = getparameter(params,"x_in",        required=false,default=[0.,0.])
  end

  initdone("EXACT FUNCTION")
end

"""
For RK methods order reduction at time-dependent methods may occur, reducing the RKs accuracy to O(2)
Simple countermeasures exists for 3-stage 3rd order RK schemes, see paper by Carpenter, Gottlieb:
The Theoretical Accuracy of Runge–Kutta Time Discretizations for the Initial Boundary Value Problem:
A Study of the Boundary Error

Note that the time for full boundary order needs to be the time of the RK schemes initial timestep

For O3 LS 3-stage RK, we have can actually define proper time dependent BCs.
add ut, utt if time dependant
(1) u = g(t)
(2) u = g(t) + dt/3*g"(t)
(3) u = g(t) + 3/4 dt g"(t) +5/16 dt^2 g""(t)
"""
function rk3corrector(u,ut,utt,stage)
    #teval = fullboundaryorder ? t : tin  # prevent temporal order reduction, works only for RK3 time integration
    if     stage == 1
    elseif stage == 2
      u .+= rkc[2]*dt .* ut
    elseif stage == 3
      u .+= rkc[3]*dt .* ut + rkc[2]*rkb[2]*dt*dt*utt
    else
      error("Exactfuntion works only for 3 Stage O3 LS RK!")
    end
end

function constant(    u::AbstractVector{T}, x::AbstractVector{T}, t::T, refstate::AbstractVector{T}=inirefstate, stage=1) where T <: AbstractFloat
  u .= inirefstate
  nothing
end

function sineconvtest(u::AbstractVector{T}, x::AbstractVector{T}, t::T, refstate::AbstractVector{T}=inirefstate, stage=1) where T <: AbstractFloat
    # g(t)
    frequency = 0.5
    amplitude = 0.3
    ω = 2π*frequency
    cent = x-advvel*t
    prim = [1 + amplitude*sin(ω*sum(cent)); advvel; 1]
    primtocons!(prim, u)

    if fullboundaryorder && stage > 1
        ov = ω*sum(advvel)
        pd = 0.5*dot(advvel,advvel)
        denst  = -amplitude*cos(ω*sum(cent))*ov
        denstt = -amplitude*sin(ω*sum(cent))*ov^2
        ut  = [denst,  advel*denst,  pd*denst]
        utt = [denstt, advel*denstt, pd*denstt]
        rk3corrector(u,ut,utt,stage)
    end
end

@inline function sinexconvtest(t,x,u)
    frequency=0.5
    amplitude=0.3
    ω=2*π*frequency
    # base flow
    prim[1]   = 1
    prim[2:4] = advvel
    prim[5]   = 1
    vel=prim[2:4]
    cent=x-vel*t
    prim[1]=prim[1]*(1+amplitude*sin(ω*cent[1]))
    # g(t)
    u[1]=prim[1] # rho
    u[2:4]=prim[1]*prim[2:4] # rho*vel
    u[5]=prim[5]\(κ-1)+0.5*prim[1]*sum(prim[2:4]*prim[2:4]) # rho*e

    if fullboundaryorder && stage > 1
        ov = ω*sum(vel)
        # g"(t)
        ut[1]   = -amplitude*cos(ω*cent[1])*ov
        ut[2:4] = ut[1]*prim[2:4] # rho*vel
        ut[5]   = 0.5*ut[1]*sum(prim[2:4]*prim[2:4])
        # g""(t)
        utt[1]  = -amplitude*sin(ω*cent[1])*ov^2
        utt[2:4]= utt[1]*prim[2:4]
        utt[5]  = 0.5*utt[1]*sum(prim[2:4]*prim[2:4])
        rk3corrector(u,ut,utt,stage)
    end
end


@inline function sineviscconvtest(x,t,u)
  # oblique sine wave (in x,y,z for 3D calculations, and x,y for 2D)
  dim = 3
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[1]*2*π

  # g(t)
  u[1:4]=2+ amp*sin(ω*sum(x[1:dim]) - a*t)
  u[5]  =u[1]*u[1]
  if fullboundaryorder
    # g"(t)
    ut[1:dim+1]=(-a)*amp*cos(ω*sum(x[1:dim]) - a*t)
    ut[5]=2*u[1]*ut[1]
    # g""(t)
    utt[1:dim+1]=-a*a*amp*sin(ω*sum(x[1:dim]) - a*t)
    utt[5]=2*(ut[1]*ut[1] + u[1]*utt[1])
    return u,ut,utt
  else
    return u
  end
end

@inline function sinexviscconvtest(x,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[1]*2*π
  # g(t)
  u = 0
  u[1:2]=2+ amp*sin(ω*x[1] - a*t)
  u[5]=u[1]*u[1]
  if fullboundaryorder
    ut = 0
    utt = 0
    # g"(t)
    ut[1:2]=(-a)*amp*cos(ω*x[1] - a*t)
    ut[5]=2*u[1]*ut[1]
    # g""(t)
    utt[1:2]=-a*a*amp*sin(ω*x[1] - a*t)
    utt[5]=2*(ut[1]*ut[1] + u[1]*utt[1])
    return u,ut,utt
  else
    return u
  end
end


@inline function sineyviscconvtest(x,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[2]*2*π
  # g(t)
  u = 0
  u[1]=2+ amp*sin(ω*x[2] - a*t)
  u[3]=u[1]
  u[5]=u[1]*u[1]
  if fullboundaryorder
    utt = 0
    # g"(t)
    ut[1]=(-a)*amp*cos(ω*x[2] - a*t)
    ut[3] = ut[1]
    ut[5]=2*u[1]*ut[1]
    # g""(t)
    utt[1]=-a*a*amp*sin(ω*x[2] - a*t)
    ut[3] = ut[1]
    utt[5]=2*(ut[1]*ut[1] + u[1]*utt[1])
    return u,ut,utt
  else
    return u
  end
end

@inline function sinezviscconvtest(x,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[3]*2*π
  # g(t)
  u = 0
  u[1]=2+ amp*sin(ω*x[3] - a*t)
  u[4]=u[1]
  u[5]=u[1]*u[1]
  if fullboundaryorder
    utt = 0
    # g"(t)
    ut[1]=(-a)*amp*cos(ω*x[3] - a*t)
    ut[4] = ut[1]
    ut[5]=2*u[1]*ut[1]
    # g""(t)
    utt[1]=-a*a*amp*sin(ω*x[3] - a*t)
    ut[4] = ut[1]
    utt[5]=2*(ut[1]*ut[1] + u[1]*utt[1])
    return u,ut,utt
  else
    return u
  end
end

@inline function roundjet(x,t)
  #Roundjet Bogey Bailly 2002, Re=65000, x-axis is jet axis
  prim[1]  =1
  prim[2:4]=0
  prim[5]  =1/κ
  prim[6]  = prim[5]/(prim[1]*r)
  # Jet inflow (from x=0, diameter 2.0)
  # Initial jet radius: rj=1
  # Momentum thickness: delta_theta0=0.05=1/20
  # Re=65000
  # Uco=0
  # Uj=0.9
  rlen=sqrt((x[2]*x[2]+x[3]*x[3]))
  prim[2]=0.9*0.5*(1+tanh((1-rlen)*10))
  r=rand(typeof(rlen))
  # random disturbance +-5
  r=0.05*2*(r-0.5)
  prim[2]=prim[2]+r*prim[2]
  prim[3]=x[2]/rlen*0.5*r*prim[2]
  prim[4]=x[3]/rlen*0.5*r*prim[2]
  primtocons(prim,ul)
  prim[1]  =1
  prim[2:4]=0
  prim[5]  =1/κ
  prim[6] = prim[5]/(prim[1]*r)
  primtocons(prim,ur)
#   after x=10 blend to ur
  u=ul+(ur-ul)*0.5*(1+tanh(x[1]-10))
end

@inline function cylinder(x,t)
  if t  ==  0   # Initialize potential flow
    prim[1]=refstateprim[1,refstate]  # density
    prim[4]=0           # VelocityZ=0 (2D flow)
    # Calculate cylinder coordinates (0<phi<Pi/2)
    if x[1] < 0
      phi = atan(abs(x[2])/abs(x[1]))
      phi = x[2] < 0 ? π+phi : π-phi
    elseif x[1]  >  0
      phi = atan(abs(x[2])/abs(x[1]))
      if x[2] < 0
        phi=2*π-phi
      end
    else
      phi = x[2] < 0 ? π*1.5 : π*0.5
    end
    # Calculate radius^2
    radius=x[1]*x[1]+x[2]*x[2]
    # Calculate velocities, radius of cylinder=0.5
    prim[2]=refstateprim(2,refstate)*(cos(phi)^2*(1-0.25/radius)+sin(phi)^2*(1+0.25/radius))
    prim[3]=refstateprim(2,refstate)*(-2.)*sin(phi)*cos(phi)*0.25/radius
    # Calculate pressure, RefState(2)=u_infinity
    prim[5]=refstateprim(5,refstate) +
            0.5*prim[1]*(refstateprim(2,refstate)*refstateprim(2,refstate)-prim[2]*prim[2]-prim[3]*prim[3])
    prim[6] = prim[5]/(prim[1]*r)
  else  # Use RefState as BC
    prim=RefStatePrim[:,RefState]
  end  # t=0
  primtocons(prim,u)
end


@inline function shuvortex(x,t)
  # SHU VORTEX,isentropic vortex
  # base flow
  prim=refstateprim[:,refstate]  # density
  # ini-Parameter of the Example
  vel=prim[2:4]
  rt=prim[pp_nvar]/prim[1] #ideal gas
  cent=(inicenter+vel*t) #centerpoint time dependant
  cent=x-cent # distance to centerpoint
  cent=cross(iniaxis,cent) #distance to axis, tangent vector, length r
  cent=cent/inihalfwidth #halfwidth is dimension 1
  r2=sum(cent*cent) #
  du = iniamplitude/(2*π)*exp(0.5*(1-r2)) # vel. perturbation
  dtemp = -(κ-1)/(2*κ*rt)*du^2 # adiabatic
  prim[1]=prim[1]*(1+dtemp)^(1\(κ-1)) #rho
  prim[2:4]=prim[2:4]+du*cent #v
  prim[pp_nvar]=prim[pp_nvar]*(1+dtemp)^(κ/(κ-1)) #p
  prim[6] = prim[5]/(prim[1]*r)
  primtocons(prim,u)
end


@inline function couettepoiseulle(x,t)
  #Couette-Poiseuille flow between plates: exact steady lamiar solution with height=1 (Gao, hesthaven, Warburton)
  rt=1 # Hesthaven: Absorbing layers for weakly compressible flows
  srt=1/rt
  # size of domain must be [-0.5, 0.5]^2 -> (x*y)
  h=0.5
  prim[2]= u_parameter*(0.5*(1+x[2]/h) + p_parameter*(1-(x[2]/h)^2))
  prim[3:4]= 0
  pexit=0.9996
  pentry=1.0004
  prim[5]= ( ( x[1] - (-0.5) )*( pexit - pentry) / ( 0.5 - (-0.5)) ) + pentry
  prim[1]=prim[5]*srt
  prim[6]=prim[5]/(prim[1]*r)
  primtocons(prim,u)
end

@inline function liddrivencavity(x,t)
  #lid driven cavity flow from Gao, Hesthaven, Warburton
  #"Absorbing layers for weakly compressible flows", to appear, JSC, 2016
  # Special "regularized" driven cavity BC to prevent singularities at corners
  # top BC assumed to be in x-direction from 0..1
  prim = refstateprim[:,refstate]
  if x[1] < 0.2
    prim[2]=1000*4.9333*x[1]^4-1.4267*1000*x[1]^3+0.1297*1000*x[1]^2-0.0033*1000*x[1]
  elseif x[1] ≤ 0.8
    prim[2]=1
  else
    prim[2]=1000*4.9333*x[1]^4-1.8307*10000*x[1]^3+2.5450*10000*x[1]^2-1.5709*10000*x[1]+10000*0.3633
  end
  primtocons(prim,u)
end


@inline function shock(x,t)
  prim=0

  # pre-shock
  prim[1] = preshockdens
  ms      = machshock

  prim[5]=prim[1]/κ
  prim[6]=prim[5]/(prim[1]*r)
  primtocons(prim,ur)

  # post-shock
  prim[3]=prim[1] # temporal storage of pre-shock density
  prim[1]=prim[1]*((κ+1)*ms*ms)/(κ-1*ms*ms+2.)
  prim[5]=prim[5]*(2*κ*ms*ms-(κ-1))/(κ+1)
  prim[6]=prim[5]/(prim[1]*r)
  if prim[2]  ==  0.0
    prim[2]=ms*(1-prim[3]/prim[1])
  else
    prim[2]=prim[2]*prim[3]/prim[1]
  end
  prim[3]=0 # reset temporal storage
  primtocons(prim,ul)
  xs=5+ms*t # 5. bei 10x10x10 Rechengebiet
  # Tanh boundary
  u=-0.5*(ul-ur)*tanh(5.0*(x[1]-xs))+ur+0.5*(ul-ur)
end


@inline function sodshocktube(x,t)
  xs = 0.5
  u = x[1] ≤ xs ? refstatecons[:,1] : refstatecons[:,2]
end


@inline function shuosherdensityshock(x,t)
  # Shu Osher density fluctuations shock wave interaction
  prim = primtocons( x[1] < -4  ? [ 3.857143, 2.629369, 0, 0, 10.33333 ] : [ 1 + 0.2 * sin(5. * x[1]), 0, 0, 0, 1 ] )
end


@inline function doublemachreflection(x,t)
  # see e.g. http:*www.astro.princeton.edu/~jstone/Athena/tests/dmr/dmr.html
  if x[1] == 0
    prim = refstateprim[:,1]
  elseif x[1] == 4.0
    prim = refstateprim[:,2]
  else
    if x[1] < 1/6 + (x[2] + 20. * t)*1 / 3. ^ 0.5
      prim = refstateprim[:,1]
    else
      prim = refstateprim[:,2]
    end
  end
  primtocons(prim,u)
end


@inline function blasius(x,t)
  prim=refstateprim[:,refstate]
  # calculate equivalent x for Blasius flat plate to have delta99_in at x_in
  x_offset[1]=(delta99_in/5)^2*prim[1]*prim[2]/mu0-x_in(1)
  x_offset[2]=-x_in[2]
  x_offset[3]=0
  x_eff=x+x_offset
  if x_eff(2) > 0 && x_eff(1) > 0
    # scale bl position in physical space to reference space, eta=5 is ~99. bl thickness
    eta=x_eff(2)*(prim[1]*prim[2]/(mu0*x_eff(1)))^0.5

    deta=0.02 # step size
    nsteps=ceiling(eta/deta)
    deta =eta/nsteps
    deta2=0.5*deta

    f=0
    fp=0
    fpp=0.332 # default literature value, don"t change if you don"t know what you"re doing
    fppp=0
    #Blasius boundary layer
    for i=1:nsteps
      # predictor
      fbar    = f   + deta * fp
      fpbar   = fp  + deta * fpp
      fppbar  = fpp + deta * fppp
      fpppbar = -0.5*fbar*fppbar
      # corrector
      f       = f   + deta2 * (fp   + fpbar)
      fp      = fp  + deta2 * (fpp  + fppbar)
      fpp     = fpp + deta2 * (fppp + fpppbar)
      fppp    = -0.5*f*fpp
    end
    prim[3]=0.5*(mu0*prim[2]/prim[1]/x_eff(1))^0.5*(fp*eta-f)
    prim[2]=refstateprim(2,refstate)*fp
  else
    if x_eff(2) < 0
      prim[2]=0
    end
  end
  primtocons(prim,u)
end



"""
Compute source terms for some specific testcases and adds it to DG time derivative
"""
function viscousconvtestxyz(ut,t)
  dim = 3
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[1]*2π
  tmp[1]= -a+dim*ω
  tmp[2]= -a+0.5*ω*(1+κ*5.)
  tmp[3]= amp*ω*(κ-1)
  tmp[4]= 0.5*((9+κ*15)*ω-8*a)
  tmp[5]= amp*(3*ω*κ-a)
  tmp[6]= dim*mu0*κ*ω*ω/pr
  tmp  *= amp
  at    = a*t
  for i=1:ndof
    cosxgp=cos(ω*sum(elemxgp[:,i])-at)
    sinxgp=sin(ω*sum(elemxgp[:,i])-at)
    sinxgp2=2*sinxgp*cosxgp # =sin(2*(ω*sum(elem_xgp[:,i,j,k,ielem))-a*t))

    src[1]   = tmp[1]*cosxgp
    src[2:4] = tmp[2]*cosxgp + tmp[3]*sinxgp2
    src[5]   = tmp[4]*cosxgp + tmp[5]*sinxgp2 + tmp[6]*sinxgp
    ut[:,i]    += src/sj[i]
  end
end


function sinexsource(ut,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[1]*2π
  c = 2
  at=a*t

  for i=1:ndof
    src[1]   = (-amp*a+amp*ω)*cos(ω*elemxgp[1,i]-at)

    src[2]   = (-amp^2*ω+amp^2*ω*κ)*sin(2*ω*elemxgp[1,i]-2*at)+
               (-amp*a+2*amp*ω*κ*c-1/2*amp*ω*κ+
               3/2*amp*ω-2*amp*ω*c)*cos(ω*elemxgp[1,i]-at)
    src[3:4] = 0.0
    src[5]   = mu0*κ*amp*sin(ω*elemxgp[1,i]-at)*ω^2/pr+
                (-amp^2*a+amp^2*ω*κ)*sin(2*ω*elemxgp[1,i]-2*at)+
                1/2 * (-4. * amp*a*c + 4. * amp*ω*κ*c-amp*ω*κ+
                   amp*ω)*cos(ω*elemxgp[1,i] - at)
    ut[:,i] += src/sj[i]
  end
end


function sineysource(ut,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[2]*2π
  c = 2
  at=a*t

  for i=1:ndof
    src[1]   = (-amp*a+amp*ω)*cos(ω*elemxgp[2,i]-at)
    src[2]   = 0
    src[3]   = (-amp^2*ω+amp^2*ω*κ)*sin(2*ω*elemxgp[2,i]-2*at)+
                      (-amp*a+2*amp*ω*κ*c-1/2*amp*ω*κ+
                      3/2*amp*ω-2*amp*ω*c)*cos(ω*elemxgp[2,i]-at)
    src[4]   = 0
    src[5]   = mu0*κ*amp*sin(ω*elemxgp[2,i]-at)*ω^2/pr+
                      (-amp^2*a+amp^2*ω*κ)*sin(2*ω*elemxgp[2,i]-2*at)+
                      1/2*(-4 * amp*a*c + 4 * amp*ω*κ*c-amp*ω*κ+
                           amp*ω)*cos(ω*elemxgp[2,i]-at)
    ut[:,i] += src/sj[i]
  end
end

function sinezsource(ut,t)
  frequency=1
  amp=0.1
  ω=π*frequency
  a=advvel[3]*2π
  c = 2
  at=a*t

  for i=1:ndof
    src[1]   = (-amp*a+amp*ω) * cos(ω*elemxgp[3,i]-at)
    src[2:3] = 0
    src[4]   = ( (-amp^2*ω+amp^2*ω*κ) * sin(2*ω*elemxgp[3,i]-2*at) +
                 (-amp*a + 2*amp*ω*κ*c - 1/2*amp*ω*κ + 3/2*amp*ω - 2*amp*ω*c) *
                 cos(ω*elemxgp[3,i]-at) )

    src[5]   = ( mu0*κ*amp * sin(ω*elemxgp[3,i]-at)*ω^2/pr  +
                 (-amp^2*a + amp^2*ω*κ) * sin(2*ω*elemxgp[3,i]-2*at) +
                 1/2*(-4*amp*a*c + 4*amp*ω*κ*c - amp*ω*κ + amp*ω) *
                 cos(ω*elemxgp[3,i]-at) )
    ut[:,i] += src/sj[i]
  end
end

noop() = nothing
exactfuncs = Dict{Integer,Tuple{Function,Function}}(
          1  => (constant,             noop),
          2  => (sineconvtest,         noop),
          21 => (sinexconvtest,        noop),
          3  => (sinexconvtest,        noop),
          4  => (sineviscconvtest,     noop),
          41 => (sinexviscconvtest,    noop),
          42 => (sineyviscconvtest,    noop),
          43 => (sinezviscconvtest,    noop),
          5  => (roundjet,             noop),
          6  => (cylinder,             noop),
          7  => (shuvortex,            noop),
          8  => (couettepoiseulle,     noop),
          9  => (liddrivencavity,      noop),
          10 => (shock,                noop),
          11 => (sodshocktube,         noop),
          12 => (shuosherdensityshock, noop),
          13 => (doublemachreflection, noop),
          1338 => (blasius, noop)
          )
