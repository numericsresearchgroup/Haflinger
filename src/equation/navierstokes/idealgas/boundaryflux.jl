"""
Routines to provide boundary conditions for the domain. Fills the boundary part of the fluxes list.

Available boundary conditions are:
 * 1   : Periodic boundary conditions (no work to be done here, are already filled due to mesh connection)
 DIRICHLET BCs:
 * 2   : Use the initial exact function (if BC state = 0) or a refstate as dirichlet boundary conditions
 * 12  : Read in dirichlet boundary conditions from a HDF5 file
 * 121 : Similar to 2, but pre-compute and store the evaluation of a exact func
 WALL BCs:
 * 3   : Adiabatic wall
 * 4   : Isothermal wall (Temperature specified by refstate)
 * 9   : Slip wall
 OUTFLOW BCs:
 * 23  : Outflow BC where the second entry of the refstate specifies the desired Mach number at the outflow
 * 24  : Pressure outflow BC (pressure specified by resfstate)
 * 25  : Subsonic outflow BC
 INFLOW BCs:
 * 27  : Subsonic inflow BC, WARNING: REFSTATE is different: Tt,alpha,beta,<empty>,pT (4th entry ignored!!), angles in DEG
"""

# TODO: Implement BCs by type as shown here
struct BCParameters
  name::String
  errormsg::String
  hasrefstate::Bool
  bcfunc::Function
end
abstract type BC end
struct IsothermalWall <: BC
  params::BCParameters
  temp::Float64
end


"""
Transform state into normal system
"""
@inline function rotatevelnormal!(uin,nv,t1,t2,un)
    @inbounds begin
        un[1] = uin[1]
        un[2] = sum(uin[2:4]*nv)
        un[3] = sum(uin[2:4]*t1)
        un[4] = sum(uin[2:4]*t2)
        un[5:end] = uin[5:end]
    end
    return
end

"""
Rotate state back to physical system
"""
@inline function rotatevelphysical!(u,nv,t1,t2)
    @inbounds begin
        u[2:4] = u[2]*nv + u[3]*t1 + u[4] * t2
    end
    return
end

"""
Isothermal wall
For isothermal wall, all gradients are from interior
We reconstruct the BC State, rho=rho_L, velocity=0, rhoE_wall =  rho_L*C_v*Twall
"""
@inline function isothermalwall!(uin,nv,t1,t2,uwall)
    @inbounds begin
        rotatevelnormal!(uin,nv,t1,t2,uwall)
        # Set pressure by solving local Riemann problem
        uwall[5]   = pressure_riemann(uwall)
        uwall[2:4] = 0. # no slip
        uwall[6]   = RefStatePrim(6,BCState) # temperature from RefState
        # set density via ideal gas equation, consistent to pressure and temperature
        uwall[1]   = uwall[5] / (uwall[6] * R)
        rotatevelphysical!(uwall,nv,t1,t2)
    end
    return
end

"""
Adiabatic wall
Diffusion: density=inside, velocity=0 (see below, after rotating back to physical space), rhoE=inside
For adiabatic wall all gradients are 0
We reconstruct the BC State, rho=rho_L, velocity=0, rhoE_wall = p_{RP}/( κ-1)
"""
@inline function adiabaticwall!(uin,nv,t1,t2,uwall)
    @inbounds begin
        rotatevelnormal!(uin,nv,t1,t2,uwall)
        # Set pressure by solving local Riemann problem
        uwall[5]   = pressure_riemann(uwall)
        uwall[2:4] = 0. # no slip
        #uwall[6]  adiabatic => temperature from the inside
        # set density via ideal gas equation, consistent to pressure and temperature
        uwall[1]   = uwall[5] / (uwall[6] * R)
        rotatevelphysical!(uwall,nv,t1,t2)
    end
    return
end

"""
Symmetry / Euler / slip wall
NOTE: from this state ONLY the velocities should actually be used for the diffusive flux
"""
@inline function symmetrywall!(uin,nv,t1,t2,uwall)
    @inbounds begin
        rotatevelnormal!(uin,nv,t1,t2,uwall)
        # Referring to Toro: Riemann Solvers and Numerical Methods for Fluid Dynamics (Chapter 6.3.3 Boundary Conditions)
        # the density is chosen from the inside
        # Set pressure by solving local Riemann problem
        uwall[5]   = pressure_riemann(uwall)
        uwall[2]   = 0. # slip in tangential directions
        # set temperature via ideal gas equation, consistent to density and pressure
        uwall[6]   = uwall[5] / (uwall[1] * R)
        rotatevelphysical!(uwall,nv,t1,t2)
    end
end

#"""
#Computes the boundary state for the different boundary conditions.
#"""
#function getboundarystate(sideid,t,nloc,uprim_boundary,uprim_master,normvec,tangvec1,tangvec2,face_xgp)
#
#BCType  = Boundarytype(BC(SideID),BC_TYPE)
#BCState = Boundarytype(BC(SideID),BC_STATE)
#
#SELECT CASE(BCType)
#CASE(2) #Exact function or refstate
#  if BCState == 0
#    for q=1:nlocZ; for p=0,Nloc
#      ExactFunc(IniExactFunc,t,Face_xGP[:,p,q),Cons)
#      ConsToPrim(UPrim_boundary[:,p,q),Cons)
#    end; end
#  ELSE
#    for q=1:nlocZ; for p=0,Nloc
#      UPrim_boundary[:,p,q) = RefStatePrim[:,BCState)
#    end; end
#  end
#CASE(12,121) # exact BC = Dirichlet BC #!
#  # SPECIAL BC: BCState uses readin state (12)
#  # SPECIAL BC: BCState uses Exact func computed once at the beginning (121)
#  # Dirichlet means that we use the gradients from inside the grid cell
#  UPrim_boundary = BCDataPrim[:,:,:,SideID)
#CASE(22) # exact BC = Dirichlet BC #!
#  # SPECIAL BC: BCState specifies exactfunc to be used!!
#  for q=1:nlocZ; for p=0,Nloc
#    ExactFunc(BCState,t,Face_xGP[:,p,q),Cons)
#    ConsToPrim(UPrim_boundary[:,p,q),Cons)
#  end; end
#
#
#
##  # Cases 21-29 are taken from NASA report "Inflow/Outflow Boundary Conditions with Application to FUN3D" Jan-Reneé Carlson
##  # and correspond to case BCs 2.1 - 2.9
##  # NOTE: quantities in paper are non-dimensional e.g. T=c^2
##  CASE(23) # Outflow mach number BC
##    # NOTE: Should not be used with adjacent walls (destroys boundary layer profile, like exact function)
##    # Refstate for this case is special, VelocityX specifies outlet mach number
##    # State: [dummy,Ma,dummy,dummy,dummy]
##    MaOut=RefStatePrim(2,BCState)
##    for q=1:nlocZ; for p=0,Nloc
##      c=SQRT(kappa*UPrim_boundary(5,p,q)/UPrim_boundary(1,p,q))
##      vmag=NORM2(UPrim_boundary(2:4,p,q))
##      Ma=vmag/c
##      cb=vmag/MaOut
##      if Ma<1
##        # use total pressure
##        pt=UPrim_boundary(5,p,q)*((1+0.5*(kappa-1)*Ma   *Ma)   **( kappa*sKappaM1))  # adiabatic/isentropic => unstable
##        #pt=prim(5)+0.5*prim(1)*vmag*vmag
##        pb=pt     *(1+0.5*(kappa-1)*MaOut*MaOut)**(-kappa*sKappaM1)
##      ELSE
##        # use total pressure for supersonic
##        pb=UPrim_boundary(5,p,q)+0.5*UPrim_boundary(1,p,q)*vmag*vmag
##      end
##      UPrim_boundary(1,p,q)=kappa*pb/(cb*cb)
##      UPrim_boundary(2:4,p,q)=UPrim_boundary(2:4,p,q)
##      UPrim_boundary(5,p,q)=pb
##      UPrim_boundary(6,p,q)=UPrim_boundary(5,p,q)/(R*UPrim_boundary(1,p,q))
##    end; end #p,q
##  CASE(24) # Pressure outflow BC
##    for q=1:nlocZ; for p=0,Nloc
##      # check if sub / supersonic (squared quantities)
##      c=kappa*UPrim_boundary(5,p,q)/UPrim_boundary(1,p,q)
##      vmag=SUM(UPrim_boundary(2:4,p,q)*UPrim_boundary(2:4,p,q))
##      # if subsonic use specified pressure, else use solution from the inside
##      if vmag<c
##        if BCState > 0
##          pb = RefStatePrim(5,BCState)
##        ELSE
##          absdiff = ABS(RefStatePrim(5,:) - UPrim_boundary(5,p,q))
##          pb = RefStatePrim(5,MINLOC(absdiff,1))
##        end
##        UPrim_boundary(1,p,q)=kappa*pb/c
##        UPrim_boundary(5,p,q)=pb
##        UPrim_boundary(6,p,q)=UPrim_boundary(5,p,q)/(R*UPrim_boundary(1,p,q))
##      end
##    end; end #p,q
##  CASE(25) # Subsonic outflow BC
##    for q=1:nlocZ; for p=0,Nloc
##      # check if sub / supersonic (squared quantities)
##      c=kappa*UPrim_boundary(5,p,q)/UPrim_boundary(1,p,q)
##      vmag=SUM(UPrim_boundary(2:4,p,q)*UPrim_boundary(2:4,p,q))
##      # if supersonic use total pressure to compute density
##      pb        = MERGE(UPrim_boundary(5,p,q)+0.5*UPrim_boundary(1,p,q)*vmag,RefStatePrim(5,BCState),vmag>=c)
##      UPrim_boundary(1,p,q)   = kappa*pb/c
##      # ensure outflow
##      UPrim_boundary(2:4,p,q) = MERGE(UPrim_boundary(2:4,p,q),SQRT(vmag)*NormVec[:,p,q),UPrim_boundary(2,p,q)>=0.)
##      UPrim_boundary(5,p,q)   = RefStatePrim(5,BCState) # always outflow pressure
##      UPrim_boundary(6,p,q)   = UPrim_boundary(5,p,q)/(R*UPrim_boundary(1,p,q))
##    end; end #p,q
##  CASE(27) # Subsonic inflow BC
##    # Refstate for this case is special
##    # State: [Density,nv1,nv2,nv3,Pressure]
##    # Compute temperature from density and pressure
##    # Nv specifies inflow direction of inflow:
##    # if ABS(nv)=0  then inflow vel is always in side normal direction
##    # if ABS(nv)!=0 then inflow vel is in global coords with nv specifying the direction
##    for q=1:nlocZ; for p=0,Nloc
##      # Prescribe Total Temp, Total Pressure, inflow angle of attack alpha
##      # and inflow yaw angle beta
##      # WARNING: REFSTATE is different: Tt,alpha,beta,<empty>,pT (4th entry ignored!!), angles in DEG not RAD
##      # Tt is computed by
##      # BC not from FUN3D Paper by JR Carlson (too many bugs), but from AIAA 2001 3882
##      # John W. Slater: Verification Assessment of Flow Boundary Conditions for CFD
##      # The BC State is described, not the outer state: use BC state to compute flux directly
##
##      Tt=RefStatePrim(1,BCState)
##      nv=RefStatePrim(2:4,BCState)
##      pt=RefStatePrim(5,BCState)
##      # Term A from paper with normal vector defined into the domain, dependent on p,q
##      A=SUM(nv(1:3)*(-1.)*NormVec(1:3,p,q))
##      # sound speed from inner state
##      c=SQRT(kappa*UPrim_boundary(5,p,q)/UPrim_boundary(1,p,q))
##      # 1D Riemann invariant: Rminus = Ui-2ci /kappamM1, Rminus = Ubc-2cb /kappaM1, normal component only!
##      Rminus=-UPrim_boundary(2,p,q)-2./KappaM1*c
##      # The Newton iteration for the T_b in the paper can be avoided by rewriting EQ 5 from the  paper
##      # not in T, but in sound speed -> quadratic equation, solve with PQ Formel (Mitternachtsformel is
##      # FORBIDDEN)
##      tmp1=(A**2*KappaM1+2.)/(Kappa*R*A**2*KappaM1)   #a
##      tmp2=2*Rminus/(Kappa*R*A**2)                    #b
##      tmp3=KappaM1*Rminus*Rminus/(2.*Kappa*R*A**2)-Tt #c
##      cb=(-tmp2+SQRT(tmp2**2-4*tmp1*tmp3))/(2*tmp1)   #
##      c=(-tmp2-SQRT(tmp2**2-4*tmp1*tmp3))/(2*tmp1)    # dummy
##      cb=MAX(cb,c)                                    # Following the FUN3D Paper, the max. of the two
##      # is the physical one...not 100. clear why
##      # compute static T  at bc from c
##      Tb=cb**2/(Kappa*R)
##      Ma=SQRT(2./KappaM1*(Tt/Tb-1.))
##      pb=pt*(1.+0.5*KappaM1*Ma**2)**(-kappa/kappam1)
##      U=Ma*SQRT(Kappa*R*Tb)
##      UPrim_boundary(1,p,q) = pb/(R*Tb)
##      UPrim_boundary(5,p,q) = pb
##
##      # we need the state in the global system for the diff fluxes
##      UPrim_boundary(2,p,q)=SUM(U*nv(1:3)*Normvec( 1:3,p,q))
##      UPrim_boundary(3,p,q)=SUM(U*nv(1:3)*Tangvec1(1:3,p,q))
##      UPrim_boundary(4,p,q)=SUM(U*nv(1:3)*Tangvec2(1:3,p,q))
##      UPrim_boundary(6,p,q)=UPrim_boundary(5,p,q)/(R*UPrim_boundary(1,p,q))
##    end; end #p,q
##  END SELECT
#
#end

function boundaryflux(sideid,t,nloc,flux,uprim_master,
                      gradux_master,graduy_master,graduz_master,
                      normvec,tangvec1,tangvec2,face_xgp)
end

#"""
#Computes the boundary fluxes for a given Cartesian mesh face (defined by SideID).
#Calls GetBoundaryState an directly uses the returned vales for all Riemann-type BCs.
#For other types of BCs, we directly compute the flux on the interface.
#"""
#function getboundaryflux(sideid,t,nloc,flux,uprim_master,
#                           gradux_master,graduy_master,graduz_master,
#                           normvec,tangvec1,tangvec2,face_xgp)
#
#  BCType  = Boundarytype(BC(SideID),BC_TYPE)
#  BCState = Boundarytype(BC(SideID),BC_STATE)
#
#  if BCType < 0 # testcase boundary condition
#    getboundaryfluxtestcase(sideid,t,nloc,flux,uprim_master,
#                                 gradux_master,graduy_master,graduz_master,
#                                 normvec,tangvec1,tangvec2,face_xgp)
#  else
#    getboundarystate(sideid,t,nloc,uprim_boundary,uprim_master,
#        normvec,tangvec1,tangvec2,face_xgp)
#
#    SELECT CASE(BCType)
#    CASE(2,12,121,22,23,24,25,27) # Riemann-Type BCs
#      for q=1:nlocZ; for p=0,Nloc
#        PrimToCons(UPrim_master[:,p,q), UCons_master[:,p,q))
#        PrimToCons(UPrim_boundary[:,p,q),      UCons_boundary[:,p,q))
#      end; end # p,q=1:n
#      Riemann(Nloc,Flux,UCons_master,UCons_boundary,UPrim_master,UPrim_boundary,
#          NormVec,TangVec1,TangVec2,doBC=true)
#    if USEPARABOLIC
#        viscousflux(nloc,fd_face_loc,uprim_master,uprim_boundary,
#             gradux_master,graduy_master,graduz_master,
#             gradux_master,graduy_master,graduz_master,
#             normvec
#        )
#        flux = flux + fd_face_loc
#    end
#
#    CASE(3,4,9) # Walls
#      for q=1:nlocZ; for p=0,Nloc
#        # Now we compute the 1D Euler flux, but use the info that the normal component u=0
#        # we directly tranform the flux back into the Cartesian coords: F=(0,n1*p,n2*p,n3*p,0)^T
#        Flux(1  ,p,q) = 0.
#        Flux(2:4,p,q) = UPrim_boundary(5,p,q)*NormVec[:,p,q)
#        Flux(5  ,p,q) = 0.
#      end; end #p,q
#      # Diffusion
#  if USEPARABOLIC
#      SELECT CASE(BCType)
#      CASE(3,4)
#        # Evaluate 3D Diffusion Flux with interior state and symmetry gradients
#        EvalDiffFlux2D(Nloc,Fd_Face_loc,Gd_Face_loc,Hd_Face_loc,UPrim_boundary,
#            gradUx_master, gradUy_master, gradUz_master
#        )
#        if BCType == 3
#          # Enforce energy flux is exactly zero at adiabatic wall
#          Fd_Face_loc(5,:,:)=0.
#          Gd_Face_loc(5,:,:)=0.
#          Hd_Face_loc(5,:,:)=0.
#        end
#      CASE(9)
#        # Euler/(full-)slip wall
#        # We prepare the gradients and set the normal derivative to zero (symmetry condition!)
#        # BCGradMat = I - n * n^T = (gradient -normal component of gradient)
#        for q=1:nlocZ; for p=0,Nloc
#          nv = NormVec[:,p,q)
#  if PP_dim==3)
#          BCGradMat(1,1) = 1. - nv(1)*nv(1)
#          BCGradMat(2,2) = 1. - nv(2)*nv(2)
#          BCGradMat(3,3) = 1. - nv(3)*nv(3)
#          BCGradMat(1,2) = -nv(1)*nv(2)
#          BCGradMat(1,3) = -nv(1)*nv(3)
#          BCGradMat(3,2) = -nv(3)*nv(2)
#          BCGradMat(2,1) = BCGradMat(1,2)
#          BCGradMat(3,1) = BCGradMat(1,3)
#          BCGradMat(2,3) = BCGradMat(3,2)
#          gradUx_Face_loc[:,p,q) = BCGradMat(1,1) * gradUx_master[:,p,q)
#                                 + BCGradMat(1,2) * gradUy_master[:,p,q)
#                                 + BCGradMat(1,3) * gradUz_master[:,p,q)
#          gradUy_Face_loc[:,p,q) = BCGradMat(2,1) * gradUx_master[:,p,q)
#                                 + BCGradMat(2,2) * gradUy_master[:,p,q)
#                                 + BCGradMat(2,3) * gradUz_master[:,p,q)
#          gradUz_Face_loc[:,p,q) = BCGradMat(3,1) * gradUx_master[:,p,q)
#                                 + BCGradMat(3,2) * gradUy_master[:,p,q)
#                                 + BCGradMat(3,3) * gradUz_master[:,p,q)
#  else
#          BCGradMat(1,1) = 1. - nv(1)*nv(1)
#          BCGradMat(2,2) = 1. - nv(2)*nv(2)
#          BCGradMat(1,2) = -nv(1)*nv(2)
#          BCGradMat(2,1) = BCGradMat(1,2)
#          gradUx_Face_loc[:,p,q) = BCGradMat(1,1) * gradUx_master[:,p,q)
#                                 + BCGradMat(1,2) * gradUy_master[:,p,q)
#          gradUy_Face_loc[:,p,q) = BCGradMat(2,1) * gradUx_master[:,p,q)
#                                 + BCGradMat(2,2) * gradUy_master[:,p,q)
#          gradUz_Face_loc[:,p,q) = 0.
#  end
#        end; end #p,q
#
#        # Evaluate 3D Diffusion Flux with interior state (with normalvel=0) and symmetry gradients
#        # Only velocities will be used from state (=inner velocities, except normal vel=0)
#        EvalDiffFlux2D(Nloc,Fd_Face_loc,Gd_Face_loc,Hd_Face_loc,UPrim_boundary,
#            gradUx_Face_loc,gradUy_Face_loc,gradUz_Face_loc
#        )
#      END SELECT
#
#      # Sum up Euler and Diffusion Flux
#      for iVar=2,nvar
#        Flux(iVar,:,:) = Flux(iVar,:,:)        +
#          NormVec(1,:,:)*Fd_Face_loc(iVar,:,:) +
#          NormVec(2,:,:)*Gd_Face_loc(iVar,:,:) +
#          NormVec(3,:,:)*Hd_Face_loc(iVar,:,:)
#      end # ivar
#  end
#end


#"""
#Computes the boundary fluxes for the lifting procedure for a given Cartesian mesh face (defined by SideID).
#"""
#function Lifting_GetBoundaryFlux(SideID,t,UPrim_master,Flux,NormVec,TangVec1,TangVec2,Face_xGP,SurfElem)
#
#BCType  = Boundarytype(BC(SideID),BC_TYPE)
#BCState = Boundarytype(BC(SideID),BC_STATE)
#
#if BCType < 0 # testcase boundary conditions
#  Lifting_GetBoundaryFluxTestcase(SideID,t,UPrim_master,Flux)
#ELSE
#  GetBoundaryState(SideID,t,n,UPrim_boundary,UPrim_master,
#                        NormVec,TangVec1,TangVec2,Face_xGP)
#  SELECT CASE(BCType)
#  CASE(2,12,121,22,23,24,25,27) # Riemann solver based BCs
#      Flux=0.5*(UPrim_master+UPrim_boundary)
#  CASE(3,4) # No-slip wall BCs
#    for q=1:nZ; for p=1:n
#      Flux(1  ,p,q) = UPrim_Boundary(1,p,q)
#      Flux(2:4,p,q) = 0.
#      Flux(5  ,p,q) = UPrim_Boundary(5,p,q)
#      Flux(6  ,p,q) = UPrim_Boundary(6,p,q)
#    end; end #p,q
#  CASE(9)
#    # Euler/(full-)slip wall, symmetry BC
#    # Solution from the inside with velocity normal component set to 0 (done in GetBoundaryState)
#    for q=1:nZ; for p=1:n
#      # Compute Flux
#      Flux(1            ,p,q) = UPrim_master(1,p,q)
#      Flux(2:4          ,p,q) = UPrim_boundary(2:4,p,q)
#      Flux(5:nvarPrim,p,q) = UPrim_master(5:nvarPrim,p,q)
#    end; end #p,q
#  CASE(1) #Periodic already filled!
#  CASE DEFAULT # unknown BCType
#    abort(__STAMP__,
#         "no BC defined in navierstokes/getboundaryflux.f90!")
#  END SELECT
#
#  #in case lifting is done in strong form
#  if !doWeakLifting  Flux=Flux-UPrim_master
#
#  for q=1:nZ; for p=1:n
#    Flux[:,p,q)=Flux[:,p,q)*SurfElem(p,q)
#  end; end
#end
#
#end



#"""
#Read in a HDF5 file containing the state for a boundary. Used in BC Type 12.
#"""
#function readbcflow(filename)
#
#  mprint("  Read BC state from file $filename")
#  opendatafile(filename,create=false,single=false,readonly=true)
#  getdataprops(nvarh5,nh5,nelemsh5,nodetypeh5)
#
#  if nelemsh5 ≠ nglobalelems
#    safeerror("Baseflow file does not match solution. Elements $nelemsh5")
#  end
#
#  uloc = zeros(nvar,nh5,nh5,nh5,nelems)
#  readarray("DG_Solution",5,[nvar,nh5+1,nh5+1,nh5Z+1,nElems],OffsetElem,5,RealArray=U_local)
#  closedatafile()
#
#  # Read in state
#  interpolatesolution=((nh5 ≠ n)  ||  (nodetypeh5) ≠ nodetype)))
#  if !interpolatesolution
#    # No interpolation needed, read solution directly from file
#    uvol=>uloc
#  else
#    # We need to interpolate the solution to the new computational grid
#    uvol = zeros(nvar,n,n,n,nelems)
#    #ALLOCATE(Vdm_NH5_N(n,nh5))
#    getvandermonde(nh5,nodetypeh5,n,nodetype,vdm_nh5_n,modal=true)
#
#    print("Interpolate base flow from restart grid with N=$nh5 to computational grid with N=$n")
#    for elemid=1:nelems
#      changebasisvolume(nvar,nh5,n,vdm_nh5_n,uloc[:,:,:,:,elemid],uvol[:,:,:,:,elemid])
#    end
#  end
#
#  # Prolong boundary state
#  for sideid=1,nbcsides
#    elemid  = sidetoelem(s2e_elem_id    ,sideid)
#    locside = sidetoelem(s2e_loc_side_id,sideid)
#
#    if nodetype == 1
#      evalelemface(nvar,n,u_n[:,:,:,:,elemid],uface,l_minus,l_plus,locside)
#    else
#      evalelemface(nvar,n,u_n[:,:,:,:,elemid],uface,locside)
#    end
#    for q=1:n; for p=1:n
#      bcdata[:,p,q,sideid]=uface[:,s2v2(1,p,q,0,locside),s2v2(2,p,q,0,locside)]
#      constoprim(bcdataprim[:,p,q,sideid],bcdata[:,p,q,sideid])
#    end; end
#  end
#
#  mprint("Done initializing BC state!")
#end

dummybc()=nothing

bcdefinition =  Dict{Integer,Tuple{Function,String,Bool,String}}(
      1  => ( dummybc,         "Periodic BC"           , false , ""),
      2  => ( dummybc,         "Exact function"        , true  , ""),
      22 => ( dummybc,         "Exact function2"       , true  , ""),
      3  => ( adiabaticwall!,  "Adiabatic wall"        , true  , ""),
      4  => ( isothermalwall!, "Isothermal wall"       , true  , "requires refstate for temperature!"),
      9  => ( symmetrywall!,   "Symmetry BC/Slip wall" , true  , ""),
      #23 => ( subsonicoutma!,  "Subsonic outflow"      , true  , "requires refstate for outflow Mach number (x,Ma,x,x,x)!"),
      #24 => ( pressureout!,    "Pressure outflow"      , true  , "requires refstate for outflow pressure!"),
      #25 => ( subsonicoutp!,   "Subsunic outflow"      , true  , "requires refstate for outflow pressure!"),
      #27 => ( subsonicin!,     "Subsonic inflow"       , true  , "requires refstate for inflow parameters (Tt,alpha,beta,empty,pT)!"),
      121=> ( dummybc,         "Precomputed exactfunc" , true  , "requires exact function!")
      )

"""
Initialize boundary conditions. Read parameters and sort boundary conditions by types.
Call boundary condition specific init routines.
"""
function initbc(params)

    # TODO: Add checks
    if !interpolationinitisdone ||  !meshinitisdone
        safeerror("InitBC not ready to be called or already called.")
    end

    # determine globally max MaxBCState
    maxbcstate = 0
    for i in Mesh.bcsides
        bc = bcs[mesh.sideprops[SP_BC, i]]
        if !(bc.bctype in [22,3,121])
            maxbcstate = max(maxbcstate,bc.state)
        end

        if !haskey(bctypes, bc.bctype)
            safeerror("BC type $(bc.bctypes) is unknown!")
        end
        bcdef = bcdict[bc.bctype]
        if bcdef[3] && bc.state < 1 # requires state
            safeerror(params[4]) # print error
        end
    end

    maxbcstateglobal = maxbcstate
    if USEMPI
        tmp = MPI.Allreduce(maxbcstateglobal, MPI.MAX, MPI.COMM_WORLD)
        maxbcstateglobal = tmp
    end

    # Sanity check for BCs
    if maxbcstate > size(refstateprim,2)
        safeerror("Boundary RefState not defined! (MaxBCState,nRefState): $maxbcstate, $nrefstate")
    end

    # TODO: Blasius
    #if USEPARABOLIC
    #  # Check for Blasius BCs and read parameters if this has not happened in the equation init
    #  if  !blasiusinitdone
    #     for i=1,nbcs
    #       loctype =boundarytype(i,bc_type)
    #       locstate=boundarytype(i,bc_state)
    #       if (loctype == 121)  (locstate == 1338)
    #         delta99_in      = getparameter(params,"delta99_in")
    #         x_in            = getparameter(params,"x_in",2,"[0.,0.]")
    #         blasiusinitdone = true
    #         exit
    #       end
    #     end
    #  end
    #end

    ## Allocate buffer array to store temp data for all BC sides
    #ALLOCATE(BCData(nvar,        n,n,nBCSides))
    #ALLOCATE(BCDataPrim(nvarPrim,n,n,nBCSides))
    #BCData=0.
    #BCDataPrim=0.
    #
    ## Initialize boundary conditions
    #readBCdone=false
    #for i=1,nBCs
    #  locType =BoundaryType(i,BC_TYPE)
    #  locState=BoundaryType(i,BC_STATE)
    #  SELECT CASE (locType)
    #  CASE(12) # State File Boundary condition
    #    if  !readBCdone) ReadBCFlow(BCStateFile)
    #    readBCdone=true
    #  CASE(27) # Subsonic inflow
    #    talpha=tan(acos(-1.)/180.*refstateprim(2,locstate))
    #    tbeta =tan(acos(-1.)/180.*refstateprim(3,locstate))
    #    # Compute vector a(1:3) from paper, the projection of the direction normal to the face normal
    #    # Multiplication of velocity magnitude by NORM2(a) gives contribution in face normal dir
    #    refstateprim(2,locstate)=1.    /sqrt((1.+talpha**2+tbeta**2))
    #    refstateprim(3,locstate)=talpha/sqrt((1.+talpha**2+tbeta**2))
    #    refstateprim(4,locstate)=tbeta /sqrt((1.+talpha**2+tbeta**2))
    #  END SELECT
    #end
    #
    ## Initialize Dirichlet BCs that use a pre-computed and then stored evaluation of an exact func
    #for iSide=1,nBCSides
    #  if Boundarytype(BC(iSide),BC_TYPE) == 121
    #    for q=1:nZ; for p=1:n
    #      ExactFunc(Boundarytype(BC(iSide),BC_STATE),0.,Face_xGP[:,p,q,0,iSide),BCData[:,p,q,iSide))
    #      ConsToPrim(BCDataPrim[:,p,q,iSide),BCData[:,p,q,iSide))
    #    end; end # p,q=1:n
    #  end
    #end

    ## Count number of sides of each boundary
    #ALLOCATE(nBCByType(nBCs))
    #nBCByType=0
    #for iSide=1,nBCSides
    #  for i=1,nBCs
    #    if BC(iSide) == i) nBCByType(i)=nBCByType(i)+1
    #  end
    #end
    #
    ## Sort BCs by type, store SideIDs
    #ALLOCATE(BCSideID(nBCs,MAXVAL(nBCByType)))
    #nBCByType=0
    #for iSide=1,nBCSides
    #  for i=1,nBCs
    #    if BC(iSide) == i
    #      nBCByType(i)=nBCByType(i)+1
    #      BCSideID(i,nBCByType(i))=iSide
    #    end
    #  end
    #end

end
