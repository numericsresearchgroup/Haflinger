"""
Compute Navier-Stokes fluxes using the conservative variables and derivatives for every volume Gauss point.
"""
@inline function advflux3d!(u::V,up::V,fx::V,fy::V,fz::V) where V <: AbstractVector{T} where T
  @fastmath @inbounds begin
  ep    = u[5] + up[5]
  # euler fluxes x-direction
  fx[1]=u[2]                 # ρ*u
  fx[2]=u[2]*up[2]+up[5]     # ρ*u²+p
  fx[3]=u[2]*up[3]           # ρ*u*v
  fx[4]=u[2]*up[4]           # ρ*u*w
  fx[5]=ep*up[2]             # [ρ*e+p]*u
  # euler fluxes y-direction
  fy[1]=u[3]                 # ρ*v
  fy[2]=fx[3]                # ρ*u*v
  fy[3]=u[3]*up[3]+up[5]     # ρ*v²+p
  fy[4]=u[3]*up[4]           # ρ*v*w
  fy[5]=ep*up[3]             # [ρ*e+p]*v
  # euler fluxes z-direction
  fz[1]=u[4]                 # ρ*v
  fz[2]=fx[4]                # ρ*u*w
  fz[3]=fy[4]                # ρ*v*w
  fz[4]=u[4]*up[4]+up[5]     # ρ*v²+p
  fz[5]=ep*up[4]             # [ρ*e+p]*w
  end
end
function advflux3d!(u::A,up::A,fx::A,fy::A,fz::A,dofs) where A <: AbstractArray{T,2} where  T
    @inbounds @views for i in dofs
       advflux3d!(u[:,i],up[:,i],fx[:,i],fy[:,i],fz[:,i])
    end
end
@inline  advflux3d!(u::A,up::A,fx::A,fy::A,fz::A     ) where A <: AbstractArray{T,2} where T =
 advflux3d!(u,up,fx,fy,fz,1:size(u,2))

const s43, s23 = 4/3, 2/3
const τ=zeros(FT,6)
const XX, YY, ZZ, XY, XZ, YZ = 1,2,3,4,5,6
"""
Compute Navier-Stokes diffusive fluxes using the conservative variables and derivatives for every volume Gauss point.
"""
@inline function diffflux3d!(up::V, gx::V, gy::V, gz::V, fx::V, fy::V, fz::V) where V <: AbstractVector{T} where T
    @inbounds begin
        μ      = viscosity(up[6])
        λ      = thermalconductivity(μ)
        τ[XX]  = μ * ( s43 * gx[2] - s23 * gy[3] - s23 * gz[4])
        τ[YY]  = μ * (-s23 * gx[2] + s43 * gy[3] - s23 * gz[4])
        τ[ZZ]  = μ * (-s23 * gx[2] - s23 * gy[3] + s43 * gz[4])
        τ[XY]  = μ * (gy[2] + gx[3])
        τ[XZ]  = μ * (gz[2] + gx[4])
        τ[YZ]  = μ * (gz[3] + gy[4])

        fx[1]  = 0.
        fx[2]  = -τ[XX]
        fx[3]  = -τ[XY]
        fx[4]  = -τ[XZ]
        fx[5]  = -τ[XX]*up[2] - τ[XY]*up[3] - τ[XZ]*up[4] - λ*gx[6]

        fy[1]  = 0.
        fy[2]  = -τ[XY]
        fy[3]  = -τ[YY]
        fy[4]  = -τ[YZ]
        fy[5]  = -τ[XY]*up[2] - τ[YY]*up[3] - τ[YZ]*up[4] - λ*gy[6]

        fz[1]  = 0.
        fz[2]  = -τ[XZ]
        fz[3]  = -τ[YZ]
        fz[4]  = -τ[ZZ]
        fz[5]  = -τ[XZ]*up[2] - τ[YZ]*up[3] - τ[ZZ]*up[4] - λ*gz[6]
    end
end
function diffflux3d!(up::A, gx::A, gy::A, gz::A, fx::A, fy::A, fz::A, dofs) where A <: AbstractArray{T,2} where  T
    @inbounds @views for i in dofs
        diffflux3d!(up[:,i], gx[:,i], gy[:,i], gz[:,i], fx[:,i], fy[:,i], fz[:,i])
    end
end
@inline  diffflux3d!(up::A, gx::A, gy::A, gz::A, fx::A, fy::A, fz::A      ) where A <: AbstractArray{T,2} where  T =
    diffflux3d!(up, gx, gy, gz, fx, fy, fz, 1:size(up,2))


"""
Computes 1D Euler flux using the primitive variables.
"""
@inline function eulerflux1d!(f::C,ue::E) where { C <: MVector{NVAR,T}, E <: MVector{NVARE,T} } where T
  # euler fluxes x-direction
  f[1] = ue[EMOM1]                       # ρ*u
  f[2] = ue[EMOM1]*ue[EVEL1] +ue[EPRES]  # ρ*u²+p
  f[3] = ue[EMOM1]*ue[EVEL2]             # ρ*u*v
  f[4] = ue[EMOM1]*ue[EVEL3]             # ρ*u*w
  f[5] =(ue[EENER]+ue[EPRES])*ue[EVEL1]  # (ρ*e+p)*u
  nothing
end
