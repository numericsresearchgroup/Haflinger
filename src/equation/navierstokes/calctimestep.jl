# Navier-Stokes calc timestep

using ...Globals
using ...Mesh

"""
Precompute some metrics used in calctimestep
"""
function initcalctimestep()
    metrics   = meshd.vol
    ndofs, nelems = size(metrics.f)[2:3]
    global metricsadv = similar(metrics.f)
    @views for iel = 1:nelems, d = 1:ndofs
        metricsadv[1,d,iel] = metrics.sjac[1,d,iel] * norm(metrics.f[:,d,iel])
        metricsadv[2,d,iel] = metrics.sjac[1,d,iel] * norm(metrics.g[:,d,iel])
        metricsadv[3,d,iel] = metrics.sjac[1,d,iel] * norm(metrics.h[:,d,iel])
    end
    if USEPARABOLIC
        global metricsvisc = similar(metrics.f)
        kmax = max(4 / 3, eosvars.κ / eosvars.Pr)
        @views for iel = 1:nelems, d = 1:ndofs
            metricsvisc[1,d,iel] = kmax * sum((metrics.f[:,d,iel] * metrics.sjac[1,d,iel]).^2)
            metricsvisc[2,d,iel] = kmax * sum((metrics.g[:,d,iel] * metrics.sjac[1,d,iel]).^2)
            metricsvisc[3,d,iel] = kmax * sum((metrics.h[:,d,iel] * metrics.sjac[1,d,iel]).^2)
        end
    end
end


"""
Compute the time step for the current update of U for the Navier-Stokes-Equations
"""
function calctimestep(uprim::AbstractArray{T}, dtelem::Array{T,2}) where T <: AbstractFloat
    fill!(dtelem, typemax(T))
    dta, dtv = view(dtelem, :, 1), view(dtelem, :, 2)

    nanfound = false
    @views for iel = 1:nelems
        if any(isnan.(uprim[:,:,iel]))
            @warn "Solution is NaN in element $iel."
            nanfound = true
        end
    end
    if nanfound
        return -1., -1., -4
    end

    err = 0
    getadvectiontimestep!(uprim, dta, metricsadv)
    nans = findall(isnan.(dta))
    if length(nans) > 0
        @warn "Advection timestep NaN on proc $myrank in elements:\n$nans"
        err -= 1
    end
    timestepadv = minimum(dta)

    if USEPARABOLIC
        getdiffusiontimestep!(uprim, dtv, metricsvisc)
        nans = findall(isnan.(dtv))
        if length(nans) > 0
            @warn "Viscous timestep NaN on proc $myrank in elements:\n$nans"
            err -= 2
        end
        timestepdiff = minimum(dtv)
    else
        timestepdiff = typemax(T)
    end
    timestepadv, timestepdiff, err
end
function calctimestep(calc, dtelem, mem)
    u2, uprim2, uprimd = r2(calc.u.vol), r2(calc.uprim.vol), rd(calc.uprim.vol)
    constoprim!(u2, uprim2)
    timestepadv, timestepdiff, err = calctimestep(uprimd, dtelem)
    dogc = needsgc(mem)
    if USEMPI
        buf = [timestepadv; timestepdiff; err; -dogc] # negative errtype due to mpi_min
        timestepadv, timestepdiff, tmperr, tmpgc = MPI.allreduce(buf, MPI.MIN, MPI.COMM_WORLD)
        err, dogc = Int(tmperr), Bool(-tmpgc)
    end
    timestepadv, timestepdiff, err, dogc
end

const λe   = zeros(FT, DIM)
const λmax = zeros(FT, DIM)
const up   = zeros(FT, NVARLIFT)

const A3 = Array{FT,3}
function getadvectiontimestep!(uprim::AbstractArray{T,3}, dtelem::AbstractVector{T}, ma::AbstractArray{T,3}) where { T <: AbstractFloat }
    ndofs, nelems = size(uprim)[2:3]
    m = meshd.vol
    mf::A3, mg::A3, mh::A3, sjac::A3 = m.f, m.g, m.h, m.sjac

    @inbounds @views for iel = 1:nelems
        λmax[1] = λmax[2] = λmax[3] = 0.
        for i = 1:ndofs
            c = speedofsound(uprim[:,i,iel])
            λe[1] = λe[2] = λe[3] = 0.
            for d=1:DIM
                vsj   = uprim[d+1,i,iel] * sjac[1,i,iel]
                λe[1] += mf[d,i,iel] * vsj
                λe[2] += mg[d,i,iel] * vsj
                λe[3] += mh[d,i,iel] * vsj
            end
            for d=1:DIM
                λmax[d] = max(λmax[d], abs(λe[d]) + c * ma[d,i,iel])
            end
        end
        dtelem[iel] = 2 / sum(λmax)
    end
end

"""
Compute the diffusion time step
"""
function getdiffusiontimestep!(uprim::AbstractArray{T,3}, dtelem::AbstractVector{T}, mv::AbstractArray{T,3}) where { T <: AbstractFloat }
    ndofs, nelems = size(uprim)[2:3]

    @inbounds @views for ielem = 1:nelems
        λmax[1] = λmax[2] = λmax[3] = 0.
        for i in 1:ndofs
            μ = viscosity(uprim[TEMP,i,ielem])
            for d = 1:DIM
                λmax[d] = max(λmax[d], μ / uprim[1,i,ielem] * mv[d,i,ielem])
            end
        end
        if sum(λmax) > 0
            dtelem[ielem] = min(dtelem[ielem], 4 / sum(λmax))
        end
    end
end
