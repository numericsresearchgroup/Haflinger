###
# The module implementing Navier-Stokes fluxes, Riemann solvers and analysis routines
###
module Navierstokes

using ...Haflinger

# TODO: add support for multiple EOS
# Here come the equation systems, at the moment we have only ideal gas
include("idealgas/idealgas.jl")


include("equation_vars.jl")
include("calctimestep.jl")
include("flux.jl")
include("riemann.jl")

export NVAR, NVARLIFT, USEPRIM, EOS
export exactfunc, getboundaryflux, riemann!, iniexactfunc, inirefstate, eddyviscosity
export docalcsource
export calctimestep
export advflux3d!, diffflux3d!, eulerflux1d!
export refstateprim, refstatecons

eosdict = Dict{String,Module}("idealgas" => Idealgas)

const EOS = Idealgas

using .EOS

"""
Set parameters needed by equation modules and initialize equations as well as boundary conditions and testcases
"""
function initequation(params)
    initstart("NAVIER-STOKES")

    # Always set docalcsource true, set false by calcsource itself on first run if not needed
    global docalcsource = true

    eostype = getparameter(params, "eos" ; required = false, default = "idealgas" )
    if haskey(eosdict, eostype)
        global EOS = eosdict[eostype]
    else
        safeerror("Unknown equation of state: $eostype")
    end
    EOS.initeos(params)

    # Read in boundary parameters
    global inirefstate     = zeros(FT,NVAR)
    global inirefstateprim = zeros(FT,NVARLIFT)
    iniref = getparameter(params, "inirefstate" ; required = true )
    EOS.primtocons!(iniref, inirefstate)
    EOS.constoprim!(inirefstate, inirefstateprim)

    EOS.initexactfunc(params)

    #TODO: decide whether inirefstate should be index, actual state or something else??!
    ## Read Boundary information / RefStates / perform sanity check
    #if inirefstate > nrefstate
    #  error("ERROR: Ini not defined# (Ini,nRefState): $inirefstate,$nrefstate")
    #end

    refstatein = getparameter(params, "refstates" ; required = true )
    global refstateprim = zeros(FT, NVARLIFT, size(refstatein, 1))
    global refstatecons = zeros(FT, NVAR,     size(refstatein, 1))
    refstateprim[1:NVAR, : ] = refstatein' # temperature not filled yet in prims!
    EOS.primtocons!(refstateprim, refstatecons)
    EOS.constoprim!(refstatecons, refstateprim)

  # boundary state filename if present
    bcstatefile = getparameter(params, "bcstatefile" ; required = false )

  # Initialize Riemann solvers to be in volume and on BCs
    initriemann(params)

  # Initialize timestep calculation
    initcalctimestep()

    if eddyviscosity
        # Initialize eddyViscosity
        safeerror("eddyvisc")
       #initeddyvisc(params)
    end

    if USESPLIT
        # Initialize SplitDG
        safeerror("split")
        #initsplitdg()
    end

    EOS.initbc(params)

    equationinitisdone = true
    initdone("NAVIER-STOKES")

    # Initialize current testcase
    minfo("We don't want any testcases yet")
    #inittestcase(params)

    global exactfunc    = EOS.exactfunc
    global boundaryflux = EOS.boundaryflux
    global iniexactfunc = EOS.iniexactfunc
end

"""
Finalizes equation, calls finalize for testcase and Riemann
"""
function finalizeequation()
    finalizetestcase()
    finalizeriemann()
    finalizecalctimestep()
    if eddyviscosity
        finalizeeddyvisc()
    end
    finalizebc()
    equationinitisdone = false
end

end
