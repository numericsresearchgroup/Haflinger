
#"""
#Computes the viscous NSE diffusion fluxes in all directions to approximate the numerical flux
#Actually not a Riemann solver...
#"""
#function viscousflux(f,upriml,uprimr,graduxl,graduyl,graduzl,graduxr,graduyr,graduzr,nv)
#  # Don't forget the diffusion contribution, my young padawan
#  # Compute NSE Diffusion flux
#  evaldiffflux2d(difffluxxl,difffluxyl,difffluxzl,upriml,graduxl,graduyl,graduzl)
#  evaldiffflux2d(difffluxxr,difffluxyr,difffluxzr,uprimr,graduxr,graduyr,graduzr)
#  # BR1 uses arithmetic mean of the fluxes
#  @inbounds for i=1:length(upriml)
#    f[:,i]=0.5*(nv[1,i]*(difffluxxl[CONS,i]+difffluxxr[CONS,i]) &
#               +nv[2,i]*(difffluxyl[CONS,i]+difffluxyr[CONS,i]) &
#               +nv[3,i]*(difffluxzl[CONS,i]+difffluxzr[CONS,i]))
#  end
#end

export riemann!,w

#"""
#Local Lax-Friedrichs (Rusanov) Riemann solver
#"""
@inline function lf!(f,fl,fr,ul,ur)
  λmax = max( abs(ur[EVEL1]),abs(ul[EVEL1]) ) + max( speedofsounde(ul),speedofsounde(ur) )
  @inbounds @simd for i=1:NVAR
      f[i] = 0.5*((fl[i]+fr[i]) - λmax*(ur[i] - ul[i]))
  end
end
@inline function lfsplit!(f,ul,ur)
  λmax = max( abs(ur[EVEL1]),abs(ul[EVEL1]) ) + max( speedofsounde(ul),speedofsounde(ur) )
  # get split flux
  splitdgsurfacefunc(ul,ur,f)
  @inbounds @simd for i=1:NVAR
    f[i] -= 0.5*λmax*(ur[i] - ul[i])
  end
end


"""
Riemann solver using purely the average fluxes
"""
function fluxaverage!(ull,urr,f)
  # get split flux
  splitdgsurfacefunc(ull,urr,f)
end


"""
Harten-Lax-Van-Leer Riemann solver resolving contact discontinuity
"""
function hllc!(fl,fr,ull,urr,f)
##REAL    :: H_L,H_R
##REAL    :: SqrtRho_L,SqrtRho_R,sSqrtRho
###REAL   :: RoeVel(3),RoeH,Roec,absVel
##REAL    :: Ssl,Ssr,SStar
##REAL    :: U_Star(PP_nVar),EStar
##REAL    :: sMu_L,sMu_R
#
#  # Compute Roe mean values (required for all but LF)
#  h_l       = totalenthalpy_he(ull)
#  h_r       = totalenthalpy_he(urr)
#  sqrtrho_l = sqrt(ull(dens))
#  sqrtrho_r = sqrt(urr(dens))
#  ssqrtrho  = 1./(sqrtrho_l+sqrtrho_r)
#  # HLLC flux
#  # Basic Davis estimate for wave speed
#  ssl       = ull(vel1) - speedofsound_he(ull)
#  ssr       = urr(vel1) + speedofsound_he(urr)
#  # Better Roe estimate for wave speeds Davis, Einfeldt
#  # Roe mean values
#  #RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#  #RoeH      = (SqrtRho_R*H_R   + SqrtRho_L*H_L)   * sSqrtRho
#  #absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#  #Roec      = SQRT(kappaM1*(RoeH-0.5*absVel))
#  #Ssl = RoeVel(1) - Roec
#  #Ssr = RoeVel(1) + Roec
#
#  # positive supersonic speed
#  if ssl ≥ 0
#    F=fl
#  # negative supersonic speed
#  elseif Ssr ≤ 0
#    f=fr
#  # subsonic case
#  else
#    smu_l = ssl - ull[vel1]
#    smu_r = ssr - urr[vel1]
#    sstar = (urr(pres) - ull(pres) + ull(mom1)*smu_l - urr(mom1)*smu_r) / (ull(dens)*smu_l - urr(dens)*smu_r)
#    if ssl ≤ 0 && sstar ≥ 0
#      estar  = totalenergy_he(ull) + (sstar-ull(vel1))*(sstar + ull(pres)*ull(srho)/smu_l)
#      u_star = ull(dens) * smu_l/(ssl-sstar) * (/ 1, sstar, ull(vel2:vel3), estar /)
#      f=fl+ssl*(u_star-ull(cons))
#    else
#      estar  = totalenergy_he(urr) + (sstar-urr(vel1))*(sstar + urr(pres)*urr(srho)/smu_r)
#      u_star = urr(dens) * smu_r/(ssr-sstar) * (/ 1, sstar, urr(vel2:vel3), estar /)
#      f=fr+ssr*(u_star-urr(cons))
#    end
#  end # subsonic case
end


"""
Roe's approximate Riemann solver
"""
function roe!(fl,fr,ull,urr,f)
##REAL                    :: H_L,H_R
##REAL                    :: SqrtRho_L,SqrtRho_R,sSqrtRho
##REAL                    :: RoeVel(3),RoeH,Roec,absVel
##REAL,DIMENSION(PP_nVar) :: a,r1,r2,r3,r4,r5  # Roe eigenvectors
##REAL                    :: Alpha1,Alpha2,Alpha3,Alpha4,Alpha5,Delta_U(PP_nVar+1)
#
#  # Roe flux
#  h_l       = totalenthalpy_he(ull)
#  h_r       = totalenthalpy_he(urr)
#  sqrtrho_l = sqrt(ull(dens))
#  sqrtrho_r = sqrt(urr(dens))
#
#  ssqrtrho  = 1/(sqrtrho_l+sqrtrho_r)
#  # Roe mean values
#  roevel    = (sqrtrho_r*urr(velv) + sqrtrho_l*ull(velv)) * ssqrtrho
#  absvel    = dot_product(roevel,roevel)
#  roeh      = (sqrtrho_r*h_r+sqrtrho_l*h_l) * ssqrtrho
#  roec      = roec_riemann_h(roeh,roevel)
#
#  # mean eigenvalues and eigenvectors
#  a  = [ roevel[1]-roec, roevel[1], roevel[1], roevel[1], roevel[1]+roec      ]
#  r1 = [ 1,              a[1],      roevel[2], roevel[3], roeh-roevel[1]*roec ]
#  r2 = [ 1,              roevel[1], roevel[2], roevel[3], 0.5*absvel          ]
#  r3 = [ 0,              0,         1,         0,         roevel[2]           ]
#  r4 = [ 0,              0,         0,         1,         roevel[3]           ]
#  r5 = [ 1,              a[5],      roevel[2], roevel[3], roeh+roevel[1]*roec ]
#
#  # calculate differences
#  delta_u(1:5) = urr(cons) - ull(cons)
#  delta_u(6)   = delta_u(5)-(delta_u(3)-roevel(2)*delta_u(1))*roevel(2) - (delta_u(4)-roevel(3)*delta_u(1))*roevel(3)
#  # calculate factors
#  alpha3 = delta_u(3) - roevel(2)*delta_u(1)
#  alpha4 = delta_u(4) - roevel(3)*delta_u(1)
#  alpha2 = alpha2_riemann_h(roeh,roevel,roec,delta_u)
#  alpha1 = 0.5/roec * (delta_u(1)*(roevel(1)+roec) - delta_u(2) - roec*alpha2)
#  alpha5 = delta_u(1) - alpha1 - alpha2
#  #ifndef SPLIT_DG
#  # assemble Roe flux
#  f=0.5*((fl+fr) - &
#         alpha1*abs(a(1))*r1 - &
#         alpha2*abs(a(2))*r2 - &
#         alpha3*abs(a(3))*r3 - &
#         alpha4*abs(a(4))*r4 - &
#         alpha5*abs(a(5))*r5)
#  #else
#  # get split flux
#  call splitdgsurface_pointer(ull,urr,f)
#  # assemble Roe flux
#  f = f - 0.5*(alpha1*abs(a(1))*r1 + &
#               alpha2*abs(a(2))*r2 + &
#               alpha3*abs(a(3))*r3 + &
#               alpha4*abs(a(4))*r4 + &
#               alpha5*abs(a(5))*r5)
#  #end /*SPLIT_DG*/
end
function roesplit!(fl,fr,ull,urr,f)
end


"""
Roe's approximate Riemann solver using the Hartman and Hymen II entropy fix
"""
function roeentropyfix!(fl,fr,ull,urr,f)
#REAL                    :: c_L,c_R
#REAL                    :: H_L,H_R
#REAL                    :: SqrtRho_L,SqrtRho_R,sSqrtRho,absVel
#REAL                    :: RoeVel(3),RoeH,Roec,RoeDens
#REAL,DIMENSION(PP_nVar) :: r1,r2,r3,r4,r5,a,al,ar,Delta_U,Alpha  # Roe eigenvectors
#REAL                    :: tmp,da

#c_L       = SPEEDOFSOUND_HE(ull)
#c_R       = SPEEDOFSOUND_HE(urr)
#H_L       = TOTALENTHALPY_HE(ull)
#H_R       = TOTALENTHALPY_HE(urr)
#SqrtRho_L = SQRT(ull(DENS))
#SqrtRho_R = SQRT(urr(DENS))
#
#sSqrtRho  = 1./(SqrtRho_L+SqrtRho_R)
## Roe mean values
#RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#RoeH      = (SqrtRho_R*H_R+SqrtRho_L*H_L) * sSqrtRho
#absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#Roec      = ROEC_RIEMANN_H(RoeH,RoeVel)
#RoeDens   = SQRT(ull(DENS)*urr(DENS))
## Roe+Pike version of Roe Riemann solver
#
## calculate jump
#Delta_U(1)   = urr(DENS) - ull(DENS)
#Delta_U(2:4) = urr(VELV) - ull(VELV)
#Delta_U(5)   = urr(PRES) - ull(PRES)
#
## mean eigenvalues and eigenvectors
#a  = (/ RoeVel(1)-Roec, RoeVel(1), RoeVel(1), RoeVel(1), RoeVel(1)+Roec      /)
#r1 = (/ 1.,             a(1),      RoeVel(2), RoeVel(3), RoeH-RoeVel(1)*Roec /)
#r2 = (/ 1.,             RoeVel(1), RoeVel(2), RoeVel(3), 0.5*absVel          /)
#r3 = (/ 0.,             0.,        1.,        0.,        RoeVel(2)           /)
#r4 = (/ 0.,             0.,        0.,        1.,        RoeVel(3)           /)
#r5 = (/ 1.,             a(5),      RoeVel(2), RoeVel(3), RoeH+RoeVel(1)*Roec /)
#
## calculate wave strenghts
#tmp      = 0.5/(Roec*Roec)
#Alpha(1) = tmp*(Delta_U(5)-RoeDens*Roec*Delta_U(2))
#Alpha(2) = Delta_U(1) - Delta_U(5)*2.*tmp
#Alpha(3) = RoeDens*Delta_U(3)
#Alpha(4) = RoeDens*Delta_U(4)
#Alpha(5) = tmp*(Delta_U(5)+RoeDens*Roec*Delta_U(2))
#
## Harten+Hyman entropy fix (apply only for acoustic waves, don't fix r)
#
#al(1) = ull(VEL1) - c_L
#al(2) = ull(VEL1)
#al(3) = ull(VEL1)
#al(4) = ull(VEL1)
#al(5) = ull(VEL1) + c_L
#ar(1) = urr(VEL1) - c_R
#ar(2) = urr(VEL1)
#ar(3) = urr(VEL1)
#ar(4) = urr(VEL1)
#ar(5) = urr(VEL1) + c_R
## HH1
##if ABS(a(1)).LT.da1) a(1)=da1
##if ABS(a(5)).LT.da5) a(5)=da5
## HH2
#for iVar=1,5
#  da = MAX(0.,a(iVar)-al(iVar),ar(iVar)-a(iVar))
#
#  if ABS(a(iVar)).LT.da) THEN
#    a(iVar)=0.5*(a(iVar)*a(iVar)/da+da)
#  ELSE
#    a(iVar) = ABS(a(iVar))
#  end
#end
#
##ifndef SPLIT_DG
## assemble Roe flux
#F=0.5*((fl+fr)        - &
#       Alpha(1)*a(1)*r1 - &
#       Alpha(2)*a(2)*r2 - &
#       Alpha(3)*a(3)*r3 - &
#       Alpha(4)*a(4)*r4 - &
#       Alpha(5)*a(5)*r5)
##else
## get split flux
#CALL SplitDGSurface_pointer(ull,urr,F)
## for KG or PI flux eigenvalues have to be altered to ensure consistent KE dissipation
## assemble Roe flux
#F= F - 0.5*(Alpha(1)*a(1)*r1 + &
#            Alpha(2)*a(2)*r2 + &
#            Alpha(3)*a(3)*r3 + &
#            Alpha(4)*a(4)*r4 + &
#            Alpha(5)*a(5)*r5)
##end /*SPLIT_DG*/
end
function roeentropyfixsplit!(fl,fr,ull,urr,f)
end


"""
Low mach number Roe's approximate Riemann solver according to Oßwald(2015)
"""
function roel2!(fl,fr,ull,urr,f)
#REAL                    :: H_L,H_R
#REAL                    :: SqrtRho_L,SqrtRho_R,sSqrtRho
#REAL                    :: RoeVel(3),RoeH,Roec,absVel
#REAL                    :: Ma_loc # local Mach-Number
#REAL,DIMENSION(PP_nVar) :: a,r1,r2,r3,r4,r5  # Roe eigenvectors
#REAL                    :: Alpha1,Alpha2,Alpha3,Alpha4,Alpha5,Delta_U(PP_nVar+1)

## Roe flux
#H_L       = TOTALENTHALPY_HE(ull)
#H_R       = TOTALENTHALPY_HE(urr)
#SqrtRho_L = SQRT(ull(DENS))
#SqrtRho_R = SQRT(urr(DENS))
#
#sSqrtRho  = 1./(SqrtRho_L+SqrtRho_R)
## Roe mean values
#RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#RoeH      = (SqrtRho_R*H_R+SqrtRho_L*H_L) * sSqrtRho
#Roec      = ROEC_RIEMANN_H(RoeH,RoeVel)
#
## mean eigenvalues and eigenvectors
#a  = (/ RoeVel(1)-Roec, RoeVel(1), RoeVel(1), RoeVel(1), RoeVel(1)+Roec      /)
#r1 = (/ 1.,             a(1),      RoeVel(2), RoeVel(3), RoeH-RoeVel(1)*Roec /)
#r2 = (/ 1.,             RoeVel(1), RoeVel(2), RoeVel(3), 0.5*absVel          /)
#r3 = (/ 0.,             0.,        1.,        0.,        RoeVel(2)           /)
#r4 = (/ 0.,             0.,        0.,        1.,        RoeVel(3)           /)
#r5 = (/ 1.,             a(5),      RoeVel(2), RoeVel(3), RoeH+RoeVel(1)*Roec /)
#
## calculate differences
#Delta_U(1:5) = urr(CONS) - ull(CONS)
#Delta_U(6)   = Delta_U(5)-(Delta_U(3)-RoeVel(2)*Delta_U(1))*RoeVel(2) - (Delta_U(4)-RoeVel(3)*Delta_U(1))*RoeVel(3)
#
## low Mach-Number fix
#Ma_loc = SQRT(absVel)/(Roec*SQRT(kappa))
#Delta_U(2:4) = Delta_U(2:4) * Ma_loc
#
## calculate factors
#Alpha3 = Delta_U(3) - RoeVel(2)*Delta_U(1)
#Alpha4 = Delta_U(4) - RoeVel(3)*Delta_U(1)
#Alpha2 = ALPHA2_RIEMANN_H(RoeH,RoeVel,Roec,Delta_U)
#Alpha1 = 0.5/Roec * (Delta_U(1)*(RoeVel(1)+Roec) - Delta_U(2) - Roec*Alpha2)
#Alpha5 = Delta_U(1) - Alpha1 - Alpha2
#
##ifndef SPLIT_DG
## assemble Roe flux
#F=0.5*((fl+fr) - &
#       Alpha1*ABS(a(1))*r1 - &
#       Alpha2*ABS(a(2))*r2 - &
#       Alpha3*ABS(a(3))*r3 - &
#       Alpha4*ABS(a(4))*r4 - &
#       Alpha5*ABS(a(5))*r5)
##else
## get split flux
#CALL SplitDGSurface_pointer(ull,urr,F)
## assemble Roe flux
#F = F - 0.5*(Alpha1*ABS(a(1))*r1 + &
#             Alpha2*ABS(a(2))*r2 + &
#             Alpha3*ABS(a(3))*r3 + &
#             Alpha4*ABS(a(4))*r4 + &
#             Alpha5*ABS(a(5))*r5)
##end /*SPLIT_DG*/
end


"""
Standard Harten-Lax-Van-Leer Riemann solver without contact discontinuity
"""
function hll!(fl,fr,ull,urr,f)
#REAL    :: H_L,H_R
#REAL    :: SqrtRho_L,SqrtRho_R,sSqrtRho,absVel
#REAL    :: RoeVel(3),RoeH,Roec
#REAL    :: Ssl,Ssr

#H_L       = TOTALENTHALPY_HE(ull)
#H_R       = TOTALENTHALPY_HE(urr)
#SqrtRho_L = SQRT(ull(DENS))
#SqrtRho_R = SQRT(urr(DENS))
#sSqrtRho  = 1./(SqrtRho_L+SqrtRho_R)
## Roe mean values
#RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#RoeH      = (SqrtRho_R*H_R        + SqrtRho_L*H_L)        * sSqrtRho
#absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#Roec      = ROEC_RIEMANN_H(RoeH,RoeVel)
## HLL flux
## Basic Davis estimate for wave speed
##Ssl = ull(VEL1) - c_L
##Ssr = urr(VEL1) + c_R
## Better Roe estimate for wave speeds Davis, Einfeldt
#Ssl = RoeVel(1) - Roec
#Ssr = RoeVel(1) + Roec
## positive supersonic speed
#if Ssl .GE. 0.
#  F=fl
## negative supersonic speed
#ELSEif Ssr .LE. 0.
#  F=fr
## subsonic case
#ELSE
#  F=(Ssr*fl-Ssl*fr+Ssl*Ssr*(urr(CONS)-ull(CONS)))/(Ssr-Ssl)
#end # subsonic case
end



"""
Harten-Lax-Van-Leer-Einfeldt Riemann solver
"""
function hlle!(fl,fr,ull,urr,f)
#REAL    :: H_L,H_R
#REAL    :: SqrtRho_L,SqrtRho_R,sSqrtRho,absVel
#REAL    :: RoeVel(3),RoeH,Roec
#REAL    :: Ssl,Ssr,beta

#H_L       = TOTALENTHALPY_HE(ull)
#H_R       = TOTALENTHALPY_HE(urr)
#SqrtRho_L = SQRT(ull(DENS))
#SqrtRho_R = SQRT(urr(DENS))
#sSqrtRho  = 1./(SqrtRho_L+SqrtRho_R)
## Roe mean values
#RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#RoeH      = (SqrtRho_R*H_R        + SqrtRho_L*H_L)        * sSqrtRho
#absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#Roec      = ROEC_RIEMANN_H(RoeH,RoeVel)
## HLLE flux (positively conservative)
#beta=BETA_RIEMANN_H()
#SsL=MIN(RoeVel(1)-Roec,ull(VEL1) - beta*SPEEDOFSOUND_HE(ull), 0.)
#SsR=MAX(RoeVel(1)+Roec,urr(VEL1) + beta*SPEEDOFSOUND_HE(urr), 0.)
#
## positive supersonic speed
#if Ssl .GE. 0.
#  F=fl
## negative supersonic speed
#ELSEif Ssr .LE. 0.
#  F=fr
## subsonic case
#ELSE
#  F=(Ssr*fl-Ssl*fr+Ssl*Ssr*(urr(CONS)-ull(CONS)))/(Ssr-Ssl)
#end # subsonic case
end


"""
Harten-Lax-Van-Leer-Einfeldt-Munz Riemann solver
"""
function hllem!(fl,fr,ull,urr,f)
#REAL                                   :: H_L,H_R
#REAL                                   :: SqrtRho_L,SqrtRho_R,sSqrtRho,absVel
#REAL                                   :: RoeVel(3),RoeH,Roec,RoeDens
#REAL                                   :: Ssl,Ssr
#REAL                                   :: Alpha(2:4),delta,beta
#REAL,DIMENSION(PP_nVar)                :: r2,r3,r4  # Roe eigenvectors + jump in prims

#H_L       = TOTALENTHALPY_HE(ull)
#H_R       = TOTALENTHALPY_HE(urr)
#SqrtRho_L = SQRT(ull(DENS))
#SqrtRho_R = SQRT(urr(DENS))
#sSqrtRho  = 1./(SqrtRho_L+SqrtRho_R)
## Roe mean values
#RoeVel    = (SqrtRho_R*urr(VELV) + SqrtRho_L*ull(VELV)) * sSqrtRho
#RoeH      = (SqrtRho_R*H_R        + SqrtRho_L*H_L)        * sSqrtRho
#absVel    = DOT_PRODUCT(RoeVel,RoeVel)
#Roec      = ROEC_RIEMANN_H(RoeH,RoeVel)
#RoeDens   = SQRT(ull(DENS)*urr(DENS))
## HLLEM flux (positively conservative)
#beta=BETA_RIEMANN_H()
#SsL=MIN(RoeVel(1)-Roec,ull(VEL1) - beta*SPEEDOFSOUND_HE(ull), 0.)
#SsR=MAX(RoeVel(1)+Roec,urr(VEL1) + beta*SPEEDOFSOUND_HE(urr), 0.)
#
## positive supersonic speed
#if Ssl .GE. 0.
#  F=fl
## negative supersonic speed
#ELSEif Ssr .LE. 0.
#  F=fr
## subsonic case
#ELSE
#  # delta
#  delta = Roec/(Roec+ABS(0.5*(Ssl+Ssr)))
#
#  # mean eigenvectors
#  Alpha(2)   = (urr(DENS)-ull(DENS))  - (urr(PRES)-ull(PRES))/(Roec*Roec)
#  Alpha(3:4) = RoeDens*(urr(VEL2:VEL3) - ull(VEL2:VEL3))
#  r2 = (/ 1., RoeVel(1), RoeVel(2), RoeVel(3), 0.5*absVel /)
#  r3 = (/ 0., 0.,        1.,        0.,        RoeVel(2)  /)
#  r4 = (/ 0., 0.,        0.,        1.,        RoeVel(3)  /)
#
#  F=(Ssr*fl-Ssl*fr + Ssl*Ssr* &
#     (urr(CONS)-ull(CONS) - delta*(r2*Alpha(2)+r3*Alpha(3)+r4*Alpha(4))))/(Ssr-Ssl)
#end # subsonic case
end

#const riemannfunc   = nothing
#const riemannbcfunc = nothing

riemanndict = Dict{String,Function}(
              "lf"      => lf!,
              "roe"     => roe!,
              "roefix"  => roeentropyfix!,
              "hll"     => hll!,
              "hllc"    => hllc!,
              "hlle"    => hlle!,
              "hllem"   => hllem!
              )

riemannsplitdict = Dict{String,Function}(
                   "lf"      => lfsplit!,
                   "roe"     => roesplit!,
                   "roefix"  => roeentropyfixsplit!,
                   "average" => fluxaverage!
                   )

#riemannnumdict = Dict{Integer,Function}
#              (
#               1  => lf,
#               3  => roe,
#               33 => roeentropyfix,
#               4  => hll,
#               2  => hllc,
#               5  => hlle,
#               6  => hllem
#              )
#
#riemannsplitnumdict = Dict{Integer,Function}
#              (
#               1  => lfsplit,
#               3  => roesplit,
#               33 => roeentropyfixsplit,
#               0  => average
#              )


"""
Initialize Riemann solver routines, read inner and BC Riemann solver parameters and set pointers
"""
function initriemann(params)
  #TODO: Numerical values for riemann solvers

  riemannkey   = getparameter(params,"riemann"   ; required=false, default="roe" )
  riemannbckey = getparameter(params,"riemannbc" ; required=false, default="same" )

  dict = USESPLIT ? riemannsplitdict : riemanndict
  getriemann(key) = haskey(dict,key) ? dict[key] : error("Riemann solver $riemann does not exist or is invalid for the selected fluxes!")

  global riemannfunc   = getriemann(riemannkey)
  global riemannbcfunc = riemannbckey == "same" ? riemannfunc : getriemann(riemannbckey)

  return true
end

export riemannfunc, riemannbcfunc

const f   = MVector{NVAR, FT}(undef)
const fl  = MVector{NVAR, FT}(undef)
const fr  = MVector{NVAR, FT}(undef)
const ull = MVector{NVARE,FT}(undef)
const urr = MVector{NVARE,FT}(undef)

"""
Computes the numerical flux
Conservative States are rotated into normal direction in this routine and are NOT backrotated: don't use it after this routine##
Attention 2: numerical flux is backrotated at the end of the routine##
"""
@inline function riemann!(fout::C, ul::C, ur::C, upl::P, upr::P,
                          nv::G, t1::G, t2::G, riemannfunc) where
   { C <: AbstractVector{T} ,P <: AbstractVector{T} ,G <: AbstractVector{T} } where T
   global ull, urr, fl, fr, f

  @inbounds begin
      # Momentum has to be rotated using the normal system individual for each
      ull[EDENS] = ul[DENS]
      ull[EMOM1] = ul[MOM1] * nv[1] + ul[MOM2] * nv[2] + ul[MOM3] * nv[3]
      ull[EMOM2] = ul[MOM1] * t1[1] + ul[MOM2] * t1[2] + ul[MOM3] * t1[3]
  @d3 ull[EMOM3] = ul[MOM1] * t2[1] + ul[MOM2] * t2[2] + ul[MOM3] * t2[3]
      ull[EENER] = ul[ENER]
      ull[ESRHO] = 1 / ull[EDENS]
      ull[EVEL1] = ull[EMOM1] * ull[ESRHO]
      ull[EVEL2] = ull[EMOM2] * ull[ESRHO]
  @d3 ull[EVEL3] = ull[EMOM3] * ull[ESRHO]
      ull[EPRES] = upl[PRES]
      ull[ETEMP] = upl[TEMP]

      urr[EDENS] = ur[DENS]
      urr[EMOM1] = ur[MOM1] * nv[1] + ur[MOM2] * nv[2] + ur[MOM3] * nv[3]
      urr[EMOM2] = ur[MOM1] * t1[1] + ur[MOM2] * t1[2] + ur[MOM3] * t1[3]
  @d3 urr[EMOM3] = ur[MOM1] * t2[1] + ur[MOM2] * t2[2] + ur[MOM3] * t2[3]
      urr[EENER] = ur[ENER]
      urr[ESRHO] = 1 / urr[EDENS]
      urr[EVEL1] = urr[EMOM1] * urr[ESRHO]
      urr[EVEL2] = urr[EMOM2] * urr[ESRHO]
  @d3 urr[EVEL3] = urr[EMOM3] * urr[ESRHO]
      urr[EPRES] = upr[PRES]
      urr[ETEMP] = upr[TEMP]

      eulerflux1d!(fl, ull)
      eulerflux1d!(fr, urr)
      riemannfunc(f, fl, fr, ull, urr)

      # Back Rotate the normal flux into Cartesian direction
      fout[DENS] = f[DENS]
      fout[MOM1] = nv[1]*f[MOM1] + t1[1]*f[MOM2] + t2[1]*f[MOM3]
      fout[MOM2] = nv[2]*f[MOM1] + t1[2]*f[MOM2] + t2[2]*f[MOM3]
      fout[MOM3] = nv[3]*f[MOM1] + t1[3]*f[MOM2] + t2[3]*f[MOM3]
      fout[ENER] = f[ENER]
  end
end
function riemann!(fout::C, ul::C, ur::C, upl::P, upr::P, nv::G, t1::G, t2::G, riemannfunc, dofs) where
   { C <: AbstractArray{T,2} ,P <: AbstractArray{T,2} ,G <: AbstractArray{T,2} } where T
    @inbounds @views for d in dofs
        riemann!(fout[:,d],
                 ul[:,d],  ur[:,d],
                upl[:,d], upr[:,d],
                 nv[:,d],  t1[:,d], t2[:,d],
                 riemannfunc)
    end
end
