module Equation

using ..Haflinger

# Export strategy: only export active equation system and EOS
export EquationSystem, EOS

include("navierstokes/navierstokes.jl")
include("linadv/linadv.jl")
include("burgers/burgers.jl")

equations = Dict{String,Module}(
            "navierstokes" => Navierstokes,
            "linadv"       => Linadv,
            "burgers"      => Burgers
            )

const EquationSystem = Navierstokes
const EOS = EquationSystem.EOS

using .EquationSystem
using .EOS

const equationinitisdone   = false
function initequation(params)
  initstart("EQUATION")

  eqntype = getparameter(params,"equation" ; required=false, default="navierstokes" )
  if haskey(equations,eqntype)
    global EquationSystem = equations[eqntype]
  else
    error("Unknown equation system: $eqntype")
  end
  EquationSystem.initequation(params)
  global EOS = EquationSystem.EOS

  initdone("EQUATION")
end


end
