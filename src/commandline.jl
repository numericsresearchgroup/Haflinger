using ArgParse

export parse_commandline, validate_args

"""
Provides settings for the command line version of the solver
"""
function parsecommandline()

    s = ArgParseSettings()
    s.prog        = "Haflinger"
    s.description = "This program has been developed by the Numerics Research Group Cologne division."
    s.version     = "0.0.1"

    @add_arg_table s begin
      "--debug", "-d"
        help = "run solver in debug mode"
        action = :store_true
      "--verbose", "-v"
        help = "more information"
        action = :store_true
      "--nompi"
        help = "run the solver in serial mode"
        action = :store_true
      "parameter.jl"
        help = "a Julia script containing the simulation setup"
        arg_type = String
        required = true
      "restart-state.h5"
        help = "filename of a solver state file, in case of a restart is requested"
        arg_type = String
        required = false
    end

    parse_args(s)
end


"""
Retrieve command line arguments
"""
function getcommandlineargs()

    args = parsecommandline()

    verbose       =  args["verbose"]
    debug         =  args["debug"]
    USEMPI        = !args["nompi"]
    parameterfile =  args["parameter.jl"]
    restartfile   =  args["restart-state.h5"]

    verbose ? (@warn "Yet to implement flag verbose ...") : Nothing
    debug   ? (@warn "Yet to implement flag debug ...")   : Nothing

    if !isfile(parameterfile)
        error("Parameter file does not exist!")
    end
    parameterfile = joinpath(pwd(), parameterfile)

    if restartfile ≠ nothing
        restartfile = joinpath(pwd(), restartfile)
        if !isfile(restartfile)
            error("Restart file does not exist!")
        end
    else
        restartfile = ""
    end

    parameterfile, restartfile#, USEMPI
end
