__precompile__(false)

module Haflinger

using Reexport
@reexport using StaticArrays
@reexport using Printf
@reexport using LinearAlgebra

# package code goes here
include("fillparameter.jl")
include("commandline.jl")

include("parallel/parallel.jl")
@reexport using .Parallel

include("logo.jl")
include("globals/globals.jl")
@reexport using .Globals
include("initialize.jl")
include("interpolation/interpolation.jl")
@reexport using .Interpolation
include("mortar/mortar.jl")
include("io_hdf5/io_hdf5.jl")
@reexport using .IO_HDF5
include("mesh/mesh.jl")
@reexport using .Mesh
include("equation/equation.jl")
@reexport using .Equation
@reexport using .EquationSystem
include("analyze/analyze.jl")
include("filtering/filtering.jl")
include("dg/dg.jl")
@reexport using .DG
include("timedisc/timedisc.jl")

#@reexport using .Equation.EquationSystem.EOS
#@reexport using .Timedisc
#@reexport using .Analyze

# Run this in non-interactive mode
if !isinteractive()
    args=getcommandlineargs()
    initsolver(args...)
    runsimulation()
    finalizesimulation()
end

end
