
"""
In this routine, the surface integral will be computed
Polynomial degree is either N or NOver (overintegration)

lhatplus,lhatminus : Lagrange polynomials evaluated at ξ=+1, ξ=-1 and premultiplied by mass matrix
#             false  BCSides+(Mortar-)InnerSides+MPISides_MINE
"""
function surfintelem!(master, tmp, ut, dg::DGBasis{N,T}, v::Val{V}, n::Val{N}, e2s, v2s2) where {V,T,N}
    nodetype = dg.ip.nodetype;
    lhatm = dg.lhatminus[1]; lhats = -lhatm # we pack change of the flux sign into lhat

    # 1. orient flux to fit flip and locSide to element local system
    # 2. perform actual surface integral
    @inbounds for elem = 1:size(ut, 5)
        for locside=1:6
            side, flip = e2s[E2S_SIDE,locside,elem], e2s[E2S_FLIP,locside,elem]
            flipside!(tmp, locside, master, side, v2s2, flip, locside, v, n, flip == 1 ? lhatm : lhats)
        end
        if nodetype == :nodetypegl
            surfintglelem!(tmp, ut, v, n, elem)
        else
            #evalelemfaceg(d.vol[:,:,:,:,elem], d.face, ip.lminus, ip.lplus, locside)
        end

    end
end
@inline surfintelem!(f, ut, dg, mesh) = surfintelem!(f.master, f.face, ut, dg, f.v, f.n, mesh.conn.elemtoside, mesh.mappings.v2s2)

"""
Update DG time derivative with corresponding SurfInt contribution

Takes the time derivative of a single element and the surface flux on a single side as input and performs the surface integral.
The time derivative will be updated. The Flux has to be provided in the volume coordinate system!

side : local side ID
flux : Flux on side, in volume system of respective element
lhat : Lagrange polynomials evaluated at ξ =+1 and ξ =-1 premultiplied by mass matrix
"""
function surfintglelem!(face::AbstractArray{T,4}, vol::AbstractArray{T,5}, ::Val{V}, ::Val{N},
                            E::Integer ) where {V,N,T}
    @inbounds begin
    for q = 1:N, p = 1:N; @simd for v = 1:V
        vol[v,1,p,q,E] += face[v,p,q,XI_MINUS]
        vol[v,p,N,q,E] += face[v,p,q,ETA_PLUS]
        vol[v,p,q,1,E] += face[v,p,q,ZETA_MINUS]
    end;end
    for q = 1:N, p = 1:N; @simd for v = 1:V
        vol[v,N,p,q,E] += face[v,p,q,XI_PLUS]
        vol[v,p,1,q,E] += face[v,p,q,ETA_MINUS]
        vol[v,p,q,N,E] += face[v,p,q,ZETA_PLUS]
    end;end
    end
end


#function surfintface!(f::Flux, ut::Array{T}, dg::DGBasis, dompisides::Bool) where T
#
#    s2e  = mesh.conn.sidetoelem
#    v2s2 = mesh.mappings.v2s2
#    master=f.master;
#    @views tmp=f.face[:,:,:,1]; #slave=f.slave;
#    nodetype = dg.ip.nodetype;
#    lhatm = dg.lhatminus[1]; lhats = -lhatm # we pack change of the flux sign into lhat
#
#    range = dompisides ? (Mesh.mpisidesyour.start:Mesh.mpimortars.stop) : (Mesh.bcsides.start:Mesh.mpisidesmine.stop)
##                         MPI_YOUR                                :  inner sides and MPI mine
#
#    # 1. orient flux to fit flip and locSide to element local system
#    # 2. perform actual surface integral
#    @inbounds for side in range
#        elem      = s2e[S2E_ELEM,  side]
#        nbelem    = s2e[S2E_NBELEM,side]
#
#        #master sides
#        if elem > 0
#            locside = s2e[S2E_LOCSIDE,side]
#            flipside!(tmp, master, side, v2s2, 1, locside, f.v, f.n)
#            if nodetype == :nodetypegl
#                surfintglface!(tmp, ut, f.v, f.n, elem, locside, lhatm)
#            else
#                #evalelemfaceg(d.vol[:,:,:,:,elem], d.face, ip.lminus, ip.lplus, locside)
#            end
#        end
#
#        # slave sides
#        if nbelem > 0
#            nblocside = s2e[S2E_NBLOCSIDE,side]
#            flip      = s2e[S2E_FLIP,side]
#            # p,q are in the master RHS system, they need to be transformed to the slave volume system using S2V2 mapping
#            flipside!(tmp, master, side, v2s2, flip, nblocside, f.v, f.n)
#            if nodetype == :nodetypegl
#                surfintglface!(tmp, ut, f.v, f.n, nbelem, nblocside, lhats)
#            else
#                #dosurfintg(fluxtmp, lhatminus,   lhatplus,    nblocside, ut[:,:,:,:,nbelem])
#            end
#        end
#    end
#end
# function surfintglface!(face::AbstractArray{T,3}, vol::AbstractArray{T,5}, ::Val{V}, ::Val{N},
#                                E::Integer, locside::Integer, lhat::T ) where {V,N,T}
#     @inbounds begin
#         if     locside == XI_MINUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,1,p,q,E] += face[v,p,q]*lhat
#             end
#         elseif locside == XI_PLUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,N,p,q,E] += face[v,p,q]*lhat
#             end
#         elseif locside == ETA_MINUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,p,1,q,E] += face[v,p,q]*lhat
#             end
#         elseif locside == ETA_PLUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,p,N,q,E] += face[v,p,q]*lhat
#             end
#         elseif locside == ZETA_MINUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,p,q,1,E] += face[v,p,q]*lhat
#             end
#         elseif locside == ZETA_PLUS
#             for q = 1:N, p = 1:N, v = 1:V
#                 vol[v,p,q,N,E] += face[v,p,q]*lhat
#             end
#         end
#     end
# end
