#"""
#This module prepares the computation of the fluxes over the sides by filling the two parts (advective and viscous)
#of the common interface flux by calling the associated numerical flux functions.
#We distinguish between inner sides, sides with boundary conditions and mpi sides.
#"""

"""
Computes the fluxes for inner sides, MPI sides where the local proc is "master" and boundary conditions.
The flux computation is performed seperately for advection and diffusion fluxes if parabolic terms are considered.

# doMPISides  #< = true only MINE (where the proc is master)  MPISides are filled, =false InnerSides
# t           #< physical time required for BC state evaluation in case of time dependent BCs
"""
function fillfluxsurf!(c, m, time, range)
    if range.stop < range.start
        return
    end
    f, u, up = c.fluxp, c.u, c.uprim
    um,  us  = u.master,  u.slave
    upm, ups = up.master, up.slave
    nv, t1, t2, surf::Array{FT,2} = m.normvec, m.tangvec1, m.tangvec2, m.surfelem
    fm::Array{FT,2}, fs::Array{FT,2} = f.master,  f.slave

    if USEPARABOLIC
        gxm, gym, gzm = c.gradux.master, c.graduy.master, c.graduz.master
        gxs, gys, gzs = c.gradux.slave , c.graduy.slave , c.graduz.slave
        fxm, fym, fzm = c.fluxx.master , c.fluxy.master , c.fluxz.master
        fxs, fys, fzs = c.fluxx.slave  , c.fluxy.slave  , c.fluxz.slave
    end
    # Fill flux for sides ranging between firstSideID and lastSideID using Riemann solver for advection and viscous terms
    # MPI:     fill only flux for MINE MPISides (where the local proc is master)
    # Non-MPI: fill only InnerSides that do not need communication
    dofs      = ((range.start-1)*N^2 + 1):(range.stop*N^2)

    # =============================
    # Workflow:
    #
    #  1.  compute flux for non-BC sides and store in fluxmaster
    #  1.1) advective flux
    #  1.2) viscous flux
    #  1.3) add up viscous flux to fluxmaster
    #  2.  compute flux for BC sides and store in fluxmaster, repeat 2)+3)
    #  3.  multiply by surfelem
    #  4.  copy flux from fluxmaster to fluxslave
    # =============================
    riemann!(fm, um, us, upm, ups,
             nv, t1, t2,
             riemannfunc, dofs)
    if USEPARABOLIC
        diffflux3d!(upm, gxm, gym, gzm,
                         fxm, fym, fzm, dofs)
        diffflux3d!(ups, gxs, gys, gzs,
                         fxs, fys, fzs, dofs)
        meanflux(fs, fxm, fym, fzm, fxs, fys, fzs, nv, dofs)

        @inbounds for d in dofs; @simd for v = 1:NVAR
            fm[v,d] = (fm[v,d] + fs[v,d]) * surf[1,d]
        end; end
    else
        @inbounds for d in dofs; @simd for v = 1:NVAR
            fm[v,d] *= m.surfelem[1,d]
        end; end
    end
end

@inline function meanflux(f::A, fxl::A, fyl::A, fzl::A, fxr::A, fyr::A, fzr::A, nv) where A <: AbstractVector{T} where T
    @inbounds for v = 1:NVAR
    f[v] = 0.5*(nv[1]*(fxl[v]+fxr[v])
               +nv[2]*(fyl[v]+fyr[v])
               +nv[3]*(fzl[v]+fzr[v]))
    end
end
function meanflux(f::A, fxl::A, fyl::A, fzl::A, fxr::A, fyr::A, fzr::A, nv, dofs) where A <: AbstractArray{T,2} where T
    @inbounds @views for i in dofs
        meanflux(f[:,i], fxl[:,i], fyl[:,i], fzl[:,i], fxr[:,i], fyr[:,i], fzr[:,i], nv[:,i])
    end
end
