"""
Computes the volume integral in the conservative way for a given set of fluxes in weak or strong form.

In the conservative form of the volume integral we calculate the integral from the transformed fluxes.
A weak or strong form volume integral can be performed.
This routine is used for the standard and the lifting volume integral, in the latter the transformed fluxes are simply
the transformed solution.
- Weak form: We integrate over the transformed flux times the derivative of the trial functions, which is implemented
  using the D_hat_T matrix. This is the transpose of the D_hat matrix which is defined as
  Dhat_{ij}=-\frac{ω_i}{ω_j} D_{ij} where D_{ij} is the standard polynomial derivative matrix.
- Strong form: We integrate over the derivative of the transformed flux times the trial function. The derivative of the
  flux is calculated using the transpose of the polynomial derivative matrix D_T.

Regarding the implementation, the only difference between strong and weak form is the use of the D or the Dhat matrix.
Note 1: 1/J(i,j,k) is not yet accounted for
Note 2: input is overwritten with the volume flux derivatives
"""
function volint!(ut::A, f::A, g::A, h::A, D::SMatrix{N,N,T}, ::Val{V}) where A <: AbstractArray{T,5} where {T, N, V}
    E=size(ut,ndims(ut))
    @inbounds for e=1:E, k=1:N, j=1:N, i=1:N;
        @simd for v=1:V
            ut[v,i,j,k,e]  = (D[1,i] *  f[v,1,j,k,e] +
                              D[1,j] *  g[v,i,1,k,e] +
                              D[1,k] *  h[v,i,j,1,e])
        end
        for l=2:N; @simd for v=1:V
            ut[v,i,j,k,e] += (D[l,i] *  f[v,l,j,k,e] +
                              D[l,j] *  g[v,i,l,k,e] +
                              D[l,k] *  h[v,i,j,l,e])
    end; end; end
end
volint!(calc::Calc) = volint!(calc.ut, calc.fluxr.x, calc.fluxr.y, calc.fluxr.z, calc.dg.dhat_t, calc.fluxr.v)


"""
Computes the advective and diffusive fluxes
"""
function fillfluxvol!(c::Calc, m::VolMetrics)
    advflux3d!(c.u.vol, c.uprim.vol,
               c.fluxp.x, c.fluxp.y, c.fluxp.z)
    if USEPARABOLIC
        diffflux3d!(c.uprim.vol, c.gradux.vol, c.graduy.vol, c.graduz.vol,
                                 c.fluxr.x,    c.fluxr.y,    c.fluxr.z)
        fastadd(c.fluxp.x, c.fluxr.x)
        fastadd(c.fluxp.y, c.fluxr.y)
        fastadd(c.fluxp.z, c.fluxr.z)
    end

    applymetrics!(c.fluxr.x, c.fluxr.y, c.fluxr.z,    # reference space
                  c.fluxp.x, c.fluxp.y, c.fluxp.z,    # physical space
                  m.f, m.g, m.h, c.fluxr.v) # metrics
end
fastadd(a,b) = @inbounds @simd for i in eachindex(a); a[i] += b[i]; end

"""
Compute the tranformed states for all conservative variables using the metric terms
NOTE: For some reason the strategy ala Flexi (i.e. applying the metrics locally) does not work
      We have to use global buffers for better performance
"""
@inline function applymetrics!(f::A,fx::A,fy::A,fz::A,m::A, ::Val{V}) where A <: AbstractVector{T} where {T,V}
    @inbounds for v=1:V
        f[v] = m[1] * fx[v] + m[2] * fy[v] + m[3] * fz[v]
    end
end
function applymetrics!(f::A, g::A, h::A, fx::A, fy::A, fz::A, mf::A, mg::A, mh::A, v::Val{V}) where A <: AbstractArray{T,2} where {T,V}
    @inbounds @views for i = 1:size(fx,2)
        applymetrics!(f[:,i], fx[:,i], fy[:,i], fz[:,i], mf[:,i], v)
        applymetrics!(g[:,i], fx[:,i], fy[:,i], fz[:,i], mg[:,i], v)
        applymetrics!(h[:,i], fx[:,i], fy[:,i], fz[:,i], mh[:,i], v)
    end
end
# 2D version
@inline function applymetrics!(f::A,fx::A,fy::A,m::A, ::Val{V}) where A <: AbstractVector{T} where {T,V}
    @inbounds for v=1:V
        f[v] = m[1] * fx[v] + m[2] * fy[v]
    end
end
function applymetrics!(f::A,g::A,fx::A,fy::A,mf::A,mg::A, v::Val{V}) where A <: AbstractArray{T,2} where {T,V}
    @inbounds @views for i = 1:size(fx,2)
        applymetrics!(f[:,i], fx[:,i], fy[:,i], v)
        applymetrics!(g[:,i], fx[:,i], fy[:,i], v)
    end
end
