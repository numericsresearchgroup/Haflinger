
"""
Interpolates the interior volume data (stored at the Gauss or Gauss-Lobatto points) to the surface
integration points, using fast 1D Interpolation and store in global side structure
"""
function prolongtoface!(vol, master, slave, tmp, ip::IPBasis{N,T}, v::Val{V}, n::Val{N}) where {N,T,V}
    e2s  = mesh.conn.elemtoside
    s2v2 = mesh.mappings.s2v2

    @inbounds for elem=1:size(vol, 5)
        slice!(tmp, vol, v, n, elem)
        for locside=1:6
            side, flip = e2s[E2S_SIDE,locside,elem], e2s[E2S_FLIP,locside,elem]
            flipside!(flip == 1 ? master : slave, side, tmp, locside, s2v2, flip, locside, v, n)
        end

    end
end
@inline prolongtoface!(d, ip) = prolongtoface!(d.vol, d.master, d.slave, d.face, ip, d.v, d.n)

#function prolongtofaceside!(d::Data, ip::IPBasis, dompisides::Bool)
#
#    s2e  = mesh.conn.sidetoelem
#    s2v2 = mesh.mappings.s2v2
#    vol  = d.vol; master = d.master; slave = d.slave; @views tmp = d.face
#    nodetype = ip.nodetype
#
#    range = dompisides ? (Mesh.mpisidesyour.start:Mesh.mpimortars.stop) : (Mesh.bcsides.start:Mesh.mpisidesmine.stop)
##                         MPI_YOUR                                :  inner sides and MPI mine
#
#    for side in range
#        elem    = s2e[S2E_ELEM  ,side]
#        nbelem  = s2e[S2E_NBELEM,side]
#
#        # TODO: put into functions
#        #elem   > 0 ? prolongmaster!(master[:,:,:,side], vol[:,:,:,:,  elem], tmp, nodetype, n, nv) : nothing
#        #nbelem > 0 ? prolongslave!( slave[ :,:,:,side], vol[:,:,:,:,nbelem], tmp, nodetype, n, nv)  : nothing
#
#        #master sides
#        if elem > 0
#            locside = s2e[S2E_LOCSIDE,side]
#            if nodetype == :nodetypegl
#                slice!(tmp, vol, d.v, d.n, elem, locside)
#            else
#                #evalelemfaceg(d.vol[:,:,:,:,elem], d.face, ip.lminus, ip.lplus, locside)
#            end
#            flipside!(master, side, tmp, s2v2, 1, locside, d.v, d.n)
#        end
#
#        #slave side (ElemID,locSide and flip =-1 if not existing)
#        if nbelem > 0
#            nblocside = s2e[S2E_NBLOCSIDE, side]
#            flip      = s2e[S2E_FLIP,      side]
#            if nodetype == :nodetypegl
#                slice!(tmp, vol, d.v, d.n, nbelem, nblocside)
#            else
#                #evalelemfaceg(d.vol[:,:,:,:,nbelem], d.face, ip.lminus, ip.lplus, nblocside)
#            end
#            flipside!(slave, side, tmp, s2v2, flip, nblocside, d.v, d.n)
#        end
#    end
#end
