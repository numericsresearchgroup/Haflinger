__precompile__(false)

@reexport module DG

using BenchmarkTools
using ..Haflinger
import ..Globals: rn

export initdg, dgoperator!, @bench
export calc, calc2, calcd, calcc, calcs
export prolongtoface!, fillfluxsurf!, fillfluxvol!, surfint!, volint!, fillini!
export applymetrics!

include("dg_vars.jl")
include("fillflux.jl")
include("prolongtoface.jl")
include("surfint.jl")
include("volint.jl")
include("lifting/lifting.jl")


const surfint! = surfintelem! #we want the element based one

function initdg(params)
    initstart("DG")

    # Check if all the necessary initialization is done before
    if !interpolationinitisdone || !meshinitisdone
      safeerror("InitDG not ready to be called or already called.")
    end
    # temp
    n=N; nodetype=NODETYPE; nvar=NVAR; nvarlift=NVARLIFT

    # Pre-compute the building blocks for
    # the interpolation and the DG operator
    # (differentiation matrices and prolongation operators)
    ip = IPBasis{n,FT}(nodetype)
    dg = DGBasis(ip)

    ut    =          zeros(FT, nvar, n,n,n,         nelems)
    u     =           Data(FT, nvar,     n, nsides, nelems)
    uprim = USEPRIM ? Data(FT, nvarlift, n, nsides, nelems) : Data(FT)
    fluxp, fluxr =   [Flux(FT, nvar,     n, nsides, nelems) for i = 1:2]

    fluxlifting            =  USEPARABOLIC ? Flux(FT, nvarlift, n, nsides, nelems) : Flux(FT)
    gradux, graduy, graduz = [USEPARABOLIC ? Data(FT, nvarlift, n, nsides, nelems) : Data(FT) for i = 1:3]
    fluxx,  fluxy,  fluxz  = [USEPARABOLIC ? Data(FT, nvarlift, n, nsides, nelems) : Data(FT) for i = 1:3]

    if USEPARABOLIC
      initlifting(ini)
    end

    global calc  = Calc{FT}(ut, u, uprim, fluxp, fluxr,
                            gradux, graduy, graduz, fluxx, fluxy, fluxz,
                            dg, Equation.EquationSystem)
    global calc2 = r2(calc)
    global calcd = rd(calc)
    global calcc = rc(calc)
    global calcs = (calc, calc2, calcd, calcc)

#    global calcs = [calc, calc2, calcd, calcc]

    # Fill the solution vector U with the initial solution by interpolation, if not filled through restart already
    if !dorestart
      fillini!(mesh2.vol.xgp, calc2.u.vol)
    end

    # Add solution array to output data
    push!(STATEDATA, ("DG_Solution", EquationSystem.VARNAMES, calc.u.vol))

    initdone("DG")
end



"""
Allocate and initialize the building blocks for the DG operator: Differentiation matrices and prolongation operators
xgp            : Gauss/Gauss-Lobatto Nodes
wgp            : Gauss/Gauss-Lobatto Weights
lminus/plus    : Values of lagrange polynomials at ξ = ±1
d              : Differentation matrix
d_t            : Transpose of differentation matrix
d_hat          : Differentiation matrix premultiplied by mass matrix,
               : Dhat = M^{-1} D^T M
d_hat_t        : Transpose of D_Hat matrix
lhatminus/plus : Values of lagrange polynomials at ξ = ±1
               : premultiplied with mass matrix
"""
function DGBasis(ib)
  T=eltype(ib.xgp)
  n=length(ib.xgp)
  # Compute Differentiation matrix D for given Gausspoints
  D=polynomialderivativematrix(ib.xgp)
  D_t=SMatrix{n,n,T}(D')

  # Build D_Hat matrix. D^ = - (M^(-1) * D^T * M)
  m    = diagm(0 => ib.wgp)
  minv = diagm(0 => 1 ./ ib.wgp)

  Dhat   = -minv * (D' * m)
  Dhat_t = SMatrix{n,n,T}(Dhat')

  if USESPLIT
    dvolsurf = D_t
    dvolsurf[1,1] +=   1.0/(2.0 * ib.wgp[1])
    dvolsurf[n,n] -= - 1.0/(2.0 * ib.wgp[n])
    dvolsurf = SMatrix{n,n,T}(dvolsurf) # copy // ugly?!
  end

  # interpolate to left and right face (1 and -1 in reference space)
  # and pre-divide by mass matrix
  lhatplus  = SVector{n,T}(minv*ib.lplus )
  lhatminus = SVector{n,T}(minv*ib.lminus)

  mb = MortarBasis(n, ib.nodetype)
  DGBasis{n,T}(D, D_t, Dhat, Dhat_t, lhatminus, lhatplus, ib, mb)
end


"""
Computes the residual Ut = \frac {d\vec{U}} {dt} from the current solution U employing the DG method.
Computes the weak DGSEM space operator from surface, volume and source contributions. To do this we need to:
- Prolong the solution from the volume to the interface
- Invoke the lifting operator to calculate the gradients
- Perform the volume integral
- Perform the surface integral
- If needed, add source terms to the residual

-----------------------------------------------------------------------------
MAIN STEPS
-----------------------------------------------------------------------------
0.  Convert volume solution to primitive
1.  Prolong to face (fill U_master/slave)
3.  ConsToPrim of face data (U_master/slave)
5.  Lifting
6.  Flux computation and volume integral
9.  Fill flux (Riemann solver) + surface integral
11. Ut = -Ut
12. Sponge and source terms
13. Perform overintegration and apply Jacobian
-----------------------------------------------------------------------------
"""
function dgoperator!(calcs, meshs, t::FT)
    calc, calc2, calcd, calcc = calcs
    mesh, mesh2, meshd        = meshs
    mc = mesh.mpiconn

    # 1. Prolong the solution to the face integration points for flux computation (and do overlapping communication)
    # -----------------------------------------------------------------------------------------------------------
    # General idea: The slave sends its surface data to the master, where the flux is computed and sent back to the slaves.
    # Steps:
    # * (these steps are done for all slave MPI sides first and then for all remaining sides):
    # 1.0  Open receive buffers
    # 1.1  Prolong solution to faces and store in U_master/slave. Use them to build mortar data (split into 2/4 smaller sides).
    #      Then U_slave can be communicated from the slave to master MPI side.
    # 1.2  Send data
    # 1.3  Finish all started MPI communications

    @mpi rrequ = receivempi(calcc.u.slave, RECVMINE, mc )
    @bench prolongtoface!(  calc.u, calc.dg.ip)
    @bench mortarbigtosmall(calc.u, calc.dg.mb, mesh)
    @mpi srequ = sendmpi(   calcc.u.slave, SENDYOUR, mc )

    @bench fill!(calc.ut, 0.)

    # 2. Compute primitive variables from conserved variables, required for lifting and fluxes
    #    Use these operation for hiding the latency of the communications in 1.
    # -----------------------------------------------------------------------------------------------------------
    # 2.1 Convert volume terms
    # 2.2 Convert face data from conservative to primitive variables
    @bench Equation.constoprim!(calc2.u.vol, calc2.uprim.vol)

    # 3. Convert face data from conservative to primitive variables
    #
    # Two possibilities for sides if using non-Lobatto node sets:
    # 1. Convert U_master/slave to prims (used):
    #    prims consistent to cons, but inconsistent to prim volume
    #    cheap and simple, no communication and mortars required
    # 2. Compute UPrim_master/slave from volume UPrim
    #    UPrim_master/slave consistent to UPrim, but inconsistent to U_master/slave
    #    more expensive, communication and mortars required
    dofs(sides) = ((sides.start-1)*N^2 + 1):(sides.stop*N^2)
    if USEPRIM
        @bench Equation.constoprim!(calc2.u.master, calc2.uprim.master, dofs(Mesh.mastersides))
    end
    @mpi MPI.Waitall!([srequ; rrequ]) # uslave: slave -> master
    if USEPRIM
        @bench Equation.constoprim!(calc2.u.slave,  calc2.uprim.slave,  dofs(Mesh.slavesides))
    end

    # 5. Lifting
    # Compute the gradients using Lifting (BR1 scheme,BR2 scheme ...)
    # The communication of the gradients is initialized within the lifting routines
    if USEPARABOLIC

        lifting(calcs, meshs, t)
        @mpi begin
            rreqx = receivempi(calcc.gradux.slave, RECVMINE, mc )
            rreqy = receivempi(calcc.graduy.slave, RECVMINE, mc )
            rreqz = receivempi(calcc.graduz.slave, RECVMINE, mc )
            sreqx = sendmpi(   calcc.gradux.slave, SENDYOUR, mc )
            sreqy = sendmpi(   calcc.graduy.slave, SENDYOUR, mc )
            sreqz = sendmpi(   calcc.graduz.slave, SENDYOUR, mc )
        end
    end

    # 6. Compute volume integral contribution and add to Ut
    #    Compute separately in case of selective overintegration
    @bench fillfluxvol!(calc2, mesh2.vol)
    @bench volint!(calc)

    if USEPARABOLIC
        # Complete send / receive for gradu started in the lifting routines
        @mpi MPI.Waitall!([rreqx; rreqy; rreqz; sreqx; sreqy; sreqz]) # gradu: slave -> master
    end

    # 9. Fill flux and Surface integral
    # General idea: U_master/slave and gradUx,y,z_master/slave are filled and can be used to compute the Riemann solver
    #               and viscous flux at the faces. This is done for the MPI master sides first, to start communication early
    #               and then for all other sides.
    #               After communication from master to slave the flux can be integrated over the faces.
    # Steps:
    # * (step 9.2 is done for all MPI master sides first and then for all remaining sides)
    # * (step 9.3 and 9.4 are done for all other sides first and then for the MPI master sides)
    # 9.3)  Fill flux (Riemann solver + viscous flux)
    # 9.4)  Combine fluxes from the 2/4 small mortar sides to the flux on the big mortar side (when communication finished)
    # 9.5)  Compute surface integral


    # 9.3) receive your / flux_slave: master -> slave
    @mpi begin
        rreq = receivempi(calcc.fluxp.master, RECVYOUR, mc)
        fillfluxsurf!(calc2, mesh2.surf, t, Mesh.mpisidesmine)
        sreq = sendmpi(   calcc.fluxp.master, SENDMINE, mc)
    end

    @bench fillfluxsurf!(calc2, mesh2.surf, t, Mesh.innersides)
    @bench mortarsmalltobig(calc.fluxp, calc.dg.mb, mesh, true)
    @mpi MPI.Waitall!([sreq; rreq]) # fluxslave: master -> slave

    #serial() do
    #    for iside=1:nsides
    #    iprintln(iside)
    #    iprintln(calc.fluxp.slave[:,1,1,iside])
    #    end
    #end
    @bench surfint!(calc.fluxp,calc.ut,calc.dg,mesh)

    # 11. Swap to right sign :)
    ut::Array{FT,5} = calc.ut
    @bench @inbounds for i in eachindex(ut)
        ut[i] = -ut[i]
    end

    # 12. Compute source terms and sponge (in physical space, conversion to reference space inside routines)
    #applysourceterms(calc, t)

    # TODO: add sponge, testcases
    #if dosponge
    #  sponge(ut)
    #end

    # TODO: add overintegration (maybe for testing ?)
    ## 13. Perform overintegration and apply Jacobian
    ## Perform overintegration (projection filtering type overintegration)
    #if overintegrationtype > 0
    #  overintegration(ut)
    #end
    ## Apply Jacobian (for OverintegrationType==CUTOFFCONS this is already done within the Overintegration, but for DG only)
    #if overintegrationtype ≠ cutoffcons
    #  applyjacobiancons(ut,tophysical=true)
    #end
    ut2::Array{FT,2} = calc2.ut
    @bench @inbounds for i = 1:size(ut2, 2); @simd for v=1:NVAR
        ut2[v,i] *= (mesh.vol.sjac::Array{FT,5})[i]
    end; end

end

"""
Fills the solution u with a initial solution by evaluating `exactfunc` through interpolation at Gauss points xgp
"""
function fillini!(xgp::T, u::T) where T<: AbstractArray{F} where F <: AbstractFloat
    #@inbounds @views for i in CartesianRange(size(u)[2:end])
    @inbounds @views for i in 1:size(u, 2)
        EquationSystem.exactfunc(u[:,i], xgp[:,i], 0.)
    end
end

applysourceterms(c::Calc, t::AbstractFloat) = nothing

end
