# Instead of single arrays we should preferably work with structs
# We define a dual data structure, a tensor one and a vector one
# where the scalar data structure is usually derived from
# This is a combination of volume and surface terms, e.g. for convervative and primitive solution
# gradux,y,z advection and viscous fluxes
struct DGBasis{N,T <: AbstractFloat}
    d::SMatrix{N,N,T}
    d_t::SMatrix{N,N,T}
    dhat::SMatrix{N,N,T}
    dhat_t::SMatrix{N,N,T}
    lhatminus::SVector{N,T}
    lhatplus::SVector{N,T}
    ip::IPBasis{N,T}
    mb::MortarBasis{N,T}
end

struct Flux{T <: AbstractFloat, V, N} <: AbstractData
    v::Val{V}
    n::Val{N}
    x::Array{T}
    y::Array{T}
    z::Array{T}
    master::Array{T}
    slave::Array{T}
    face::Array{T}
end
function Flux(T, nvar, n, nsides, nelems)
    x      = zeros(T, nvar, n, n, n, nelems)  # the volume
    y      = similar(x)
    z      = similar(x)
    master = zeros(T, nvar, n, n,    nsides) # left of the interface
    slave  = similar(master)                 # right of the interface
    face   = zeros(T, nvar, n, n, 2*DIM)     # 4 or 6 buffer sides
    Flux{T,nvar,n}(Val(nvar), Val(n), x, y, z, master, slave, face)
end
Flux(T) = Flux(T, 1, 1, 1, 1)

struct Calc{T <: AbstractFloat}
    ut::Array{T}     # time derivative
    # the solution
    u::Data{T}       # solution vector in conserved variables
    uprim::Data{T}   # solution vector in primitive variables
    fluxp::Flux{T}   # fluxes in physical space
    fluxr::Flux{T}   # fluxes in reference space
    # the gradients in x/y/z
    gradux::Data{T}
    graduy::Data{T}
    graduz::Data{T}
    fluxx::Data{T}
    fluxy::Data{T}
    fluxz::Data{T}
    # basis, eqnsys etc.
    dg::DGBasis
    eqnsys::Module
end

rn(f::Flux{T,V,N},i) where {T,V,N} = Flux{T,V,N}(f.v, f.n, rn(f.x, i), rn(f.y, i), rn(f.z, i),
                                   rn(f.master, i), rn(f.slave, i), rn(f.face, i))
rn(c::Calc{T},i) where T = Calc{T}(rn(c.ut, i), rn(c.u, i), rn(c.uprim, i), rn(c.fluxp, i), rn(c.fluxr, i),
                                   rn(c.gradux, i), rn(c.graduy, i), rn(c.graduz, i),
                                   rn(c.fluxx,  i), rn(c.fluxy,  i), rn(c.fluxz,  i), c.dg, c.eqnsys)

export Calc, Flux, DGBasis
