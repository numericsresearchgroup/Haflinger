
include("br1.jl")
include("br2.jl")

const xdir, ydir, zdir = Val(1), Val(2), Val(3)
const liftings = Dict{String,Function}("br1" => liftingbr1,
                                       "br2" => liftingbr2)

global lifting = liftingbr1


export lifting, liftingbr1, liftingbr2
export liftingfillfluxvol!, liftingfillfluxsurf!, liftingfillfluxnormvec!, liftingvolint!

"""
Initialize BR1 lifting: get parameters and allocate arrays required for the BR1 lifting procedure.

Important parameters:
- doWeakLifting will set the lifting procedure to be performed in weak or strong form
- In the strong form, the lifting can be performed in a conservative or non conservative  version
  using the doConservativeLifting parameter

Default ist the non conservative form since this version has the fastest implementation.

The arrays containing the lifted gradients in x/y/z direction in the volume as well as on the element faces
will be allocated and nullified if necessary. If selective overintergration is used, the gradients on the element faces are
also needed on NOver.

Note that the gradient arrays in x/y/z directions in the volume and on the surfaces contain the gradients of the primitive
variables, i.e. they must be allocated for the lifting variables, i.e. for NS ( ρ,u,v,w,p,T ).
"""
function initlifting(params)
    global lifting, doweaklifting, doconservativelifting
    initstart("Lifting")
    liftkey = getparameter(params, "lifting"; required = false, default = liftingbr1)
    lifting = isa(liftkey, Function) ? liftkey : liftings[liftkey]

    if lifting == liftingbr1
        doweaklifting = getparameter(params, "doweaklifting" ; required = false, default = false)
    elseif lifting == liftingbr2
        doweaklifting = false
    end
    doconservativelifting = doweaklifting ? true : getparameter(params, "doconservativelifting"; required = false, default = false)

    initdone("Lifting")
end

function liftingfillfluxvol!(f::A, g::A, h::A, u::A, mf::A, mg::A, mh::A, ::Val{D} ) where A <: AbstractArray{T,2} where {T,D}
    @inbounds for i = 1:size(f,2), v=1:NVARLIFT
        f[v,i] = mf[D,i] * u[v,i]
        g[v,i] = mg[D,i] * u[v,i]
        h[v,i] = mh[D,i] * u[v,i]
    end
end
@inline liftingfillfluxvol!(flx::D,  fly::D,  flz::D,  u::D,  m::VolMetrics{T}, dir) where D <: Data{T,V,N} where {T,V,N} =
        liftingfillfluxvol!(flx.vol, fly.vol, flz.vol, u.vol, m.vol.f, m.vol.g, m.vol.h, dir)

function applymetricslift!(gx::A, gy::A, gz::A, gξ::A, gη::A, gζ::A, mf::A, mg::A, mh::A) where A <: AbstractArray{T,2} where T
    @inbounds for i = 1:size(gx,2), v=1:NVARLIFT
        gx[v,i] = mf[1,i] * gξ[v,i] +  mg[1,i] * gη[v,i] + mh[1,i] * gζ[v,i]
        gy[v,i] = mf[2,i] * gξ[v,i] +  mg[2,i] * gη[v,i] + mh[2,i] * gζ[v,i]
        gz[v,i] = mf[3,i] * gξ[v,i] +  mg[3,i] * gη[v,i] + mh[3,i] * gζ[v,i]
    end
end

function liftingfillfluxsurf!(flux::A, um::A, us::A, surfelem::A, range, weak::Bool, ::Val{V}, ::Val{N}) where A <: AbstractArray{T,2} where {T,V,N}
    dofs   = ((range.start-1)*N^2 + 1):(range.stop*N^2)

    if weak
        @inbounds for d in dofs; @simd for v = 1:V
            flux[v,d] = ( um[v,d] + us[v,d]) / 2 * surfelem[1,d]
        end; end
    else
        @inbounds for d in dofs; @simd for v = 1:V
            flux[v,d] = (-um[v,d] + us[v,d]) / 2 * surfelem[1,d]
        end; end
    end
end
@inline liftingfillfluxsurf!(flux, u::Data, mesh2, range, weak) = liftingfillfluxsurf!(flux, u.master, u.slave, mesh2.surf.surfelem, range, weak, u.v, u.n)

function liftingfillfluxnormvec!(fx::A, fy::A, fz::A, fref::A, nv::A, ::Val{V}) where A <: AbstractArray{T,2} where {T,V}
    @inbounds for d in 1:size(fx,2); @simd for v = 1:V
        fx[v,d] = fref[v,d] * nv[1,d]
        fy[v,d] = fref[v,d] * nv[2,d]
        fz[v,d] = fref[v,d] * nv[3,d]
    end; end
end
liftingfillfluxnormvec!(fluxx::Data, fluxy::Data, fluxz::Data, mesh2) = liftingfillfluxnormvec!(fluxx.master, fluxy.master, fluxz.master, fluxx.slave, mesh2.surf.normvec, fluxx.v)

"""
Computes the volume integral of the BR1 scheme in the non-conservative way for all directions in strong form.
In the non conservative form of the volume integral, we first differentiate the flux (equivalent to the solution) and
then apply the metric terms. Fastest implementation of the volume integral and only available in strong form.
"""
function liftingvolint!(gξ::A, gη::A, gζ::A, u::A, D::SMatrix{N,N,T}, ::Val{V}) where A <: AbstractArray{T,5} where {T, N, V}
    E=size(u,ndims(u))
    @inbounds for e=1:E, k=1:N, j=1:N, i=1:N;
        @simd for v=1:V
            gξ[v,i,j,k,e]  = D[1,i] * u[v,1,j,k,e]
            gη[v,i,j,k,e]  = D[1,j] * u[v,i,1,k,e]
            gζ[v,i,j,k,e]  = D[1,k] * u[v,i,j,1,e]
        end
        for l=2:N; @simd for v=1:V
            gξ[v,i,j,k,e] += D[l,i] * u[v,l,j,k,e]
            gη[v,i,j,k,e] += D[l,j] * u[v,i,l,k,e]
            gζ[v,i,j,k,e] += D[l,k] * u[v,i,j,l,e]
        end; end
    end
end
@inline liftingvolint!(flx::D,  fly::D,  flz::D,  u::D,  Dmat::SMatrix{N,N,T}) where D <: Data{T,V,N} where {T,V,N} =
        liftingvolint!(flx.vol, fly.vol, flz.vol, u.vol, Dmat, u.v)
