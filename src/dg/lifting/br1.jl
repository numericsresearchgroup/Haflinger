
const A2 = Array{FT,2}

"""
Computes the DG gradients using the BR1 scheme in x/y/z direction.

This routine will be called after the current solution U has been prolonged to the element faces.
To compute the lifted gradients of the solution the following steps are taken:
- Compute and communicate the surface fluxes on the mpi interface, do this first to use latency hiding. The fluxes will be
  temporarily stored in the gradUxyz_master arrays. Surface fluxes are different if strong or weak form is used.
- Compute the volume integral. The gradients will also get nullified in this routine.
  There are different versions of the VolInt routine depending on the usage of the conservative (weak or strong)
  or non conservative (strong only) form.
- The surface fluxes for all remaining sides (boundaries and inner) will be computed.
- The surface integral is performed, first for all inner and boundary sides. Then the communication of the fluxes is finished
  and the surface integral of the remaining sides is performed.
  In the surface integral there is a distinction between weak and strong formulation. The fluxes for the strong form are
  different on the slave or master sides since we substract the inner solution, this is accounted for in the SurfInt routine.
- The gradients are transformed back to physical space to be used by the DG routines.
- The computed volume gradients are prolonged to the surfaces at the end of the routine.
"""
function liftingbr1(calcs, meshs, t)
    mesh, mesh2 = meshs[1:2]
    calc, calc2, calcc  = calcs[1:3]
    u, u2 = USEPRIM ? [calc.uprim, calc2.uprim] : [calc.u, calc2.u];
    gx , gy , gz , flx,  fly,  flz  = calc.gradux,  calc.graduy,  calc.graduz , calc.fluxx,  calc.fluxy,  calc.fluxz;
    gx2, gy2, gz2, flx2, fly2, flz2 = calc2.gradux, calc2.graduy, calc2.graduz, calc2.fluxx, calc2.fluxy, calc2.fluxz;
    D = doweaklifting ? calc.dg.dhat_t : calc.dg.d_t;
    mc = mesh.mpiconn;
    mvol = mesh2.vol; sjac = mvol.sjac;

    @mpi begin
        rreqflux  = receivempi(calcc.fluxx.slave, RECVYOUR, mc )
        liftingfillfluxsurf!(  calc2.fluxx.slave, u2, mesh2, Mesh.mpisidesmine, doweaklifting)
        sreqflux  = sendmpi(   calcc.fluxx.slave, SENDMINE, mc)
    end
#
    if doconservativelifting
        # flux and volint in x/y/z direction
        liftingfillfluxvol!(flx2, fly2, flz2, u2, mvol, xdir)
        volint!(gx, flx, fly, flz, D)
        liftingfillfluxvol!(flx2, fly2, flz2, u2, mvol, ydir)
        volint!(gy, flx, fly, flz, D)
        liftingfillfluxvol!(flx2, fly2, flz2, u2, mvol, zdir)
        volint!(gz, flx, fly, flz, D)
    else
        # We compute the volume integral first and store it in the flux vector
        # The transformation is applied afterwards
        liftingvolint!(flx, fly, flz, u, D)
        applymetrics!(gx2.vol, gy2.vol, gz2.vol,  flx2.vol, fly2.vol, flz2.vol,  mvol.f, mvol.g, mvol.h, gx2.v)
    end
#
    liftingfillfluxsurf!(flx2.slave, u2, mesh2, Mesh.innersides,  doweaklifting)
#
    @mpi MPI.Waitall!([rreqflux; sreqflux]) # fluxslave: master -> slave
#
    liftingfillfluxnormvec!(flx2, fly2, flz2, mesh2)
    mortarsmalltobig(flx, calc.dg.mb, mesh, doweaklifting)
    mortarsmalltobig(fly, calc.dg.mb, mesh, doweaklifting)
    mortarsmalltobig(flz, calc.dg.mb, mesh, doweaklifting)
    surfint!(flx, gx.vol, calc.dg, mesh)
    surfint!(fly, gy.vol, calc.dg, mesh)
    surfint!(flz, gz.vol, calc.dg, mesh)
#
    # apply the Jacobian
    xv::A2, yv::A2, zv::A2, sjac::A2 = gx2.vol, gy2.vol, gz2.vol, mvol.sjac
    @inbounds for i = 1:size(xv,2); @simd for v=1:NVARLIFT
        xv[v,i] *= sjac[i]
        yv[v,i] *= sjac[i]
        zv[v,i] *= sjac[i]
    end; end
#
    prolongtoface!(gx, calc.dg.ip)
    prolongtoface!(gy, calc.dg.ip)
    prolongtoface!(gz, calc.dg.ip)
#
    mortarbigtosmall(gx, calc.dg.mb, mesh)
    mortarbigtosmall(gy, calc.dg.mb, mesh)
    mortarbigtosmall(gz, calc.dg.mb, mesh)
end
