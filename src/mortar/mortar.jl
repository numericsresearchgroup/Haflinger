@reexport module Mortar

using ..Haflinger
export MortarBasis, mortarsmalltobig, mortarbigtosmall, mortarbasis_smalltobig, mortarbasis_bigtosmall

struct MortarBasis{N,T}
    m01::SMatrix{N,N,T}
    m02::SMatrix{N,N,T}
    m10::SMatrix{N,N,T}
    m20::SMatrix{N,N,T}
end

"""
Basic Mortar initialization.
"""
function MortarBasis(n::I, nodetype) where I <: Integer
    m01, m02 = mortarbasis_bigtosmall(n, nodetype, n, nodetype)
    m10, m20 = mortarbasis_smalltobig(n, nodetype)
    MortarBasis(SMatrix{n,n,FT}(m01), SMatrix{n,n,FT}(m02),
                SMatrix{n,n,FT}(m10), SMatrix{n,n,FT}(m20))
end

"""
Build 1D operators for non-conforming interfaces:
   M01(:,:)  interpolation from full  interval 0: [-1,1] to left  interval 1: [-1,0]
   M02(:,:)  interpolation from full  interval 0: [-1,1] to right interval 2: [0, 1]
"""
function mortarbasis_bigtosmall(ns, nodetypes, nb, nodetypeb)
    xis, wgps, wbarys = nodesandweights(BigFloat, ns, nodetypes)
    xib, wgpb, wbaryb = nodesandweights(BigFloat, nb, nodetypeb)
    # build interpolation operators M 0->1,M 0->2
    m01 = initializevandermonde(wbaryb, xib, 0.5 .* (xis .- 1))
    m02 = initializevandermonde(wbaryb, xib, 0.5 .* (xis .+ 1))
    # ATTENTION: MortarBasis_BigToSmall computes the transposed matrices, which perform better for
    #            hand-written matrix multiplications. For the use with the intrinsic MATMUL, they must be transposed.
    Array{FT}(m01'), Array{FT}(m02')
end
mortarbasis_bigtosmall(n, nodetype) = mortarbasis_bigtosmall(n, nodetype, n, nodetype)

"""
Build 1D operators for non-conforming interfaces:
   M10(:,:)  projection    from left  interval 1: [-1,0] to full  interval 0: [-1,1]
   M20(:,:)  projection    from right interval 1: [0, 1] to full  interval 0: [-1,1]
TODO: Add projection mortar support for different nodetypes and polynomial degrees
"""
function mortarbasis_smalltobig(n, nodetype)
    xi,  wgp,  wbary  = nodesandweights(BigFloat, n, nodetype)
    xig, wgpg, wbaryg = nodesandweights(BigFloat, n, :nodetypeg)
    # build projection operators M 1->0,M 2->0
    # 1. interpolate to Gauss and multiply by weights
    vgp = diagm(0 => wgpg) * getvandermonde(n, nodetype, n, :nodetypeg)
    # 2. compute the Vandermonde on xGP (Depends on NodeType)
    legvdm = legendrevdm(xi)
    vphi1  = legendrevdm(0.5 .* (xig .- 1))
    vphi2  = legendrevdm(0.5 .* (xig .+ 1))
    # final Mortar: Vphi1
    m10  = legvdm * (vphi1' * vgp)
    m20  = legvdm * (vphi2' * vgp)
    # ATTENTION: MortarBasis_SmallToBig computes the transposed matrices, which is useful when they are used
    #            in hand-written matrix multiplications. For the use with the intrinsic MATMUL, they must be transposed.
    Array{FT}(m10'), Array{FT}(m20')
end

"""
Fills small non-conforming sides with data from the corresponding large side,
using 1D interpolation operators M01, M02.
This is used to obtain the face solution for flux computation.

      Type 1             Type 2              Type3
        η                  η                   η
        ^                  ^                   ^
        |                  |                   |
    +---+---+          +---+---+           +---+---+
    | 3 | 4 |          |   2   |           |   |   |
    +---+---+ ---> ξ   +---+---+ --->  ξ   + 1 + 2 + ---> ξ
    | 1 | 2 |          |   1   |           |   |   |
    +---+---+          +---+---+           +---+---+
"""
function mortarbigtosmall(master::AbstractArray{T,4}, slave::AbstractArray{T,4},
                          tmp::AbstractArray{T,4}, mb::MortarBasis{N,T}, mesh,
                          v::Val{V}, n::Val{N}) where {T,N,V}
    m1, m2 = mb.m01, mb.m02
    sp, mi, fs2m = mesh.conn.sideprops, mesh.conn.mortarinfo, mesh.mappings.fs2m

    @inbounds for s in 1:size(master,4)
        mtype = sp[SP_MORTAR, s]
        if !(mtype in 1:3)
            continue
        end
        @views mortarbigtosmall(master[:,:,:,s], tmp[:,:,:,1:4], tmp[:,:,:,5:6], mtype, m1, m2, v)

        for imortar in 1:(mtype == 1 ? 4 : 2)
            side, flip = mi[MI_SIDE, imortar, s], mi[MI_FLIP, imortar, s]
            if flip == 1
                @views copy!(master[:,:,:,side], tmp[:,:,:,imortar])
            else
                flipside!(slave,  side, tmp, imortar, fs2m, flip, v, n)
            end
        end
    end
end
@inline mortarbigtosmall(d::AbstractData, mb::MortarBasis, mesh) = mortarbigtosmall(d.master, d.slave, d.face, mb::MortarBasis, mesh, d.v, d.n)

function mortarbigtosmall(big::AbstractArray{T,3}, small::AbstractArray{T,4},
                                  tmp::AbstractArray{T,4}, mortartype,
                                  m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, v::Val{V}) where {T,N,V}
    @views if     mortartype == 1 # 1 -> 4
        # first in η-dir, then 2 x ξ-dir
        etamortarbigtosmall(big         ,   tmp[:,:,:,1:2], m1, m2, v)
        ximortarbigtosmall( tmp[:,:,:,1], small[:,:,:,1:2], m1, m2, v)
        ximortarbigtosmall( tmp[:,:,:,2], small[:,:,:,3:4], m1, m2, v)
    elseif mortartype == 2 # 1 -> 2 η-dir
        etamortarbigtosmall(big         , small[:,:,:,1:2], m1, m2, v)
    elseif mortartype == 3 # 1 -> 2 ξ-dir
        ximortarbigtosmall( big         , small[:,:,:,1:2], m1, m2, v)
    end
end

@inline function etamortarbigtosmall(big::AbstractArray{T,3}, small::AbstractArray{T,4},
                    m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, ::Val{V}) where {T, N, V}
    # for every ξ-layer perform mortar operation in η-dir
    @inbounds for q = 1:N, p = 1:N
        @simd for v = 1:V
            small[v,p,q,1] =      m1[1,q] * big[v,p,1]
            small[v,p,q,2] =      m2[1,q] * big[v,p,1]
        end
        for l = 2:N
            @simd for v = 1:V
                small[v,p,q,1] += m1[l,q] * big[v,p,l]
                small[v,p,q,2] += m2[l,q] * big[v,p,l]
            end
        end
    end
end
@inline function ximortarbigtosmall(big::AbstractArray{T,3}, small::AbstractArray{T,4},
                    m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, ::Val{V}) where {T, N, V}
    # for every η-layer perform mortar operation in ξ-dir
    @inbounds for q = 1:N, p = 1:N
        @simd for v = 1:V
            small[v,p,q,1] =      m1[1,p] * big[v,1,q]
            small[v,p,q,2] =      m2[1,p] * big[v,1,q]
        end
        for l = 2:N
            @simd for v = 1:V
                small[v,p,q,1] += m1[l,p] * big[v,l,q]
                small[v,p,q,2] += m2[l,p] * big[v,l,q]
            end
        end
    end
end


"""
Fills master side from small non-conforming sides, using 1D projection operators M10, M20.
This function can e.g. be used to project the numerical flux at the small sides of the
nonconforming interface to the corresponding big sides.
"""
function mortarsmalltobig(master, slave, tmp, mb::MortarBasis, mesh,
                          v::Val{V}, n::Val{N}, weak) where {V, N}
    m1, m2 = mb.m10, mb.m20
    sp, mi, fs2m = mesh.conn.sideprops, mesh.conn.mortarinfo, mesh.mappings.fs2m

    sign = weak ? -1 : 1
    @inbounds for s in 1:size(master,4)
        mtype = sp[SP_MORTAR, s]
        if !(mtype in 1:3)
            continue
        end
        nmortars = mtype == 1 ? 4 : 2
        for imortar in 1:nmortars
            side, flip = mi[MI_SIDE, imortar, s], mi[MI_FLIP, imortar, s]
            if flip == 1
                flipside!(tmp, imortar, master, side, fs2m, flip, v, n)
                @views copy!(master[:,:,:,side], tmp[:,:,:,imortar])
            else
                flipside!(tmp, imortar, slave,  side, fs2m, flip, v, n, sign)
            end
        end
        @views mortarsmalltobig(tmp[:,:,:,1:nmortars], master[:,:,:,s], tmp[:,:,:,5:6], mtype, m1, m2, v)
    end
end
@inline mortarsmalltobig(d::AbstractData, mb::MortarBasis, mesh, weak) =
        mortarsmalltobig(d.master, d.slave, d.face, mb::MortarBasis, mesh, d.v, d.n, weak)

function mortarsmalltobig(small, big, tmp, mortartype,
                    m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, v::Val{V}) where {T,N,V}
   @views if     mortartype == 1 # 1 -> 4
       # first 2 x in ξ-dir, then η-dir
       ximortarsmalltobig( small[:,:,:,1:2], tmp[:,:,:,1], m1, m2, v)
       ximortarsmalltobig( small[:,:,:,3:4], tmp[:,:,:,2], m1, m2, v)
       etamortarsmalltobig(  tmp[:,:,:,1:2], big         , m1, m2, v)
   elseif mortartype == 2 # 1 -> 2 η-dir
       etamortarsmalltobig(small[:,:,:,1:2], big         , m1, m2, v)
   elseif mortartype == 3 # 1 -> 2 ξ-dir
       ximortarsmalltobig( small[:,:,:,1:2], big         , m1, m2, v)
   end
end

@inline function etamortarsmalltobig(small::AbstractArray{T,4}, big::AbstractArray{T,3},
                    m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, ::Val{V}) where {T, N, V}
    # for every ξ-layer perform mortar operation in η-dir
    @inbounds for q = 1:N, p = 1:N
        @simd for v = 1:V
            big[v,p,q]      = m1[1,q] * small[v,p,1,1] + m2[1,q] * small[v,p,1,2]
        end
        for l = 2:N
            @simd for v = 1:V
                big[v,p,q] += m1[l,q] * small[v,p,l,1] + m2[l,q] * small[v,p,l,2]
            end
        end
    end
end
@inline function ximortarsmalltobig(small::AbstractArray{T,4}, big::AbstractArray{T,3},
                    m1::SMatrix{N,N,T}, m2::SMatrix{N,N,T}, ::Val{V}) where {T, N, V}
    # for every η-layer perform mortar operation in ξ-dir
    @inbounds for q = 1:N, p = 1:N
        @simd for v = 1:V
            big[v,p,q]        = m1[1,p] * small[v,1,q,1] + m2[1,p] * small[v,1,q,2]
        end
        for l = 2:N
            @simd for v = 1:V
                big[v,p,q]   += m1[l,p] * small[v,l,q,1] + m2[l,p] * small[v,l,q,2]
            end
        end
    end
end

end
