begin
  local timediscname = "Standard RK3-3"
  local nrkstages = 3
  local relativedfl = 1.
  local fullboundaryorder = true
  local func = timestepbylserkw2
  local rktype = LSERKW2
  local rkop   = LSERKW2Op
  
  # CFL scaling factors derived from the analysis of the operator for linear scalar advection
  # NOTE: Overintegration with Gauss-Lobatto nodes results in a projection DG formulation, i.e. we have to use the Gauss nodes timestep
  local cflscalealphag  = [ 1.2285, 1.0485, 0.9101, 0.8066, 0.7268, 0.6626, 0.6109, 0.5670, 0.5299, 0.4973, 0.4703, 0.4455, 0.4230, 0.4039, 0.3859 ]
  local cflscalealphagl = [ 3.1871, 2.2444, 1.7797, 1.5075, 1.3230, 1.1857, 1.0800, 0.9945, 0.9247, 0.8651, 0.8134, 0.7695, 0.7301, 0.6952, 0.6649 ]
  local cflscalefv = 1.2285
  
  local rka = [ 0,   5/9,   153/128 ]
  local rkb = [ 1/3, 15/16, 8/15    ]
  local rkc = [ 0,   1/3,   3/4     ]
  
  local sv = SVector{nrkstages,FT}
  @rk lowercase(replace(timediscname, " " => "")) => rktype( 
        RKParams(timediscname, fullboundaryorder, cflscalealphag, cflscalealphagl, relativedfl, func, rkop),
        sv(rka), sv(rkb), sv(rkc) )

end
