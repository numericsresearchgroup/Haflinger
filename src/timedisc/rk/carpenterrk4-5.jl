begin
  local timediscname = "Carpenter RK4-5"
  local nrkstages = 5
  local relativedfl = 1.853
  local fullboundaryorder = false
  local func = timestepbylserkw2
  local rktype = LSERKW2
  local rkop   = LSERKW2Op
  
  # CFL scaling factors derived from the analysis of the operator for linear scalar advection
  # NOTE: Overintegration with Gauss-Lobatto nodes results in a projection DG formulation, i.e. we have to use the Gauss nodes timestep
  local cflscalealphag  = [ 2.0351, 1.7595, 1.5401, 1.3702, 1.2375, 1.1318, 1.0440, 0.9709, 0.9079, 0.8539, 0.8066, 0.7650, 0.7290, 0.6952, 0.6660 ]
  local cflscalealphagl = [ 4.7497, 3.4144, 2.8451, 2.4739, 2.2027, 1.9912, 1.8225, 1.6830, 1.5682, 1.4692, 1.3849, 1.3106, 1.2454, 1.1880, 1.1362 ]
  local cflscalefv = 2.0351
  
  local rka = [
                 0,
                 567301805773 /  1357537059087,
                2404267990393 /  2016746695238,
                3550918686646 /  2091501179385,
                1275806237668 /   842570457699
              ]

  local rkb = [
                1432997174477 /  9575080441755,
                5161836677717 / 13612068292357,
                1720146321549 /  2090206949498,
                3134564353537 /  4481467310338,
                2277821191437 / 14882151754819 
              ]

  local rkc = [ 
                0,
                1432997174477 /  9575080441755,
                2526269341429 /  6820363962896,
                2006345519317 /  3224310063776,
                2802321613138 /  2924317926251  
              ]
  
  local sv = SVector{nrkstages,FT}
  @rk lowercase(replace(timediscname, " " => "")) => rktype( 
        RKParams(timediscname, fullboundaryorder, cflscalealphag, cflscalealphagl, relativedfl, func, rkop),
        sv(rka), sv(rkb), sv(rkc) )

end
