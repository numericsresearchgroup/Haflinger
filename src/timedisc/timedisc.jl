__precompile__(false)
module Timedisc

using ..Haflinger
using Dates

include("timedisc_vars.jl")

export timediscop, dotimestep
export iter, iterloc, maxiter, tend, outputdt, analyzedt, ncalctimestep, cfl, dfl, dtminold, dtmin, t

iter          = -1
iterloc       = -1
maxiter       = -1
tend          = -1.
outputdt      = -1.
analyzedt     = -1.
ncalctimestep = 0
cfl           = -1.
dfl           = -1.
dtminold      = -999.
t             = 0.
dtmin         = 0.

"""
Get information for end time and max time steps from ini file
"""
function inittimedisc(params)
    global timediscinitisdone

    if timediscinitisdone
        mprint("InitTimeDisc already called.")
        return
    end
    initstart("TIMEDISC")

    global timediscmethod   = getparameter(params, "timediscmethod" ; required = false, default = "Carpenter RK4-5" )
    # Read the end time TEnd from ini file
    global tend             = getparameter(params, "tend"           ; required = true )
    # Read the normalized CFL number
    global cflscale         = getparameter(params, "cflscale"       ; required = true )
    # Read the normalized viscous timestep coefficient
    global dflscale = USEPARABOLIC ? getparameter(params, "dflscale"; required = true ) : -1.
    # read max number of iterations to perform
    global maxiter          = getparameter(params, "maxiter"        ; required = true, default=-1 )
    global maxcalctimestep  = getparameter(params, "maxcalctimestep"; required = true, default=3 )

    global analyzedt        = getparameter(params, "analyzedt"      ; required = false, default=tend )
    global outputdt         = getparameter(params, "outputdt"       ; required = false, default=analyzedt )
#    global analyzeiter = getparameter(params,"analyzeiter"; required=false, default=100 )

    tmp = replace(lowercase(timediscmethod), " " => "")
    rk = timediscs[tmp]
    global timediscop = rk.params.rkop(rk, size(calc.ut))
    global dotimestep = rk.params.timestepfunc

    nfilter = nunder = N #TODO: no overintegration yet
    nadv = min(N, nfilter, nunder)
    global cfl; global dfl
    cfl, dfl = fillcfldfl(cflscale, dflscale, nadv-1, N-1, rk.params, NODETYPE)
    minfo(" Method of time integration: $(rk.params.name)")

    global dtelem = zeros(FT,nelems,2)
    # TODO implement addtoelem
    #addtoelemdata('dt',dtelem)

    timediscinitisdone = true
    initdone("TIMEDISC")
end

"""
GTS Temporal discretization
"""
function timedisc(starttime)
    global t, tend, tanalyze, toutput, dtmin

    t = dorestart ? restarttime : 0.

    # No computation needed if tEnd=tStart!
    if t ≥ tend || maxiter == 0
        mprintln(" No simulation, since t ≥ tend!")
        return
    end

    # Determine next write time, since it will be written into output file
    tstart   = t
    toutput  = t + outputdt
    tanalyze = t + analyzedt
    iter = iterloc = 0
    doanalyze = dooutput = dofinalize = false
    ncalctimestep = 0
    dtminold      = -999.
    mem           = -999999999999999999

    # compute initial timestep
    dta, dtv, err, dogc = calctimestep(calc, dtelem, mem)
    checkerr(err)
    dt = USEPARABOLIC ? min(dta * cfl, dtv * dfl) : dta * cfl
    # Do first RK stage of first timestep to fill gradients
    dgoperator!(calcs, meshs, t)

    # Write the state at time=0, i.e. the initial condition
    dorestart ? mprintln(" Rewriting solution:") : mprintln(" Writing initial solution:")
    writestate(calc, t; iserrorfile=false)
    mprintln(" ...done!")

    #visualize(t,u) TODO: no visu yet

    ## fill recordpoints buffer (initialization/restart)
    #if hasrp
    #    error("no record points yet")
    #    recordpoints(iter, t, true)
    #end

    # Run initial analyze
    mprint("CALCULATION RUNNING...\n")
    timer = mpitime()
    @root showinfo(iterloc, dt, dta, dtv)
    analyze!(t, iter)

    # Run computation
    done = false
    timer = mpitime()
    while !done
        if ncalctimestep < 1
            dta, dtv, err, dogc = calctimestep(calc, dtelem, mem)
            mem = dogc ? rungc() : mem
            checkerr(err)
            dtmin = USEPARABOLIC ? min(dta*cfl,dtv*dfl) : dta*cfl
            # heuristic for detecting recomputation frequency of timestep size
            ncalctimestep = min(floor(abs(log10(abs(dtminold / dtmin - 1)^2 * 100 + eps(FT)))), maxcalctimestep)
            dtminold = dtmin
        end
        ncalctimestep -= 1

        dt = dtmin
        dtanalyze = dtoutput = dtend = typemax(t)
        dttol = dt * (1. + 1.e-4)

        if tanalyze - t ≤ dttol
            dtanalyze = tanalyze - t
            doanalyze = true
        end
        if toutput - t ≤ dttol
            dtoutput = toutput - t
            doanalyze = dooutput = true
        end
        if tend - t ≤ dttol
            dtend = tend - t
            doanalyze = dooutput = dofinalize = true
        end
        dt = min(dt, dtanalyze, dtoutput, dtend)

        #if docalctimeaverage
        #    calctimeaverage(false, dt, t)
        #end
        #if dotcsource
        #    calcforcing(t, dt)
        #end

        #printstatusline(t, dt, tstart, tend)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Perform Timestep using global time stepping routine
        # Attention: time dependent BC only provided for RK3
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        dotimestep(timediscop,t,dt)

        iter    += 1
        iterloc += 1
        t       += dt
        if iter == maxiter
            tanalyze  = toutput  = tend       = t
            doanalyze = dooutput = dofinalize = true
        end

        # DG operator to fill face data, fluxes, gradients for analyze
        if doanalyze
            dgoperator!(calcs, meshs, t)
        end

        # your Analysis Routine for your Testcase here.
        #if mod(iter, nanalyzetestcase) == 0 || doanalyze
        #    analyzetestcase(t)
        #end
        ## evaluate recordpoints
        #if hasrp
        #    recordpoints(iter, t, doanalyze)
        #end

        # Check GC, analyze and output now
        if doanalyze
            loctime = mpitime()
            @root showinfo(iterloc, dtmin, dta, dtv, timer, loctime)
            # do analysis
            analyze!(t, iter)
            iterloc = 0
            tanalyze  += analyzedt
            timer = mpitime()
        end
        if dooutput
            # Visualize data and write solution
            # Write various derived data

            #if docalctimeaverage
            #  calctimeaverage(true,dt,t)
            #end
            #if hasrp
            #  writerp(t,true)
            #end

            # Write state file
            # NOTE: this should be last in the series, so we know all previous data
            # has been written correctly when the state file is present
            writestate(calc, t; iserrorfile = false)
            ## Visualize data
            #visualize(t, u)
            toutput += outputdt
            timer = mpitime()
            dooutput = false
        end
        if doanalyze || dooutput
            loctime = mpitime()
            mprintln("."^100)
            mprintln(dofinalize ? " HAFLINGER FINISHED!  [ $(loctime - starttime) sec ]" :
                                  " HAFLINGER RUNNING $projectname... [ $(loctime - starttime) sec ]")
            mprintln("-"^100)
            doanalyze  = false
        end
        if dofinalize
            break
        end

    end
end

"""
Scaling of the CFL number.
For N=1-10 input CFLscale can now be (nearly) 1. and will be scaled adequately depending on
polynomial degree N, NodeType and TimeDisc method.
(see paper Gassner, Kopriva: 'A comparision of the Gauss and Gauss-Lobatto DGSEM for Wave Propagation Problems' .
"""
function fillcfldfl(cflscale, dflscale, ncfl, ndfl, par, nodetype)

    ############################ CFL ########################################
    # CFL in DG depends on the polynomial degree
    # Runge-Kutta methods

    alphas   = nodetype == :nodetypegl ? par.cflscalealphagl : par.dflscalealphag
    maxn     = length(alphas)
    alpha    = alphas[min(ncfl, maxn)]
    cflscale = cflscale * alpha
    if ncfl > maxn || cflscale > alpha
        minfo("!"^100)
        minfo("Warning: The chosen CFL number may be too high for the selected polynomial degree!")
        minfo("!"^100)
    end
    #scale with 2n+1
    cfl = cflscale / (2 * ncfl + 1)

    ############################ DFL ########################################
    # DFL in DG depends on the polynomial degree
    # since DFl is only on real axis, stability numbers are defined for RK3 and then scaled for RK4

    alphas   = nodetype == :nodetypegl ? dflscalealphagl : dflscalealphag
    maxn     = length(alphas)
    alpha    = alphas[min(ndfl, maxn)] * par.relativedfl
    dflscale = dflscale * alpha
    if ndfl > maxn || dflscale > alpha
        minfo("!"^100)
        minfo("Warning: The chosen DFL number may be too high for the selected polynomial degree!")
        minfo("!"^100)
    end
    dfl = dflscale / (2 * ndfl + 1)^2

    mprintln(" CFL: $cfl / DFL: $dfl")
    cfl, dfl
end

function showinfo(iter, dtmin, dta, dtv, starttime = -1., endtime = -1.)
    # Get calculation time per DOF
    println("-"^100)
    println(" Sim time   :  $t")
    println(" Timestep   :  $dtmin", dtv < dta ? " Viscous timestep dominates!" : "")
    println(" #Timesteps :  $iter")
    if starttime > 0
        pid = (endtime - starttime) * nmpiprocs / (nglobalelems * N^3 * iter) / length(timediscop.coeffs.a)
        println(" PID        :  [ $pid s ]")
    end
    println(" Date       :  $(Dates.now())")
end

function checkerr(err)
    if     err == 1
        safeerror("Convective timestep is NaN. Time: $t")
    elseif err == 2
        safeerror("Diffusive  timestep is NaN. Time: $t")
    elseif err == 2
        safeerror("Convective and diffusive timesteps are NaN. Time: $t")
    elseif err == 2
        safeerror("Solution is NaN: Time: $t")
    end
end

end
