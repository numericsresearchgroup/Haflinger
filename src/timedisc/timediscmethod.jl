export timestepbylserkk3, timestepbylserkw2

"""
Low-Storage Runge-Kutta integration, 2 register version with notation from Ketcheson paper.
This procedure takes the current time t, the time step dt and the solution at
the current time U(t) and returns the solution at the next time level.
RKA/b/c coefficients are low-storage coefficients, NOT the ones from Butcher table.
"""
function timestepbylserkw2(op::LSERKW2Op, t::FT, dt::FT)
    @inbounds begin
    rk = op.coeffs
    A = Array{FT,5}
    s1::A, ut::A, s2::A = calc.u.vol, calc.ut, op.s2
  
    # premultiply with dt
    bdt = rk.b .* dt
  
    dgoperator!(calcs, meshs, t)
    @simd for i in eachindex(ut)
      s2[i]  = ut[i]
      s1[i] += ut[i]*bdt[1]
    end
  
    # following steps
    for istage = 2:length(bdt)
      tstage = t + dt * rk.c[istage]
      dgoperator!(calcs, meshs, tstage)
      @simd for i in eachindex(ut)
        s2[i]  = ut[i] - s2[i] * rk.a[istage]
        s1[i] +=         s2[i] *  bdt[istage]
      end
    end
    end
end


"""
Low-Storage Runge-Kutta integration:  3 register version with notation from Ketcheson paper.
This procedure takes the current time t, the time step dt and the solution at
the current time U(t) and returns the solution at the next time level.
RKA/b/c coefficients are low-storage coefficients, NOT the ones from butcher table.
"""
function timestepbylserkk3(op::LSERKK3Op, t::FT, dt::FT)
    @inbounds begin
    rk = op.coeffs
    A = Array{FT,5}
    s1::A, s2::A, s3::A, ut::A = calc.u.vol, op.s2, op.s3, calc.ut

    # Nomenclature:
    # S1 == U, S3 == U_n
  
    # premultiply with dt
    bdt = rk.b .* dt
  
    # first evaluation of DG operator already done in timedisc
    dgoperator!(calcs, meshs, t)
    @simd for i in eachindex(ut)
      s1[i] += ut[i]*bdt[1]
      s2[i]  = s1[i]
      s3[i]  = s1[i]
    end
  
    # following steps
    for istage = 2:length(bdt)
      tstage = t + dt * rk.c[istage]
      dgoperator!(calcs, meshs, tstage)
      @simd for i in eachindex(ut)
        s2[i] += s1[i] * rk.δ[istage]
        s1[i]  = rk.γ1[istage] * s1[i] + rk.γ2[istage] * s2 + rk.γ3 * s3[i] + ut[i] * bdt[istage]
      end
    end
    end
end
