export LSERKW2, LSERKK3, LSERKW2Op

timediscinitisdone = false
cfl = -1.
dfl = -1.

abstract type RK{N,T} end
abstract type RKOp{N,T} end

struct RKParams{T}
  name::String
  fullboundaryorder::Bool
  cflscalealphag::Vector{T}
  cflscalealphagl::Vector{T}
  relativedfl::T
  timestepfunc::Function
  rkop::Type
end

struct LSERKW2{N,T} <: RK{N,T}
  params::RKParams{T}
  a::SVector{N,T}
  b::SVector{N,T}
  c::SVector{N,T}
end
struct LSERKW2Op{N,T} <: RKOp{N,T}
  coeffs::LSERKW2{N,T}
  s2::Array{T}
end
LSERKW2Op(coeffs, s::NTuple{I,Integer}) where I = LSERKW2Op(coeffs, zeros(s))

struct LSERKK3{N,T} <: RK{N,T}
  params::RKParams{T}
  γ1::SVector{N,T}
  γ2::SVector{N,T}
  γ3::SVector{N,T}
  b::SVector{N,T}
  c::SVector{N,T}
  δ::SVector{N,T}
end
struct LSERKK3Op{N,T} <: RKOp{N,T}
  coeffs::LSERKK3{N,T}
  s2::Array{T}
  s3::Array{T}
end
LSERKK3Op(coeffs, s::NTuple{I,Integer}) where I = LSERKK3Op(coeffs, zeros(s), zeros(s))

dflscalealphag  = [ 1.12, 0.76, 0.55, 0.41, 0.32, 0.25, 0.20, 0.17, 0.14, 0.12 ]
dflscalealphagl = [ 4.50, 1.95, 1.18, 0.79, 0.56, 0.42, 0.32, 0.25, 0.20, 0.17 ]

include("timediscmethod.jl")

timediscs = Dict{String,RK}()
macro rk(val)
  return :(push!(timediscs,$(esc(val))))
end

rkdir = joinpath(Base.source_dir(),"rk")
for file in readdir(rkdir)
  include(joinpath("rk",file))
end
export timediscs
