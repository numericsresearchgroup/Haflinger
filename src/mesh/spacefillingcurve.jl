module SpaceFillingCurve
# This code should eventually go into a separate package once

export SFCSetup, sortbyspacefillingcurve
export bitget, bitset, bitclear

struct SFCSetup{U,F}
    bmin::Array{F}     # smallest x,y,z values of bounding box
    bmax::Array{F}     # smallest x,y,z values of bounding box
    spacing::Array{F}  # dx,dy,dz size of boxes
    nbits::U           # number of bits for each direction
    intfact::U         # number of boxes in each direction
    curve::Function
    function SFCSetup{U,F}(bmin, bmax, sfc=morton_c2i) where {U,F}
        blen = bmax - bmin
        nbits = div(sizeof(U)*8,3)
        intfact = 2^nbits-1
        spacing = intfact ./ blen
        new(bmin, bmax, spacing, nbits, intfact ,sfc)
    end
end

# get / set (=1) / clear (=0) bit at position p in number N
bitget(N::I,p::I)   where I = (N >> p) & I(1) != I(0)
bitset(N::I,p::I)   where I =  N |=   I(2)^p
bitclear(N::I,p::I) where I =  N &= ~(I(2)^p)

const ST = UInt128

function sortbyspacefillingcurve(coords::AbstractArray{T,2},
                                 keys::AbstractVector{I},
                                 boundingboxtype::Symbol) where {I,T}

    @assert length(keys) == size(coords)[2]

    if length(keys) == 1
        keys[1] = 1
        return
    end
    
    # Determine extreme verticies for bounding box
    lower, upper = getboundingbox(coords, boundingboxtype)
    sfc = SFCSetup{ST, T}(lower, upper)
    
    cart = zeros(ST, 3)
    sfcind = similar(keys, ST)
    @views for i=1:length(keys)
      # Compute the integer discretization in each direction
      cart .= round.(ST, (coords[:,i] .- sfc.bmin) .* sfc.spacing)
      # Map the three coordinates to a single integer value.
      sfcind[i] = sfc.curve(cart)
    end
    
    # Now sort the elements according to their index on the space filling curve.
    s = collect(zip(sfcind, keys))
    sort!(s, by = x -> x[1] )
    sortedkeys = [i[2] for i in s]
end

function getboundingbox(coords::AbstractArray{T,2}, boundingboxtype) where T
    lower, upper = zeros(T,3), zeros(T,3)
    if boundingboxtype == :independent
        for d = 1:3
            lower[d] = minimum(coords[d,:])
            upper[d] = maximum(coords[d,:])
        end
    elseif boundingboxtype == :cube
        lower   .= minimum(coords)
        upper   .= maximum(coords)
    end
    for d=1:3
      if lower[d] ≈ upper[d]
        lower[d] -= sqrt(eps(T))
        upper[d] += sqrt(eps(T))
      end
    end
    lower, upper
end


function morton_c2i(intcoords::AbstractVector{U}) where U <: Unsigned 
    nbits = U(div(sizeof(U)*8,3))
    ind = U(0)
    for dir=1:3
        # interleave the directions, start from pos 0
        for i=0:nbits-1
          if bitget(intcoords[dir],i)
              ind = bitset(ind,U(3*i-dir+3))
          end
        end
    end
    ind
end

end