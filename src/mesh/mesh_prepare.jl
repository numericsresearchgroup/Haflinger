#Contains subroutines to collect and condense mesh topology information (master,slave,flip)
#and define communication neighbours.

"""
This routine sorts sides into three groups containing BC sides, inner sides and MPI sides in the following manner:

* BCSides         : sides with boundary conditions (excluding periodic BCs)
* InnerMortars    : "virtual" sides introduced for collecting the data of the small sides at a non-conforming interface
* InnerSides      : normal inner sides
* MPI_MINE sides  : MPI sides to be processed by the current processor (e.g. flux computation)
* MPI_YOUR sides  : MPI sides to be processed by the neighbour processor
* MPIMortars      : mortar interfaces to be comunicated

Each side can be accessed through its SideID defining its position in the processor local side list.
The routine furthermore sets the MPI masters and slave sides.
"""
function setlocalsideids(bcs,elems,n,periodicbcmap)
    # ----------------------------------------
    # Set side IDs to arrange sides:
    # 1. BC sides
    # 2. inner sides
    # 3. MPI sides
    # MPI Sides are not included here!
    # ----------------------------------------

    # For each side set "sideID" to -1 (indicates non-assigned side) and for periodic
    # sides set BCindex of the slave side to the index of the master side using PeriodicBCMap.
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      side.sideid = -1
      # periodic master and slave side have different BCs (i.e. different BCindex).
      # = overwrite BCindex of the slave side by the BCindex of the master side.
      if side.bcindex ≥ 1 # side is at BC
        if periodicbcmap[side.bcindex] ≠ -1 # side is slave side of a periodic side = store BC index of master side
          side.bcindex = periodicbcmap[side.bcindex]
        end
      end
    end

    iside = ibcside = 0
    imortarinnerside = n.bcsides               # inner Mortar sides come directly after BC sides
    iinnerside = n.bcsides + n.mortarinnersides # inner (non-Mortar) side come next
    imortarmpiside = n.sides - n.mortarmpisides # MPI Mortar sides come last
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      if side.sideid == -1   # SideID not set so far
        if side.nbproc == -1 # no MPI Sides
          if isdefined(side, :connection) # side has a connection = inner side
            iinnerside += 1
            iside += 1
            side.sideid = side.connection.sideid = iinnerside # set SideID for this side and connected side
          else  # side has no connection = big Mortar or BC side
            if side.mortartype > 0 # if big Mortar side
              if side.tmp == -1         # if MPI Mortar side
                imortarmpiside += 1
                side.sideid = imortarmpiside
              else                         # else (non-MPI Mortar side)
                imortarinnerside += 1
                iside += 1
                side.sideid = imortarinnerside
              end # MPI mortar
            else                           # no Mortar side = this is now a BC side, really!
              ibcside += 1
              iside += 1
              side.sideid = ibcside
            end # mortar
          end
        end #  ! MPISide
      end # sideID EQ -1
    end
    if iside ≠ n.innersides + n.bcsides + n.mortarinnersides
      terminate("Not all SideIDs are set! $(iside), $(n.innersides), $(n.bcsides), $(nmortarinnersides) ")
    end
end

function MPIConnectivity(elems, mpielemoffset, n)
    # Count MPI sides per neighbor proc
    mpisidecount = zeros(Int, nmpiprocs)
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
        if side.nbproc ≠ -1 # count total number of MPI sides and number of MPI sides for each neighboring processor
          mpisidecount[side.nbproc+1] += 1
        end
    end
    # Define variables used for MPI communication:
    #   - nBnProcs: count of neighboring processors (sharing a side with this one)
    #   - NbProc(1:nNbProcs): global index of each neigboring processor
    #   - nMPISides_Proc(1:nNbProcs): count of common sides with each neighboring processor
    nnbprocs=sum(mpisidecount .> 0)
    nbproc, nmpisides_proc = [zeros(Int,nnbprocs) for i = 1:2]
    # build a mapping 1..nNbProcs to the global number of processors (allows to iterate over all neighboring processors)
    # and store for each neighboring processor the number of common sides
    inbproc=0
    for iproc=1:nmpiprocs
      if mpisidecount[iproc] > 0
        inbproc += 1
        nbproc[inbproc] = iproc-1  # map neighboring proc number to global proc number
        nmpisides_proc[inbproc] = mpisidecount[iproc] # store number of common sides
      end
    end

    # SPLITTING number of MPISides in MINE and YOURS
    # General strategy:
    # For each neighboring processor split the number of common sides with this proc in two halves.
    # The processor with the smaller rank gets #commonSides/2, the processor with the higher rank
    # gets the rest: #commonSides - #commonSides/2
    nmpisidesmine_proc, nmpisidesyour_proc = [zeros(Int,nnbprocs) for i=1:2]
    for i = 1:nnbprocs
      nmpisidesmine_proc[i] = myrank < nbproc[i] ? nmpisides_proc[i] ÷ 2 :
                                                   nmpisides_proc[i]-nmpisides_proc[i] ÷ 2
    end
    nmpisidesyour_proc = nmpisides_proc - nmpisidesmine_proc
    n.mpisidesmine = sum(nmpisidesmine_proc)
    n.mpisidesyour = sum(nmpisidesyour_proc)

    # Since we now know the number of MINE and YOUR sides for each neighboring processor we
    # can calculate the SideID offsets for each processor.
    # The common sides with one processor are all stored consecutively and with ascending neighbor rank.
    # The offsets hold the first index in the list of all sides for each neighbor processor.
    # ATTENTION: "offsetMPISides_MINE" and "offsetMPISides_YOUR" have another indexing than
    #    "nMPISides_MINE_Proc" and "nMPISides_YOUR_Proc". The latter ones start with index "1", while the
    #    offset arrays start with "0"!
    offsetmpisidesmine, offsetmpisidesyour = [zeros(Int, nnbprocs+1) for i=1:2]
    for i=2:nnbprocs+1
      offsetmpisidesmine[i] = offsetmpisidesmine[i-1] + nmpisidesmine_proc[i-1]
      offsetmpisidesyour[i] = offsetmpisidesyour[i-1] + nmpisidesyour_proc[i-1]
    end
    offsetmpisidesmine .+= (n.innersides + n.bcsides + n.mortarinnersides)
    offsetmpisidesyour .+= offsetmpisidesmine[nnbprocs+1]

    # Check if the side has a neighbor with the respective rank, otherwise cycle.
    # In total this is an iteration over all processor and all common sides with this processor.
    # The global side indices of these sides are stored in a map "SideIDMap", which is sorted.
    # Therewith this map holds all global side indices (sorted) of all common sides with the specific neighbor-processor.
    # The sides are then enumerated consecutively according to this map.
    USEMPI ? MPI.Barrier(MPI.COMM_WORLD) : nothing
    for inbproc=1:nnbprocs
        sideidmap = zeros(Int, nmpisides_proc[inbproc])
        iside=0
        for elem in elems, locside in elem.side, side in [locside; locside.mortars]
          if side.nbproc ≠ nbproc[inbproc]
            continue
          end
          iside += 1
          # optimization: put non-mortars first to optimize addtoInnerMortars (also small virtual sides are marked with MortarType<0)
          if side == locside && side.mortartype == 0
             side.ind = -side.ind
          end
          sideidmap[iside] = side.ind # fill map with global Side indices
        end
        if iside > 1
            @views sort!(sideidmap[1:iside]) # sort by global side index
        end
        # again loop over all common sides with the specific neighboring processor and assign SideIDs according to the "SideIDMap".
        for elem in elems, locside in elem.side, side in [locside; locside.mortars]
          if side.nbproc ≠ nbproc[inbproc]
            continue
          end
          # For the side get index of global index in sorted SideIDMap and set this as SideID of the side (offset is added below).
          s = searchsortedfirst(sideidmap, side.ind)
          @assert sideidmap[s] == side.ind ""
          # Since SideIDMap starts with index "1", this SideID must be shifted further by the offsetMPISides_MINE/YOUR.
          # ATTENTION: indexing of offsetMPISides_MINE and offsetMPISides_YOUR starts with "0".
          if myrank < side.nbproc
            s += (s ≤ nmpisidesmine_proc[inbproc]) ? offsetmpisidesmine[inbproc] :
                                                    (offsetmpisidesyour[inbproc] - nmpisidesmine_proc[inbproc])
          else
            s += (s ≤ nmpisidesyour_proc[inbproc]) ? offsetmpisidesyour[inbproc] :
                                                    (offsetmpisidesmine[inbproc] - nmpisidesyour_proc[inbproc])
          end # myrank<NbProc
          side.sideid = s
        end
    end

    # Revert the negative SideIDs used to put non-mortars at the top of the list.
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      side.ind = abs(side.ind)
    end

    if n.mortarsides > 0
      optimizemortars(elems, n, offsetmpisidesmine, offsetmpisidesyour)
    end
    # Copy data into some MPI arrays and set number of sides and offset send/receive cases:
    # SEND MINE - RECEIVE YOUR == 1,  SEND YOUR - RECEIVE MINE == 2
    nmpisides_send      = [nmpisidesmine_proc nmpisidesyour_proc]
    nmpisides_rec       = [nmpisidesyour_proc nmpisidesmine_proc]
    offsetmpisides_send = [offsetmpisidesmine offsetmpisidesyour]
    offsetmpisides_rec  = [offsetmpisidesyour offsetmpisidesmine]

    MPIConnectivity(nbproc, mpielemoffset, nmpisides_send, nmpisides_rec, offsetmpisides_send, offsetmpisides_rec)
end

"""
Optimize Mortars: Search for big Mortars which only have small virtual MPI_MINE sides. Since the MPI_MINE-sides evaluate
the flux, the flux of the big Mortar side (computed from the 2/4 fluxes of the small virtual sides) can be computed BEFORE
the communication of the fluxes. Therefore those big Mortars can be moved from MPIMortars to the InnerMortars.
Order of sides (see mesh.f90):
   BCSides
   InnerMortars
   InnerSides
   MPI_MINE sides
   MPI_YOUR sides
   MPIMortars
"""
function optimizemortars(elems, n, offsetmpisidesmine, offsetmpisidesyour)
    # reset "tmp"-marker of all sides to 0
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      side.tmp = 0
    end

    # set "tmp"-marker of the big Mortar sides to:
    #  -2 : if a small virtual side is MPI_YOUR (can not be moved)
    #  -1 : otherwise (can be moved to inner Mortars)
    # Therewith only the big Mortar sides have a "tmp"-marker different from 0.
    # Additionally count all big Mortar sides that can be moved to inner Mortars (with tmp == -1).
    # ATTENTION: big Mortars, which are already inner Mortars are also marked and counted.
    addtoinnermortars=0
    for elem in elems, locside in elem.side
      if length(locside.mortars) > 0
        locside.tmp = -1 # mortar side
        for mortar in locside.mortars
          if mortar.sideid > offsetmpisidesyour[1]
            locside.tmp = -2  # mortar side with side used in MPI_YOUR
            break
          end
        end
        if locside.tmp == -1
          addtoinnermortars += 1
        end
      end
    end
    addtoinnermortars -= n.mortarinnersides # inner big Mortars are counted as well, subtract them.
    # Shift all InnerSides, MPI_MINE and MPI_YOUR sides rearwards by the number of big Mortars that will be moved.
    # Therewith the space/gap in the InnerMortars for the big Mortars, that will be moved, is created.
    if addtoinnermortars > 0
      lastmortarinnerside = n.bcsides + n.mortarinnersides # SideID of the last InnerMortar (before the move)
      # Iterate over all elements and within each element over all sides (6 for hexas)
      for elem in elems, locside in elem.side, side in [locside; locside.mortars]
        # if side is not a big Mortar side and the SideID is greater than off the last inner Mortar,
        # then shift SidID by number of big Mortars that will be moved and mark side as shifted (tmp=1)
        if side.tmp == 0 && side.sideid > lastmortarinnerside
          side.sideid += addtoinnermortars
          side.tmp = 1
        end
      end
      # increase the offsets
      offsetmpisidesmine .+= addtoinnermortars
      offsetmpisidesyour .+= addtoinnermortars

      n.mortarinnersides = n.mortarinnersides + addtoinnermortars  # increase number of inner Mortars
      n.mortarmpisides   = n.mortarmpisides   - addtoinnermortars  # decrease number of MPI Mortars
      imortarmpiside     = n.sides - n.mortarmpisides              # first index of the remaining MPI Mortars
      imortarinnerside   = n.bcsides                               # first index of the new inner Mortars

      # Until now only the InnerSides, MPI_MINE and MPI_YOUR are moved. Since the BCSides come before the
      # InnerMortars they stay untouched. It only remains to adjust the SideIDs of the InnerMortars and the MPIMortars.
      # They are totally renumbered.
      for elem in elems, side in elem.side
        if side.tmp == -2 # MPI mortars, renumber SideIDs
          imortarmpiside += 1
          side.sideid = imortarmpiside
        elseif side.tmp == -1 # innermortars mortars, renumber SideIDs
          imortarinnerside += 1
          side.sideid = imortarinnerside
        end # side.tmp==-1
      end
    end
end


"""
This routine communicates the flip between MPI sides, as the flip determines wheter
a side is a master or a slave side. The flip of MINE sides is set to zero, therefore
send MINE flip to other processor, so YOUR sides get their corresponding flip>0.
"""
function exchangeflip(conn, elems, n)
    if nmpiprocs == 1
        return
    end
    offsets = conn.offsetmpisides_send
    nnbprocs = length(conn.nbproc)

    flip_mine, flip_your = zeros(Int, n.sides), zeros(Int, n.sides)
    # fill MINE flip info
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      if offsets[1,SENDMINE] < side.sideid ≤ offsets[nnbprocs+1,SENDMINE]
        flip_mine[side.sideid] = side.flip
      end
    end

    sreq = fill(MPI.REQUEST_NULL, nnbprocs)
    rreq = fill(MPI.REQUEST_NULL, nnbprocs)
    for i = 1:nnbprocs
      # Start send flip from MINE
      if conn.nmpisides_send[i, SENDMINE] > 0
        srange  = offsets[i, SENDMINE]+1 : offsets[i+1, SENDMINE]
        sreq[i] = MPI.Isend( flip_mine[srange], conn.nbproc[i], 0, MPI.COMM_WORLD)
      end
      # Start receive flip to YOUR
      if conn.nmpisides_send[i, SENDYOUR] > 0
        rrange  = offsets[i, SENDYOUR]+1 : offsets[i+1, SENDYOUR]
        rreq[i] = MPI.Irecv!(view(flip_your,rrange), conn.nbproc[i], 0, MPI.COMM_WORLD)
      end
    end
    stats = MPI.Waitall!([sreq; rreq])

    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      if side.nbproc == -1
          continue #no MPISide
      end
      if side.sideid > offsets[1, SENDYOUR]
        if side.flip == 1
          if flip_your[side.sideid] == 1
            terminate("Problem in exchangeflip")
          end
          side.flip = flip_your[side.sideid]
        end
      else
        side.flip = 1 #MINE MPISides
      end
    end
end

"""
This routine condenses the mesh topology from a pointer-based structure into arrays.
The array ElemToSide contains for each elements local side the global SideID and its
flip with regard to the neighbour side.
The SideToElem array contains for each side the neighbour elements (master and slave)
as well as the local side IDs of the side within those elements.
The last entry is the flip of the slave with regard to the master element.
"""
function Connectivity(elems, n)

    elemtoside = fill(-1, 2, 6, n.elems)
    sidetoelem = fill(-1, 5,    n.sides)
    sideprops  = fill(-1, 3,    n.sides)
    mortarinfo = fill(-1, 2, 4, n.sides)
    nsides_mortartype = zeros(Int, 3)

    # Element to Side mapping
    nsides_flip = zeros(Int,5)
    for ielem = 1:n.elems
      elem = elems[ielem]
      for locside in locsides
        side = elem.side[locside]
        elemtoside[ [E2S_SIDE, E2S_FLIP], locside, ielem] = [side.sideid, side.flip]
   #     @assert flip in 0:4
        nsides_flip[side.flip] += 1
      end
    end

    # Side to Element mapping, sorted by SideID
    for ielem = 1:n.elems
      elem = elems[ielem]
      for locside in locsides
        side = elem.side[locside]
        if side.flip == 1 # root side/element
          sideprops[SP_ANALYZE, side.sideid] = side.bcindex
          sidetoelem[ [S2E_ELEM, S2E_LOCSIDE],               side.sideid] = [ielem, locside]
        else # element with flipped side
          sidetoelem[ [S2E_NBELEM, S2E_NBLOCSIDE, S2E_FLIP], side.sideid] = [ielem, locside, side.flip]
        end
        sideprops[SP_BC, side.sideid] = side.bcindex
      end
    end

    # Mapping of Mortar Master Side to Mortar Slave Side
    # Set type of mortar:
    #    -1: Small side belonging to big mortar
    #     0: No mortar
    # 1,2,3: Type of big mortar
    for elem in elems, side in elem.side
      sideprops[SP_MORTAR, side.sideid] = side.mortartype
      for imortar=1:length(side.mortars)
        mortar = side.mortars[imortar]
        mortarinfo[ [MI_SIDE, MI_FLIP] ,imortar, side.sideid] = [mortar.sideid, mortar.flip]
      end # iMortar
      if side.mortartype in 1:3
        nsides_mortartype[side.mortartype] += 1
      end
    end

    buf  = [nsides_flip; nsides_mortartype]
    buf2 = USEMPI ?  MPI.Reduce(buf, MPI.SUM, 0, MPI.COMM_WORLD) : buf
    if MPIROOT
        nsides_flip, nsides_mortartype = buf2[1:5], buf2[6:8]
        print(" | "); fillpost(30,"nSides with Flip 1...5");     print(" | "); for i=1:5 fillpost(9,nsides_flip[i])       end; print("\n")
        print(" | "); fillpost(30,"nSides of MortarType 1...3"); print(" | "); for i=1:3 fillpost(9,nsides_mortartype[i]) end; print("\n")
    end

    Connectivity(sidetoelem, elemtoside, sideprops, mortarinfo)
end
