meshfile = ""
ngeo = 1
const locsides = DIM == 3 ? (1:6) : (2:5)

"Volume metrics"
struct VolMetrics{ T <: AbstractFloat }
    xgp  ::Array{T}
    f    ::Array{T}
    g    ::Array{T}
    h    ::Array{T}
    sjac ::Array{T}
end
function VolMetrics(T::DataType, n::I, nelems::I) where I <: Integer
    @d3 sjac         =  zeros(T, 1, n, n, n, nelems)
    @d2 sjac         =  zeros(T, 1,    n, n, nelems)
    @d3 xgp, f, g, h = [zeros(T, 3, n, n, n, nelems) for i = 1:4]
    @d2 xgp, f, g    = [zeros(T, 2,    n, n, nelems) for i = 1:3]
    @d2 h            = Array{T}()
    VolMetrics(xgp, f, g, h, sjac)
end

"Surface metrics"
struct SurfMetrics{ T <: AbstractFloat }
    xgp     ::Array{T}
    normvec ::Array{T}
    tangvec1::Array{T}
    tangvec2::Array{T}
    surfelem::Array{T}
    ja      ::Array{T}
end
function SurfMetrics(T::DataType, n::I, nsides::I) where I <: Integer
    @d3 surfelem                         =  zeros(T, 1, n, n,    nsides)
    @d2 surfelem                         =  zeros(T, 1,    n,    nsides)
    @d3 xgp, normvec, tangvec1, tangvec2 = [zeros(T, 3, n, n,    nsides) for i = 1:4]
    @d2 xgp, normvec, tangvec1           = [zeros(T, 2,    n,    nsides) for i = 1:3]
    @d3 ja                               =  zeros(T, 3, 3, n, n, nsides)
    @d2 ja                               =  zeros(T, 2, 2, n,    nsides)
    @d2 tangvec2                         = Array{T}()
    SurfMetrics(xgp, normvec, tangvec1, tangvec2, surfelem, ja)
end

"Counters for various mesh types"
mutable struct Counters{ I <: Integer }
    elems::I
    sides::I
    nodes::I
    bcsides::I
    innersides::I
    mpisides::I
    mpisidesmine::I
    mpisidesyour::I
    periodicsides::I
    mpiperiodics::I
    mortarsides::I
    mortarinnersides::I
    mortarmpisides::I
    analyzesides::I
end

"Store the connectivity mappings"
struct Connectivity{ I <: Integer }
    sidetoelem::Array{I,2}
    elemtoside::Array{I,3}
    sideprops ::Array{I,2}
    mortarinfo::Array{I,3}
end

mutable struct MPIConnectivity{ I <: Integer }
    nbproc             ::Array{I,1}
    mpielemoffset      ::Array{I,1}
    nmpisides_send     ::Array{I,2}
    nmpisides_rec      ::Array{I,2}
    offsetmpisides_send::Array{I,2}
    offsetmpisides_rec ::Array{I,2}
end

struct BC{ I <: Integer }
    name::String
    bctype::I
    curveind::I
    state::I
    alpha::I
end

struct Tree{ T <: AbstractFloat }
  ntrees::IT
  x::Array{T,5}
  ximinmax::Array{T,3}
  elemtotree::Array{IT,1}
end

abstract type Meshtype end

struct CartMesh{ T <: AbstractFloat, I <: Integer } <: Meshtype
    vol::VolMetrics{T}
    surf::SurfMetrics{T}
    conn::Connectivity{I}
    mappings::Mappings
    x::Array{T}
    bcs::Array{BC{I},1}
    mpiconn::MPIConnectivity{I}
    info::Counters
end

struct H5Mesh{ T <: AbstractFloat, I <: Integer } <: Meshtype
    vol::VolMetrics{T}
    surf::SurfMetrics{T}
    conn::Connectivity{I}
    mappings::Mappings
    x::Array{T}
    tree::Tree{T}
    bcs::Array{BC{I},1}
    mpiconn::MPIConnectivity{I}
    info::Counters
end


rn(a::VolMetrics{T}, i)  where T = VolMetrics{T}(
      rn(a.xgp, i ), rn(a.f, i), rn(a.g, i), rn(a.h, i), rn(a.sjac, i))
rn(a::SurfMetrics{T}, i) where T = SurfMetrics{T}(
      rn(a.xgp, i), rn(a.normvec, i), rn(a.tangvec1, i), rn(a.tangvec2, i), rn(a.surfelem, i), rn(a.ja, i))
rn(a::SurfMetrics{T}, ::Val{0}) where T = SurfMetrics{T}( # rd is special due to ja
      rd(a.xgp), rd(a.normvec), rd(a.tangvec1), rd(a.tangvec2), rd(a.surfelem), reshape(a.ja, size(a.ja)[1:2]..., :, size(a.ja)[end]))
rn(a::CartMesh, i) = CartMesh(
      rn(a.vol, i ), rn(a.surf, i), a.conn, a.mappings, rn(a.x, i), a.bcs, a.mpiconn, a.info)
rn(a::H5Mesh, i)   = H5Mesh(
      rn(a.vol, i ), rn(a.surf, i), a.conn, a.mappings, rn(a.x, i), a.tree, a.bcs, a.mpiconn, a.info)
