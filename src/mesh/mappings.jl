#"""
# Functions to build mappings for easier handling of 3D data-structures and their connectivity
#"""

export Mappings, flipside!

"""
Routine which prebuilds mappings for a specific polynomial degree and allocates and stores them in given mapping arrays.
- V2S  : VolumeToSide mapping
- S2V  : SideToVolume mapping
- S2V2 : SideToVolume2 mapping
- FS2M : FlipSlaveToMaster mapping
"""
struct Mappings{ T <: Integer }
    v2s::Array{T}
    s2v::Array{T}
    s2v2::Array{T}   # side->side: side reference frame to volume reference frame
    v2s2::Array{T}   # side->side: volume reference frame to side reference frame
    fs2m::Array{T}   # convert between master <-> slave, fs2m==fm2s
    v2srange::Tuple
end
function Mappings(T, n, dim = 3)

    nz, flips, sides = dim == 3 ? (n, 1:5, 1:6)  :  (1, 1:2, 2:5)
    nflips, nsides = length(flips), length(sides)

    # VolToSide / SideToVol
    v2s = zeros(T, 3, n, n, nz, nflips, nsides)
    s2v = zeros(T, 3, n, n, nz, nflips, nsides)
    for f in flips, s in sides
        for k = 1:nz, j = 1:n, i = 1:n
            v2s[:,i,j,k,f,s] = voltoside(n + 1, i, j, k, f, s, Val(dim))
        end
        for q = 1:nz, p = 1:n, l = 1:n
            s2v[:,l,p,q,f,s] = sidetovol(n + 1, l, p, q, f, s, Val(dim))
        end
    end

    # SideToVol2
    s2v2 = zeros(T, 2, n, nz, nflips, nsides)
    v2s2 = zeros(T, 2, n, nz, nflips, nsides)
    for f in flips, s in sides
        for j = 1:nz, i = 1:n
            s2v2[:,i,j,f,s] = a = sidetovol2(n + 1, i, j, f, s, Val(dim))
            v2s2[:,a[1],a[2],f,s] = [i,j]
        end
    end

    # flip_s2m
    fs2m = zeros(T, 2, n, nz, nflips)
    for j = 1:nz, i = 1:n
        for f in flips
            fs2m[:,i,j,f] = flip_s2m(n + 1, i, j, f, Val(dim))
        end
    end

    # perform sanity checks
    for f in flips, s in sides
        for q = 1:nz, p = 1:n
            i, j, k = s2v[:, 1, p, q, f, s]
            ps, qs  = v2s[:, i, j, k, f, s]
            if ps ≠ p || qs ≠ q
                safeerror("sidetovol fores not fit to voltoside")
            end
        end
    end

    for f in flips, s in sides
        for k = 1:nz, j = 1:n, i = 1:n
            p, q = v2s[ :, i, j, k, f, s ]
            ic, jc = s2v2[ :, p ,q ,f ,s ]
            correct =
            (s in [XI_MINUS,XI_PLUS]     && (ic ≠ j || jc ≠ k))  ? false :
            (s in [ETA_MINUS,ETA_PLUS]   && (ic ≠ i || jc ≠ k))  ? false :
            (s in [ZETA_MINUS,ZETA_PLUS] && (ic ≠ i || jc ≠ j))  ? false :
            true
            if !correct
                safeerror("sidetovol2 fores not fit to voltoside")
            end
        end
    end

    Mappings(v2s, s2v, s2v2, v2s2, fs2m, cgns_voltosiderange(n))
end

"""
Transforms Coordinates from RHS of Slave to RHS of Master
   input: p,q in Slave-RHS, flip;
  output: indices in Master-RHS
"""
function flip_s2m(n, p, q, flip, ::Val{3})
  s2m = flip == 1 ? [   p ,   q ] :
        flip == 2 ? [   q ,   p ] :
        flip == 3 ? [ n-p ,   q ] :
        flip == 4 ? [ n-q , n-p ] :
        flip == 5 ? [   p , n-q ] :
        nothing
end
function flip_s2m(n, p, q, flip, ::Val{2})
  s2m = flip == 1 ? [   p , 0 ] :
        flip == 2 ? [ n-p , 0 ] :
        nothing
end
"""
Transforms Coordinates from RHS of Master to RHS of Slave, this is actualy the same function as Flip_S2M
"""
flip_m2s(n, p, q, flip, dim) = flip_s2m(n, p, q, flip, dim)

"""
 Transforms Volume-Coordinates into RHS of the Side (uses CGNS-Notation for side orientation)
 input: i,j,k, locSideID
   where: i,j,k = volume-indices
 output: indices in Master-RHS  +  volume-index which is not used (depending on local side)
"""
function cgns_voltoside(n, i, j, k, side, ::Val{3})
  cgns_voltoside =
      side == XI_MINUS   ? [   k , j ,   i ] :
      side == XI_PLUS    ? [   j , k , n-i ] :
      side == ETA_MINUS  ? [   i , k ,   j ] :
      side == ETA_PLUS   ? [ n-i , k , n-j ] :
      side == ZETA_MINUS ? [   j , i ,   k ] :
      side == ZETA_PLUS  ? [   i , j , n-k ] :
      nothing
end
function cgns_voltoside(n, i, j, k, side, ::Val{2})
  cgns_voltoside =
      side == XI_MINUS   ? [ n-j ,   i , 0 ] :
      side == XI_PLUS    ? [   j , n-i , 0 ] :
      side == ETA_MINUS  ? [   i ,   j , 0 ] :
      side == ETA_PLUS   ? [ n-i , n-j , 0 ] :
      nothing
end

cgns_voltosiderange(n) = (
                            (:, :, 1), # ZETA_MINUS
                            (:, 1, :), #  ETA_MINUS
                            (n, :, :), #   XI_PLUS
                            (:, n, :), #  ETA_PLUS
                            (1, :, :), #   XI_MINUS
                            (:, :, n)  # ZETA_PLUS
                         )

"""
 Transforms RHS-Coordinates of Side (CGNS-Notation for side orientation) into Volume-Coordinates
 input: l, p,q, locSideID
   where: p,q are in Master-RHS;
          l is the xi-,eta- or zeta-index in 0:n corresponding to local side
 output: volume-indices
"""
function cgns_sidetovol(n, l, p, q, side, ::Val{3})
  cgns_sidetovol =
      side == XI_MINUS   ? [   l ,   q ,   p ] :
      side == XI_PLUS    ? [ n-l ,   p ,   q ] :
      side == ETA_MINUS  ? [   p ,   l ,   q ] :
      side == ETA_PLUS   ? [ n-p , n-l ,   q ] :
      side == ZETA_MINUS ? [   q ,   p ,   l ] :
      side == ZETA_PLUS  ? [   p ,   q , n-l ] :
      nothing
end
function cgns_sidetovol(n, l, p, q, side, ::Val{2})
  cgns_sidetovol =
      side == XI_MINUS   ? [   l  , n-p  , 0 ] :
      side == XI_PLUS    ? [ n-l  ,   p  , 0 ] :
      side == ETA_MINUS  ? [   p  ,   l  , 0 ] :
      side == ETA_PLUS   ? [ n-p  , n-l  , 0 ] :
      nothing
end

"""
Transforms RHS-Coordinates of side (CGNS-notation for side orientation)
into side-local tensor product volume-coordinates
input: p,q, locSideID
  where: p,q are in Master-RHS;
output: Surface coordinates in volume frame
"""
function cgns_sidetovol2(n, p, q, side, ::Val{3})
  cgns_sidetovol2 =
      side == XI_MINUS   ? [   q , p ] :
      side == XI_PLUS    ? [   p , q ] :
      side == ETA_MINUS  ? [   p , q ] :
      side == ETA_PLUS   ? [ n-p , q ] :
      side == ZETA_MINUS ? [   q , p ] :
      side == ZETA_PLUS  ? [   p , q ] :
      nothing
end
function cgns_sidetovol2(n, p, q, side, ::Val{2})
  cgns_sidetovol2 =
      side == XI_MINUS   ? [ n-p , 0 ] :
      side == XI_PLUS    ? [   p , 0 ] :
      side == ETA_MINUS  ? [   p , 0 ] :
      side == ETA_PLUS   ? [ n-p , 0 ] :
      nothing
end

"""
 Transform Volume-Coordinates to RHS-Coordinates of Master. This is: VolToSide = Flip_S2M(CGNS_VolToSide(...))
 input: i,j,k, flip, locSideID
   where: i,j,k = volume-indices
 output: indices in Master-RHS
"""
function voltoside(n, i, j, k, flip, side, dim)
    p, q, r = cgns_voltoside(n, i, j, k, side, dim)
    [ flip_s2m(n, p, q, flip, dim)..., r ]
end

"""
 Transform RHS-Coordinates of Master to Volume-Coordinates. This is: SideToVol = CGNS_SideToVol(Flip_M2S(...))
 input: l, p, q, flip, local side, where: p,q are in Master-RHS;
                                   l is the xi-,eta- or zeta-index in 1:n corresponding to local side
 output: volume-indices
"""
function sidetovol(n, l, p, q, flip, side, dim)
    ps, qs = flip_m2s(n, p, q, flip, dim)
    cgns_sidetovol(n, l, ps, qs, side, dim)
end

"""
 Transform RHS-Coordinates of Master to Volume-Coordinates. This is: SideToVol2 = CGNS_SideToVol2(Flip_M2S(...))
 input:  p,q, flip, local side, where: p,q are in Master-RHS;
 output: volume-indicies (two indices on the specific local side)
"""
function sidetovol2(n, p, q, flip, side, dim)
    ps, qs = flip_m2s(n, p, q, flip, dim)
    a = cgns_sidetovol2(n, ps, qs, side, dim)
end


"""
Apply flip to local coordinate system
Two variants: - flip applied to input dataset
              - inverse flip applied to output dataset
The first version seems to be slightly faster.
"""
function flipside!(fout, sout::I, fin, sin::I, map, f::I, s::I, ::Val{V}, ::Val{N}) where {I<:Integer,V,N}
    @inbounds for q = 1:N, p = 1:N, v = 1:V
        fout[v,p,q,sout] = fin[v,map[1,p,q,f,s],map[2,p,q,f,s],sin]
    end
end
function flipside!(fout, sout::I, fin, sin::I, map, f::I,       ::Val{V}, ::Val{N}) where {I<:Integer,V,N}
    @inbounds for q = 1:N, p = 1:N, v = 1:V
        fout[v,p,q,sout] = fin[v,map[1,p,q,f],map[2,p,q,f],sin]
    end
end
function flipside!(fout, sout::I, fin, sin::I, map, f::I, s::I, ::Val{V}, ::Val{N}, scale) where {I<:Integer,V,N}
    @inbounds for q = 1:N, p = 1:N; @simd for v = 1:V
        fout[v,p,q,sout] = fin[v,map[1,p,q,f,s],map[2,p,q,f,s],sin]*scale
    end; end
end
function flipside!(fout, sout::I, fin, sin::I, map, f::I,       ::Val{V}, ::Val{N}, scale) where {I<:Integer,V,N}
    @inbounds for q = 1:N, p = 1:N; @simd for v = 1:V
        fout[v,p,q,sout] = fin[v,map[1,p,q,f],map[2,p,q,f],sin]*scale
    end; end
end
flipside!(fout::AbstractArray, sout::I, fin::AbstractArray,         map::AbstractArray, args...) where I<:Integer = flipside!(fout          , sout, add_dimr(fin), 1  , map, args...)
flipside!(fout::AbstractArray,          fin::AbstractArray, sin::I, map::AbstractArray, args...) where I<:Integer = flipside!(add_dimr(fout), 1   , fin          , sin, map, args...)
flipside!(fout::AbstractArray,          fin::AbstractArray,         map::AbstractArray, args...)                  = flipside!(add_dimr(fout), 1   , add_dimr(fin), 1  , map, args...)
