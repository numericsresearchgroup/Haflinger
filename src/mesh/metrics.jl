export calcvolmetrics, calcsurfmetrics

const interpolatefromtree = false
const crossproductmetrics = false
rf(a) = reshape(a, :, size(a)[3:end]...)    # squash the first two dims
rl(a) = reshape(a, size(a)[1:end-DIM]...,:) # squash the last 2/3 dims
#"""
#Fast determinants of 2x2 and 3x3 matrices.
#"""
@inline @inbounds det22(x::A) where A <: AbstractArray{T,2} where T =
    x[2,2]*x[1,1] - x[2,1]*x[1,2]
@inline @inbounds det33(x::A) where A <: AbstractArray{T,2} where T = (
    x[1,1]*(x[2,2]*x[3,3] - x[3,2]*x[2,3]) +
    x[2,1]*(x[3,2]*x[1,3] - x[1,2]*x[3,3]) +
    x[3,1]*(x[1,2]*x[2,3] - x[2,2]*x[1,3]) )


struct MeshBasis{ T <: AbstractFloat }
    xicln   ::Vector{T}; wgpcln   ::Vector{T}; wbarycln   ::Vector{T}
    xiclngeo::Vector{T}; wgpclngeo::Vector{T}; wbaryclngeo::Vector{T}
    xiref   ::Vector{T}; wgpref   ::Vector{T}; wbaryref   ::Vector{T}
    # Polynomial derivativion matrices
    d_cln              ::Array{T,2}
    d_clngeo           ::Array{T,2}
    # The Vandermonde interpolation matrices:
    vdm_eqngeo_clngeo  ::Array{T,2}
    vdm_clngeo_ngeoref ::Array{T,2}
    vdm_ngeoref_n      ::Array{T,2}
    vdm_clngeo_cln     ::Array{T,2}
    vdm_cln_n          ::Array{T,2}
    # volume temp arrays
     xcl_ngeo          ::Array{T,4}
    dxcl_ngeo          ::Array{T,5}
     xcl_n             ::Array{T,4}
    dxcl_n             ::Array{T,5}
    jacl_n             ::Array{T,5}
     rcl_n             ::Array{T,5}
    dx_ngeoref         ::Array{T,5}
    detjac_ref         ::Array{T,4}
    # surface temp arrays
    tmp                ::Array{T,3}
    surf               ::SurfMetrics{T}
    mb                 ::MortarBasis
end
function MeshBasis(T,n,ngeo,nodetype)
    ngeoref = 3*ngeo-1
    # Nodes and weights
    xicln,    wgpcln,    wbarycln    = nodesandweights(BigFloat, n,       :nodetypecl)
    xiclngeo, wgpclngeo, wbaryclngeo = nodesandweights(BigFloat, ngeo,    :nodetypecl)
    xiref,    wgpref,    wbaryref    = nodesandweights(BigFloat, ngeoref, nodetype)
    # Polynomial derivativion matrices
    d_cln              = polynomialderivativematrix(xicln)
    d_clngeo           = polynomialderivativematrix(xiclngeo)
    # The Vandermonde interpolation matrices:
    # only use modal Vandermonde for terms that need to be conserved as Jacobian if N_out>N_in.
    # Always use interpolation for the rest
    vdm_eqngeo_clngeo  = getvandermonde(ngeo   , :nodetypeequi , ngeo    , :nodetypecl , projection=false) # coords
    vdm_clngeo_ngeoref = getvandermonde(ngeo   , :nodetypecl   , ngeoref , nodetype    , projection=false) # jacobian
    vdm_ngeoref_n      = getvandermonde(ngeoref, nodetype      , n       , nodetype    , projection=true)  # jacobian
    vdm_clngeo_cln     = getvandermonde(ngeo   , :nodetypecl   , n       , :nodetypecl , projection=false) # dcl
    vdm_cln_n          = getvandermonde(n      , :nodetypecl   , n       , nodetype    , projection=false) # actual gp
    # Temp arrayss
     xcl_ngeo  = zeros(T, 3,    ngeo, ngeo, ngeo )          # mapping X(ξ) P ∈ ngeo
    dxcl_ngeo  = zeros(T, 3, 3, ngeo, ngeo, ngeo )          # Jacobi matrix on CL ngeo
    dx_ngeoref = zeros(T, 3, 3, ngeoref, ngeoref, ngeoref ) # Jacobi matrix on SOL ngeoref
    detjac_ref = zeros(T, 1,    ngeoref, ngeoref, ngeoref )
     xcl_n     = zeros(T, 3,    n   , n   , n    )          # mapping X(ξ) P ∈ N
    dxcl_n     = zeros(T, 3, 3, n   , n   , n    )
    jacl_n, rcl_n = [copy(dxcl_n) for i=1:2]                # volume metric terms P ∈ N and buffer
    # Surface metrics for a single element
    tmp        = zeros(T, 3,    n   , n       )
    surf       = SurfMetrics(T, n, 6)

    m01,  m02  = mortarbasis_bigtosmall(n, nodetype)
    m01h, m02h = 0.5*m01, 0.5*m02
    # CAUTION: we store the m01h, m02h in m10, m20!!!
    mb = MortarBasis(SMatrix{n,n,FT}(m01), SMatrix{n,n,FT}(m02),
                     SMatrix{n,n,FT}(m01h),SMatrix{n,n,FT}(m02h))

    t = Array{T}
    MeshBasis(t(xicln), t(wgpcln), t(wbarycln),   t(xiclngeo), t(wgpclngeo), t(wbaryclngeo),  t(xiref), t(wgpref), t(wbaryref),
              t(d_cln), t(d_clngeo),
              vdm_eqngeo_clngeo, vdm_clngeo_ngeoref, vdm_ngeoref_n, vdm_clngeo_cln, vdm_cln_n,
              xcl_ngeo, dxcl_ngeo, xcl_n, dxcl_n, jacl_n, rcl_n, dx_ngeoref, detjac_ref,
              tmp, surf, mb)
end


"""
This module contains routines for computing the geometries volume and surface metric terms.

Compute the volume and surface metric terms:
    Metrics_fTilde(n=1:3,i,j,k,iElem)=Ja_n^1
    Metrics_gTilde(n=1:3,i,j,k,iElem)=Ja_n^2
    Metrics_hTilde(n=1:3,i,j,k,iElem)=Ja_n^3

  Per Element we do:
  1.) a.) Preparation: the geometry (equidistant nodal basis, ngeo+1 points/dir) is interpolated to a high precision
          mapping X_n(xi_i) using a Chebyshev-Lobatto basis and stored in XCL_ngeo(1:3,i,j,k,iElem) i,j,k=[0:ngeo]
      b.) Computing the gradients: compute the derivative of the mapping XCL_ngeo in \fS (xi_1,xi_2,xi_3) \fS direction,
          using a polynomial derivative Matrix at degree ngeo.
      c.) Computing the Jacobian: compute Jacobian JRef at a degree of ngeoRef=3*ngeo (exact).
                                  For this gradients have to be interpolated to ngeoRef first.
                                  Then project JRef down to degree N. Finally check for negative Jacobians.
      d.) For computing Ja the gradients at degree N are required: if N>=ngeo directly interpolate dXCL_ngeo to dXCL_N,
                                                                   else compute dXCL_N from XCL_N directly.

  2.) for each direction n
      a.) compute the nth vector and for each Chebyshev point [:,i,j,k)
         (dXCL_n^1,dXCL_n^2,dXCL_n^3)^T=(X_l grad_xi (X_m) ) for n= 1:2,3 and (n,m,l) cyclic
      b.) interpolate the dXCL_n vector defined primarily on (ngeo+1)x(ngeo+1)x(ngeo+1) Chebyshev-Lobatto points to
            (N+1)x(N+1)x(N+1) Chebyshev-Lobatto points and write to Ja_n(1:3,i,j,k) i,j,k=[0:N]
      c.) compute the curl of vector Ja_n(1:3,i,j,k) using the derivative Matrix DCL_N [NxN]
      d.) interpolate from (N+1)x(N+1)x(N+1) Chebyshev-Lobatto points to  Gauss-Points (N+1)x(N+1)x(N+1) (exact#)
      e.) store Ja_n in the Metrics arrays

  3.) Compute the surface metrics (normal/tangential vectors, surface area) from volume metrics for each side.

 Special case if non-conforming meshes with octree mappings are used. Then compute ALL volume quantities on tree (macro element)
 level and interpolate down to small actual elements. This will ensure watertight meshes and free-stream preservation.

This routine takes the equidistant node coordinats of the mesh (on ngeo points) and uses them to build the coordinates
of solution/interpolation points of type NodeType on polynomial degree n (N points per direction).
The coordinates (for a non-conforming mesh) can also be build from an octree if the mesh is based on a conforming baseline mesh.
"""
function calcvolmetrics(b::MeshBasis{T}, x::A, xgp::A, f::A, g::A, h::A, jac::A ) where
                        A <: AbstractArray{T,DIM+1} where T <: AbstractFloat

    nz     = n    = length(b.xicln)
    ngeoz  = ngeo = length(b.xiclngeo)
    # squash dofs to one dim for scalar operations
    jacl_n, rcl_n, xcl_n = rl(b.jacl_n), rl(b.rcl_n), rl(b.xcl_n)
    dxcl_n, dx_ngeoref   = rl(b.dxcl_n), rl(b.dx_ngeoref)
    detjac_ref           = reshape(b.detjac_ref, :)
    # squash first two dims for changebasis
    vdxcl_n, vdxcl_ngeo, vdx_ngeoref, vjacl_n = rf(b.dxcl_n), rf(b.dxcl_ngeo), rf(b.dx_ngeoref), rf(b.jacl_n)

    # 1.a) NodeCoords:==I ngeo to CLngeo and CLN
    # 1.b) dXCL_ngeo:
    # 1.c) Jacobian: CLngeo to ngeoRef, CLngeoRef to N
    # 1.d) derivatives (dXCL) by projection or by direct derivation (D_CL):
    # 2.d) derivatives (dXCL) by projection or by direct derivation (D_CL):

    #1.a) Transform from==I_ngeo to CL points on ngeo and N
    changebasisvolume(b.vdm_eqngeo_clngeo, x,          b.xcl_ngeo)
    changebasisvolume(b.vdm_clngeo_cln,    b.xcl_ngeo, b.xcl_n)
    changebasisvolume(b.vdm_cln_n,         b.xcl_n   , xgp)

    #1.b) Jacobi Matrix of d/dxi_dd(X_nn): dXCL_ngeo(dd,nn,i,j,k))
    fill!(b.dxcl_ngeo,0.)
    for k = 1:ngeoz, j = 1:ngeo, i = 1:ngeo
      for ll = 1:ngeo, d=1:DIM
        b.dxcl_ngeo[1,d,i,j,k] += b.d_clngeo[i,ll] * b.xcl_ngeo[d,ll,j,k]
        b.dxcl_ngeo[2,d,i,j,k] += b.d_clngeo[j,ll] * b.xcl_ngeo[d,i,ll,k]
    @d3 b.dxcl_ngeo[3,d,i,j,k] += b.d_clngeo[k,ll] * b.xcl_ngeo[d,i,j,ll]
      end
    end

    # 1.c)Jacobians# grad(X_1) (grad(X_2) x grad(X_3))
    # Compute Jacobian on ngeo and then interpolate:
    # required to guarantee conservativity when restarting with N<ngeo
    changebasisvolume(b.vdm_clngeo_ngeoref, vdxcl_ngeo, vdx_ngeoref)
    @views @inbounds for i = 1:length(detjac_ref)
        @d2 detjac_ref[i] = det22(dx_ngeoref[:,:,i])
        @d3 detjac_ref[i] = det33(dx_ngeoref[:,:,i])
    end

    # interpolate detJac_ref to the solution points
    changebasisvolume(b.vdm_ngeoref_n,b.detjac_ref,jac)

    #2.a) Jacobi Matrix of d/dxi_dd(X_nn): dXCL_N(dd,nn,i,j,k))
    # N>=ngeo: interpolate from dXCL_ngeo (default)
    # N< ngeo: directly derive XCL_N
    if n ≥ ngeo #compute first derivative on ngeo and then interpolate
      changebasisvolume(b.vdm_clngeo_cln, vdxcl_ngeo, vdxcl_n)
    else  #N<ngeo: first interpolate and then compute derivative (important if curved&periodic)
      fill(b.dxcl_n,0.)
      for k = 1:nz, j = 1:n, i = 1:n
        for ll = 1:n, d = 1:DIM
          b.dxcl_n[1,d,i,j,k] += b.d_cln[i,ll] * b.xcl_n[d,ll,j,k]
          b.dxcl_n[2,d,i,j,k] += b.d_cln[j,ll] * b.xcl_n[d,i,ll,k]
      @d3 b.dxcl_n[3,d,i,j,k] += b.d_cln[k,ll] * b.xcl_n[d,i,j,ll]
        end
      end
    end

    fill!(jacl_n,0.)
    @inbounds if  DIM == 2
      # No need to differentiate between curl and cross product metrics in 2D, we can directly use the calculated derivatives
      for i = 1:size(jacl_n,3)
        jacl_n[1,1,i] =  dxcl_n[2,2,i]
        jacl_n[2,1,i] = -dxcl_n[1,2,i]
        jacl_n[1,2,i] = -dxcl_n[2,1,i]
        jacl_n[2,2,i] =  dxcl_n[1,1,i]
      end
    else
      if crossproductmetrics
        # exact (cross-product) form
        @views for i = 1:size(jacl_n,3)
          # exact (cross-product) form
          # Ja(:)^nn = ( d/dxi_(nn+1) XCL_N(:) ) x (d/xi_(nn+2) XCL_N(:))
          # JaCL_N(dd,nn) = dXCL_N(dd+1,nn+1)*dXCL_N(dd+2,nn+2) -dXCL_N(dd+1,nn+2)*dXCL_N(dd+2,nn+1)
          jacl_n[:,1,i] = cross(dxcl_n[:,2,i], dxcl_n[:,3,i])
          jacl_n[:,2,i] = cross(dxcl_n[:,3,i], dxcl_n[:,1,i])
          jacl_n[:,3,i] = cross(dxcl_n[:,1,i], dxcl_n[:,2,i])
        end
      else # curl metrics
        # invariant curl form, as cross product: R^dd = 1/2( XCL_N(:) x (d/dxi_dd XCL_N(:)))
        #
        #R_CL_N(dd,nn)=1/2*( XCL_N(nn+2)* d/dxi_dd XCL_N(nn+1) - XCL_N(nn+1)* d/dxi_dd XCL_N(nn+2))
        for i = 1:size(rcl_n,3), d=1:3
          rcl_n[d,1,i] = (xcl_n[3,i] * dxcl_n[d,2,i] - xcl_n[2,i] * dxcl_n[d,3,i])/2
          rcl_n[d,2,i] = (xcl_n[1,i] * dxcl_n[d,3,i] - xcl_n[3,i] * dxcl_n[d,1,i])/2
          rcl_n[d,3,i] = (xcl_n[2,i] * dxcl_n[d,1,i] - xcl_n[1,i] * dxcl_n[d,2,i])/2
        end
        # Metrics are the curl of R:  Ja(:)^nn = -(curl R_CL(:,nn))
        # JaCL_N(dd,nn)= -[d/dxi_(dd+1) RCL(dd+2,nn) - d/dxi_(dd+2) RCL(dd+1,nn) ]
        #              =   d/dxi_(dd+2) RCL(dd+1,nn) - d/dxi_(dd+1) RCL(dd+2,nn)
        for k = 1:n, j = 1:n, i = 1:n
          for q = 1:n, d = 1:DIM
            b.jacl_n[1,d,i,j,k] += b.d_cln[k,q] * b.rcl_n[2,d,i,j,q] - b.d_cln[j,q] * b.rcl_n[3,d,i,q,k]
            b.jacl_n[2,d,i,j,k] += b.d_cln[i,q] * b.rcl_n[3,d,q,j,k] - b.d_cln[k,q] * b.rcl_n[1,d,i,j,q]
            b.jacl_n[3,d,i,j,k] += b.d_cln[j,q] * b.rcl_n[1,d,i,q,k] - b.d_cln[i,q] * b.rcl_n[2,d,q,j,k]
          end
        end
      end #crossProductMetrics
    end

    # interpolate Metrics from Cheb-Lobatto N onto GaussPoints N
    ja2 = r2(b.dxcl_n)
    changebasisvolume(b.vdm_cln_n,vjacl_n,vdxcl_n) # use dxcl_n as buffer
    @inbounds for i=1:length(f)
      f[i] = ja2[1,i]
      g[i] = ja2[2,i]
      h[i] = ja2[3,i]
    end
end

function calcmetricsfromtree()
        xi0   =ximinmax[:,1,ielem]
        length=ximinmax[:,2,ielem]-xi0
    @d2 length[3] = 1.

    # Jacobian
        #interpolate detJac to the GaussPoints
        for i = 1:ngeoref
          dxi=0.5*(xiref[i]+1.)*length
          lagrangeinterpolationpolys(xi0[1] + dxi[1],ngeoref,xiref,wbaryref,vdm_xi_ref[i,:])
          lagrangeinterpolationpolys(xi0[2] + dxi[2],ngeoref,xiref,wbaryref,vdm_eta_ref[i,:])
      @d3 lagrangeinterpolationpolys(xi0[3] + dxi[3],ngeoref,xiref,wbaryref,vdm_zeta_ref[i,:])
        end
        tmp=detjac_ref[:,:,:,:,ielem]
    @d3 changebasis3d_xyz(1,ngeoref,ngeoref,vdm_xi_ref,vdm_eta_ref,vdm_zeta_ref,
                               tmp,detjac_ref[:,:,:,:,ielem])
    @d2 changebasis2d_xyz(1,ngeoref,ngeoref,vdm_xi_ref,vdm_eta_ref,
                               tmp[:,:,:,1],detjac_ref[:,:,:,1,ielem])

        # interpolate Metrics from Cheb-Lobatto N on tree level onto GaussPoints N on quad level
        for i= 1:n
          dxi=0.5*(xGP[i]+1.)*length
          lagrangeinterpolationpolys(xi0[1] + dxi[1],n,xicl_n,wbarycl_n,vdm_xi_n[i,:])
          lagrangeinterpolationpolys(xi0[2] + dxi[2],n,xicl_n,wbarycl_n,vdm_eta_n[i,:])
      @d3 lagrangeinterpolationpolys(xi0[3] + dxi[3],n,xicl_n,wbarycl_n,vdm_zeta_n[i,:])
        end
        if DIM == 3
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[1,:,:,:,:],metrics_ftilde[:,:,:,:,ielem])
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[2,:,:,:,:],metrics_gtilde[:,:,:,:,ielem])
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[3,:,:,:,:],metrics_htilde[:,:,:,:,ielem])
        else
          changebasis2d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,jacl_n[1,:,:,:],metrics_ftilde[:,:,:,1,ielem])
          changebasis2d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,jacl_n[2,:,:,:],metrics_gtilde[:,:,:,1,ielem])
        end
        # for the metrics and the jacobian, we have to take into account the level #####
        metrics_ftilde[:,:,:,:,ielem] = (length[1]/2)^2 * metrics_ftilde[:,:,:,:,ielem]
        metrics_gtilde[:,:,:,:,ielem] = (length[2]/2)^2 * metrics_gtilde[:,:,:,:,ielem]
    @d3 metrics_htilde[:,:,:,:,ielem] = (length[3]/2)^2 * metrics_htilde[:,:,:,:,ielem]
        sj[:,:,:,ielem]=(2^DIM/product(length))*sj[:,:,:,ielem] # scale down sj

        # interpolate Metrics and grid to Cheb-Lobatto on quadrant level for Surface metrics
        for i = 1:n
          dxi = 0.5*(xicl_n[i]+1.)*length
          lagrangeinterpolationpolys(xi0[1] + dxi[1],n,xicl_n,wbarycl_n,vdm_xi_n[i,:])
          lagrangeinterpolationpolys(xi0[2] + dxi[2],n,xicl_n,wbarycl_n,vdm_eta_n[i,:])
      @d3 lagrangeinterpolationpolys(xi0[3] + dxi[3],n,xicl_n,wbarycl_n,vdm_zeta_n[i,:])
        end
        if DIM == 3
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,xcl_n            ,xcl_n_quad)
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[1,:,:,:,:],jacl_n_quad[1,:,:,:,:])
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[2,:,:,:,:],jacl_n_quad[2,:,:,:,:])
          changebasis3d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,vdm_zeta_n,jacl_n[3,:,:,:,:],jacl_n_quad[3,:,:,:,:])
        else
          changebasis2d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,xcl_n          ,xcl_n_quad)
          changebasis2d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,jacl_n[1,:,:,:],jacl_n_quad[1,:,:,:])
          changebasis2d_xyz(3,n,n,vdm_xi_n,vdm_eta_n,jacl_n[2,:,:,:],jacl_n_quad[2,:,:,:])
        end
        #TODO: scale Ja for anisotropic
        jacl_n_quad[:,1,:,:,:] = (length[2]*length[3]/(2^(DIM-1))) * jacl_n_quad[:,1,:,:,:]
        jacl_n_quad[:,2,:,:,:] = (length[1]*length[3]/(2^(DIM-1))) * jacl_n_quad[:,2,:,:,:]
    @d3 jacl_n_quad[:,3,:,:,:] = (length[1]*length[2]/(2^(DIM-1))) * jacl_n_quad[:,3,:,:,:]
        calcsurfmetrics(jacl_n_quad,xcl_n_quad,vdm_cln_n,ielem,
                        normvec,tangvec1,tangvec2,surfelem,face_xgp,ja_face)
end

const normaldir   = ( 3 , 2 , 1 , 2 , 1 , 3 ) # normal vector direction for element local side
@d3 const tangdir = ( 1 , 3 , 2 , 3 , 2 , 1 ) # first tangential vector direction for element local side
@d2 const tangdir = ( 1 , 1 , 2 , 1 , 2 , 1 ) # first tangential vector direction for element local side
const normalsign  = (-1 ,-1 , 1 , 1 ,-1 , 1 ) # normal vector sign for element local side

"""
Prepares computation of the faces' normal, tangential vectors, surface area and Gauss points from volume metrics.
Input are the Gauss point coordinates and the volume metrics f,g,h.
For each side the volume metrics are interpolated to the surface and rotated into the side reference frame.
"""
function calcsurfmetrics(b::MeshBasis{T}, vxgp::A, f::A, g::A, h::A) where A <: AbstractArray{T,DIM+1} where T
    s, sd = b.surf, rd(b.surf)
    d, n  = Val(DIM), Val(length(b.xicln))
    s2v2  = mesh.mappings.s2v2

    @views for side in locsides
        # evaluate volume metrics on faces and rotate to master system
        slice!(b.tmp, vxgp, d, n, side); flipside!(s.xgp          , side, b.tmp, s2v2, 1, side, d, n)
        slice!(b.tmp, f,    d, n, side); flipside!(s.ja[:,1,:,:,:], side, b.tmp, s2v2, 1, side, d, n)
        slice!(b.tmp, g,    d, n, side); flipside!(s.ja[:,2,:,:,:], side, b.tmp, s2v2, 1, side, d, n)
        slice!(b.tmp, h,    d, n, side); flipside!(s.ja[:,3,:,:,:], side, b.tmp, s2v2, 1, side, d, n)

        surfmetricsfromja(sd.ja[:,:,:,side], normaldir[side], tangdir[side], normalsign[side],
                          sd.normvec[:,:,side], sd.tangvec1[:,:,side], sd.tangvec2[:,:,side], sd.surfelem[:,:,side])
    end
end

"""
Computes surface normal and tangential vectors and surface area from surface metrics Ja_Face.
NormalDir  : direction of normal vector
TangDir    : direction of 1. tangential vector
NormalSign : sign of normal vector
"""
@inline function surfmetricsfromja(ja::AbstractArray{T,3}, normaldir::I, tangdir::I, normalsign::I,
                                   normvec::A, tangvec1::A, tangvec2::A, surfelem::A) where
                                   { I <: Integer, A <: AbstractArray{T,2} } where T

    @views for i = 1:size(surfelem)[2]
        surfelem[1,i]  = norm(ja[:,normaldir,i])
        normvec[ :,i]  = normalsign * ja[:,normaldir,i] / surfelem[1,i]
        # For two-dimensional computations, the normal direction will be 1 or 2. For the tangential direction
        # we then set 2 or 1 accordingly.
        tangvec1[:,i]  = ja[:,tangdir,i] - dot(ja[:,tangdir,i],normvec[:,i]) * normvec[:,i]
        tangvec1[:,i]  = tangvec1[:,i] / norm(tangvec1[:,i])
    @d3 tangvec2[:,i]  = cross(normvec[:,i],tangvec1[:,i])
    end
end

"""
Pack surface elements from an element local representation into the global surfmetrics arrays
"""
function fillsurfmetricarray(b::MeshBasis{T}, sg::SurfMetrics{T}, elem::Integer) where T
    e2s, sp, mi = mesh.conn.elemtoside, mesh.conn.sideprops, mesh.conn.mortarinfo
    s = b.surf
    # Copy data from local surf metrics s to global metrics sg
    @views for locside in locsides
        if e2s[E2S_FLIP, locside, elem] ≠ 1
            continue
        end
        side = e2s[E2S_SIDE, locside, elem]
        copyto!(sg.surfelem[:,:,:,side], s.surfelem[:,:,:,locside])
        copyto!(sg.normvec[ :,:,:,side], s.normvec[ :,:,:,locside])
        copyto!(sg.tangvec1[:,:,:,side], s.tangvec1[:,:,:,locside])
        copyto!(sg.tangvec2[:,:,:,side], s.tangvec2[:,:,:,locside])
        copyto!(sg.xgp[     :,:,:,side], s.xgp[     :,:,:,locside])
        copyto!(sg.ja[    :,:,:,:,side], s.ja[    :,:,:,:,locside])
    end
end

"""
Interpolates surface metrics Ja and xGP on face from master to slave (small) mortar sides.
Already existing surface metrics are overwritten, metrics for small sides are built from
big (master) side, i.e. all small sides belonging to a mortar interface are slave sides
(with inward pointing normal vector). NOTE THAT THIS IS NOT THE CASE FOR MPI_YOUR MORTAR SIDES!
In an MPI setting if the big sides are not present on a CPU and if this CPU has small master sides
they are not rebuilt and fluxes need to be rotated at the big mortar.
"""
function calcmortarsurfmetrics(b::MeshBasis{T}, glob::SurfMetrics{T}, elem::Integer) where T
    e2s, sp, mi = mesh.conn.elemtoside, mesh.conn.sideprops, mesh.conn.mortarinfo
    d, d2  = Val(DIM), Val(DIM^2)
    loc, locd = b.surf, rd(b.surf)
    jaloc,   jaglob = rn(loc.ja,  Val(-2)), rn(glob.ja, Val(-2))  # squash first 2 dims
    m1,  m2  = b.mb.m01, b.mb.m02
    m1h, m2h = b.mb.m10, b.mb.m20 # half mortars are stored in m10, m20

    @views for s in locsides
        side  = e2s[E2S_SIDE, s, elem]
        mtype = sp[SP_MORTAR, side]
        if !(e2s[E2S_FLIP, s, elem] == 1 && mtype in 1:3)
            continue
        end
        nmortars = mtype == 1 ? 4 : 2
        # big sides are already stored in glob, mortarize into loc
        mortarbigtosmall(jaglob[  :,:,:,side], jaloc[  :,:,:,1:nmortars], jaloc[  :,:,:,5:6], mtype, m1h, m2h, d2)
        mortarbigtosmall(glob.xgp[:,:,:,side], loc.xgp[:,:,:,1:nmortars], loc.xgp[:,:,:,5:6], mtype, m1,  m2,  d)
        # Calc metrics from ja and copy
        for m = 1:nmortars
            surfmetricsfromja(locd.ja[:,:,:,m], normaldir[s], tangdir[s], normalsign[s],
                              locd.normvec[:,:,m], locd.tangvec1[:,:,m], locd.tangvec2[:,:,m], locd.surfelem[:,:,m])
            mside, flip = mi[MI_SIDE, m, side], mi[MI_FLIP, m, side]
            copy!(glob.surfelem[:,:,:,mside], loc.surfelem[:,:,:,m])
            copy!(glob.normvec[ :,:,:,mside], loc.normvec[ :,:,:,m])
            copy!(glob.tangvec1[:,:,:,mside], loc.tangvec1[:,:,:,m])
            copy!(glob.tangvec2[:,:,:,mside], loc.tangvec2[:,:,:,m])
            copy!(glob.xgp[     :,:,:,mside], loc.xgp[     :,:,:,m])
            copy!(glob.ja[    :,:,:,:,mside], loc.ja[    :,:,:,:,m])
        end
    end
end


const jactolerance = 0.01
function checkjacobian(jac::A,xgp::A,ielem::Integer) where A <: AbstractArray
    # check for negative Jacobians
    minj = minimum(jac)
    if minj ≤ 0
        println("Negative Jacobian $minj found. Element $(ielem), $(mean(xgp,2:ndims(xgp)))")
        return -2
    else
        # check scaled Jacobians
        scaledjac = minj/maximum(jac)
        if scaledjac < jactolerance
            println("Scaled Jacobian $scaledjac lower then tolerance. Element $(ielem), $(mean(xgp,2:ndims(xgp)))")
            return -1
        end
    end
    return 1
end
