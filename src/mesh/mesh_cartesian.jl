function CartMesh(params, n)

    global ngeo = 2
    global meshfile   = "internal"
    global usecurveds = getparameter(params, "usecurveds", required=false, default=true)

    bcs = getparameter(params, "bcs", required = false )
    x0  = getparameter(params, "x0" , required = false, default = [-1.,-1.,-1.])
    x1  = getparameter(params, "x1" , required = false, default = [ 1., 1., 1.])
    nelemsxyz = getparameter(params, "nelemsxyz", required = false, default = [2,2,2]) # returns array [nx,ny,nz]
    userbcs   = getparameter(params, "bcs"      , required = false, default = Array{Any,1}() )
    #mprint(" | "); fillpost(30,"Name"); mprint(" | "); fillpre(10,"Type","State","Alpha"); mprint("\n")
    #for i = 1:nbcs
    #    mprint(" | "); fillpost(30,"$(bcnames[i])"); mprint(" | "); fillpre(10,bctypes[:,i]...); mprint("\n")
    #end
    # TODO: add BCs for cartmesh
    bcs = [BC("BC$i", 0, 0, 0, 0) for i = 1:6]

  # pack this stuff into the data structures

    global nglobalelems = prod(nelemsxyz)
    global nelems = nglobalelems
    global nsides = 3 * nelems
    global nbcsides = 0
    global nmortarinnersides = 0
    global ninnersides = nsides
    global nmpisidesmine = 0
    global nmpisidesyour = 0
    global nmpimortars = 0

    conn = Connectivity(nelemsxyz...)
    for i = 1:size(conn.elemtoside,3)
      println(conn.elemtoside[:,:,i])
    end
    for i = 1:size(conn.sidetoelem,2)
     println(conn.sidetoelem[:,i])
    end
    vol  = VolMetrics(FT, n, nelems)
    surf = SurfMetrics(FT, n, nsides)
    mappings = Mappings(MT,n)

    coords = nodecoordsunitcube(nelemsxyz...,x0,x1)

    initstart("SFC")
    sfctype = "Morton"
    mprintln("SORT ELEMENTS ON SPACE FILLING CURVE, TYPE $sfctype")
    id = [i for i=1:size(r2(coords),2)]
    sorted = sortbyspacefillingcurve(r2(coords), id, :cube)
    initdone("SFC")

    nsides         = 3*nelems
    nbcsides       = 0
    ninnersides    = nsides - nbcsides
    nmpisides      = 0
    nperiodicsides = nelemsxyz[1]*nelemsxyz[2] + nelemsxyz[1]*nelemsxyz[3] + nelemsxyz[2]*nelemsxyz[3] 
    nmpiperiodics  = 0
    nmortarsides   = 0
    nmortarmpisides = 0
    nanalyzesides  = 0
    tmp = [nelems, nsides, nelems*ngeo^3, nbcsides, ninnersides, nmpisides, 0, 0, nperiodicsides,
           nmpiperiodics, nmortarsides, nmortarinnersides, nmortarmpisides, nanalyzesides ]

    junk0, junk1 = zeros(Int,1,1), [nsides nsides]
    mpiconn = MPIConnectivity(zeros(Int,1), [0, nelems], junk0, junk0, junk1, junk1)

    CartMesh(vol, surf, conn, mappings, coords, bcs, mpiconn, Counters(tmp...))
end

function Connectivity(nx::IT, ny::IT, nz::IT)

    nelems = nx * ny * nz
    nsides = 3 * nelems

    elemtosidecart = zeros(IT, 2, 6, nx, ny, nz)
    elemtoside     = zeros(IT, 2, 6, nelems)
    sidetoelem     = zeros(IT, 5, nelems * 3)
    sideprops  = zeros(Int, 3,    3*nelems)
    mortarinfo = zeros(Int, 2, 4, 3*nelems)

    ielem = 1
    iside = 0
    for k = 1:nz, j = 1:ny,  i = 1:nx
        elemtosidecart[E2S_SIDE,ZETA_MINUS,i,j,k] = iside+1 # ζ-
        elemtosidecart[E2S_SIDE,ETA_MINUS,i,j,k]  = iside+2 # η-
        elemtosidecart[E2S_SIDE,XI_PLUS,i,j,k]    = iside+3 # ξ+

        elemtosidecart[E2S_FLIP,ZETA_MINUS,i,j,k] = 1
        elemtosidecart[E2S_FLIP,ETA_MINUS,i,j,k]  = 1
        elemtosidecart[E2S_FLIP,XI_PLUS,i,j,k]    = 1
        elemtosidecart[E2S_FLIP,ETA_PLUS,i,j,k]   = 3
        elemtosidecart[E2S_FLIP,XI_MINUS,i,j,k]   = 2
        elemtosidecart[E2S_FLIP,ZETA_PLUS,i,j,k]  = 2
        iside += 3
    end
    for k = 1:nz, j = 1:ny,  i = 1:nx
        im = i==1  ? nx : i-1 
        jp = j==ny ? 1  : j+1 
        kp = k==nz ? 1  : k+1 
        elemtosidecart[E2S_SIDE,ZETA_PLUS,i,j,k] = elemtosidecart[E2S_SIDE,ZETA_MINUS,i,j,kp]  # ζ+
        elemtosidecart[E2S_SIDE,ETA_PLUS ,i,j,k] = elemtosidecart[E2S_SIDE,ETA_MINUS, i,jp,k]  # η+
        elemtosidecart[E2S_SIDE,XI_MINUS ,i,j,k] = elemtosidecart[E2S_SIDE,XI_PLUS,   im,j,k]  # ξ-
        ielem = i + (j - 1) * (ny) + (k - 1) * nz * ny
        elemtoside[:,:,ielem] = elemtosidecart[:,:,i,j,k]
    end

    for ielem = 1:nelems
        for ilocside = 1:6
            iside = elemtoside[E2S_SIDE,ilocside,ielem]
            flip  = elemtoside[E2S_FLIP,ilocside,ielem]
            if flip == 1
                sidetoelem[S2E_ELEM,iside] = ielem
                sidetoelem[S2E_LOCSIDE,iside] = ilocside
            else
                sidetoelem[S2E_NBELEM,iside] = ielem
                sidetoelem[S2E_NBLOCSIDE,iside] = ilocside
                sidetoelem[S2E_FLIP,iside] = flip
            end
        end
    end


    Connectivity(sidetoelem, elemtoside, sideprops, mortarinfo)
end

function nodecoordsunitcube(nx::T, ny::T, nz::T, x0, x1) where T
    ngeo = 2
    #create equidistant points in each element for unitcube with side [-1,1]
    d = x1-x0
    dx = d[1]/nx
    dy = d[2]/ny
    dz = d[3]/nz
    nodecoords=zeros(3,ngeo,ngeo,ngeo,nx*ny*nz)
    ielem=1
    for k = 1:nz, j = 1:ny,  i = 1:nx
        for ii=1:ngeo, ji=1:ngeo, ki=1:ngeo
        nodecoords[1,ii,ji,ki,ielem] = (x0[1] + dx*(i-1)) + (ii-1)*(dx/(ngeo-1))
        nodecoords[2,ii,ji,ki,ielem] = (x0[2] + dy*(j-1)) + (ji-1)*(dy/(ngeo-1))
        nodecoords[3,ii,ji,ki,ielem] = (x0[3] + dz*(k-1)) + (ki-1)*(dz/(ngeo-1))
        end
        ielem+=1
    end
    return nodecoords
end
