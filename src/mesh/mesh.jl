@reexport module Mesh

using HDF5
using ..Haflinger
import ..Globals: rn

include("spacefillingcurve.jl")
using .SpaceFillingCurve

export meshinitisdone
export mesh, mesh2, meshd, meshs
export nsides, nbcsides, nelems, nglobalelems, myelemoffset
export VolMetrics, SurfMetrics, Connectivity, Meshtype

include("mappings.jl")
include("mesh_vars.jl")
include("mesh_cartesian.jl")
include("mesh_readin.jl")
include("mesh_prepare.jl")
include("metrics.jl")


function initmesh(params)
    initstart("MESH")
    n=N

    global meshmode = getparameter(params, "meshmode", required = false, default = 0)
    if meshmode == 0
        global mesh = CartMesh(params, n)
    elseif meshmode == 1
        global mesh = H5Mesh(  params, n)
    end
    c = mesh.info

    #-----------------|-----------------|-------------------|
    #    U_master     | U_slave         |    FLUX           |
    #-----------------|-----------------|-------------------|
    #  BCsides        |                 |    BCSides        |
    #  InnerMortars   |                 |    InnerMortars   |
    #  InnerSides     | InnerSides      |    InnerSides     |
    #  MPI_MINE sides | MPI_MINE sides  |    MPI_MINE sides |
    #                 | MPI_YOUR sides  |    MPI_YOUR sides |
    #  MPIMortars     |                 |    MPIMortars     |
    #-----------------|-----------------|-------------------|

    # Definition of ranges and mesh constants
    global ngeo             = size(mesh.x,2)
    global nelems           = c.elems
    global nglobalelems     = mesh.mpiconn.mpielemoffset[end]
    global myelemoffset     = mesh.mpiconn.mpielemoffset[myrank+1]
    global nsides           = c.sides
    global nbcsides         = c.bcsides
    global bcsides          = 1:nbcsides
    global mortarinnersides = (bcsides.stop+1):(bcsides.stop+c.mortarinnersides)
    global innersides       = (mortarinnersides.stop+1):(mortarinnersides.stop+c.innersides)
    global mpisidesmine     = (innersides.stop+1):(innersides.stop+c.mpisidesmine)
    global mpisidesyour     = (mpisidesmine.stop+1):(mpisidesmine.stop+c.mpisidesyour)
    global mpimortars       = (mpisidesyour.stop+1):(mpisidesyour.stop+c.mortarmpisides)

    global mastersides      = 1:mpisidesmine.stop
    global slavesides       = innersides.start:mpisidesyour.stop
    global mpisides         = mpisidesmine.start:mpisidesyour.stop


    # Reshaped structs
    global mesh2 = r2(mesh)
    global meshd = rd(mesh)
    global meshs = (mesh, mesh2, meshd)

    mb = MeshBasis(FT, n, ngeo, NODETYPE)

    v, vd, s = mesh.vol, meshd.vol, meshd.surf
    r = DIM == 3 ? (:,:,:,:) : (:,:,:)
    if interpolatefromtree
      t = VolMetrics(FT, n, mesh.tree.ntrees)
      @views for i = 1:ntrees
          calcvolmetrics( mb, mesh.tree.x[r...,i],
                          t.xgp[r...,i], t.f[r...,i], t.g[r...,i], t.h[r...,i], t.sjac[r...,i] )
      end
    end
    stat = 1
    @views for i = 1:nelems
        calcvolmetrics( mb, mesh.x[r...,i],
                        v.xgp[r...,i], v.f[r...,i], v.g[r...,i], v.h[r...,i], v.sjac[r...,i] )
        calcsurfmetrics(mb, v.xgp[r...,i], v.f[r...,i], v.g[r...,i], v.h[r...,i])
        fillsurfmetricarray(mb, mesh.surf, i)
        calcmortarsurfmetrics(mb, mesh.surf, i)

        stat=min(stat,checkjacobian(vd.sjac[:,:,i], vd.xgp[:,:,i], i + myelemoffset))
    end
    if stat < 0
        terminate("Errors in mesh found")
    end
    if USEMPI
        s = rc(mesh.surf)
        for t in (s.normvec, s.tangvec1, s.tangvec2, s.surfelem, s.xgp)
            rreq = receivempi(rn(t, Val(-1)), RECVYOUR, mesh.mpiconn) # Receive YOUR / Geo: master -> slave
            sreq = sendmpi(   rn(t, Val(-1)), SENDMINE, mesh.mpiconn) # SEND MINE / Geo: master -> slave
            MPI.Waitall!([sreq; rreq])
        end
    end
    @simd for i = 1:length(v.sjac)
        v.sjac[i] = 1/v.sjac[i]
    end

    #printstuff()
    global meshinitisdone = true
    initdone("MESH")
end

function printstuff()
    println("sideprops")
    for i=1:mesh.info.sides
        println(mesh.conn.sideprops[:,i])
    end
    println("mortarinfo")
    for i=1:mesh.info.sides
        println(mesh.conn.mortarinfo[:,:,i])
    end
    println("sidetoelem")
    for i=1:mesh.info.sides
        println(mesh.conn.sidetoelem[:,i])
    end
    println("elemtoside")
    for i=1:mesh.info.elems
        println(mesh.conn.elemtoside[:,:,i])
    end
    println("surfelem")
    for i=1:mesh.info.sides
        println(mesh.surf.surfelem[:,:,1,i])
    end
end

end
