####
# Module containing routines to read the mesh and BCs from a HDF5 file
#
# This module contains the following routines related to mesh IO
# - parallel HDF5-based mesh IO
# - readin of mesh coordinates and connectivity
# - readin of boundary conditions
####

# TODO: evaluate named tuples for accessing array components with julia 0.7/1.0

#  Named parameters for ElemInfo array in mesh file
const ELEM_TYPE=1
const ELEM_ZONE=2
const ELEM_FIRSTSIDE=3
const ELEM_LASTSIDE=4
const ELEM_FIRSTNODE=5
const ELEM_LASTNODE=6

#  Named parameters for SideInfo array in mesh file
const SIDE_TYPE=1
const SIDE_ID=2
const SIDE_NBELEM=3
const SIDE_FLIP=4
const SIDE_BCID=5

"""
This subroutine reads the mesh from the HDF5 mesh file. The connectivity and
further relevant information as flips (i.e. the orientation of sides towards
each other) is already contained in the mesh file. For parallel computations
the number of elements will be distributed equally onto all processors and
each processor only reads its own subset of the mesh.
For a documentation of the mesh format see the documentation provided with
HOPR (hopr-project.org). The arrays ElemInfo, SideInfo and NodeCoords are read,
alongside with the boundary condition data. If the mesh is non-conforming and
based on a tree representation, the corresponding tree data
(Coords, parameter ranges, connectivity) is also read.
"""
function H5Mesh(params, n)
    global meshfile   = getparameter(params, "meshfile"  , required=true)
    global usecurveds = getparameter(params, "usecurveds", required=false, default=true)
    userbcs    = getparameter(params, "bcs"       , required=false, default=Array{Any,1}() )

    @assert isfile(meshfile) "Mesh readin: File $meshfile does not exist"
    mprintln("READING MESH FROM DATA FILE $meshfile ...")

    # Open mesh file, read BCs, connectivity, coords, mortars
    f = openfile(meshfile; mode="r", mpio=USEMPI)
    bcs, periodicbcmap = getbcs(f,userbcs)
    elems, mpielemoffset = readconnectivity(f, bcs)
    x     = readcoords(f, elems, usecurveds)
    tree  = readmortars(f, elems)
    close(f)

    # IO is now finished, start building connectivity and exchange information
    infoloc, infoglob = getstatisitics(elems, bcs, size(x,2))
    setlocalsideids(bcs, elems, infoloc, periodicbcmap)
    mpiconn  = MPIConnectivity(elems, mpielemoffset, infoloc)
    exchangeflip(     mpiconn, elems, infoloc)
    conn     = Connectivity(   elems, infoloc)

    vol      = VolMetrics( FT, n, infoloc.elems)
    surf     = SurfMetrics(FT, n, infoloc.sides)
    mappings = Mappings(MT,n)

    H5Mesh(vol, surf, conn, mappings, x, tree, bcs, mpiconn, infoloc)
end

"""
This subroutine will read boundary conditions from the HDF5 mesh file and from the parameter file.
The parameters defined in the mesh file can be overridden by those defined in the parameter file,
by specifying the boundary name and a new boundary condition set which consists of a type and a state.
"""
function getbcs(f,userbcs)
    # Read boundary names from data file
    bcnames = read(f,"BCNames")
    bctypes = Array{Int,2}(read(f,"BCType"))
    @assert size(bctypes)==(4,size(bcnames,1)) "Problem in readbc: number of bctypes and bcnames do not match"
    # User may have redefined boundaries in the ini file. So we remap the boundary conditions
    nbcs     = size(bcnames,1)
    nuserbcs = size(userbcs,1)
    userbcfound = zeros(Bool,nuserbcs)
    mprintln(" BOUNDARY CONDITIONS")
    for ibc = 1:nbcs
      for u = 1:nuserbcs
        a = lowercase(bcnames[ibc])
        b = lowercase(userbcs[u][1])
        if contains(a,b)
          bc = userbcs[u][2]
          if (bc[1] == 1) && (bctypes[1,ibc] != 1)
            safeerror("Remapping non-periodic to periodic BCs is not possible!")
          end
          mprintln(" |     Boundary in HDF file found | $(bcnames[ibc])")
          mprintln(" |                            was | $(bctypes[[1,3],ibc])")
          mprintln(" |                      is set to | $bc")
          bctypes[[1,3],ibc] = bc
          userbcfound[u] = true
        end
      end
    end
    for u = 1:nuserbcs
      if !userbcfound[u]
        safeerror("Boundary condition specified in parameter file has not been found: $(userbcs[u][1])")
      end
    end
    mprint(" | "); fillpost(30,"Name"); mprint(" | "); fillpre(10,"Type","State","Alpha"); mprint("\n")
    for i = 1:nbcs
        mprint(" | "); fillpost(30,"$(bcnames[i])"); mprint(" | "); fillpre(10,bctypes[:,i]...); mprint("\n")
    end
    bcs = [BC(bcnames[i], bctypes[:,i]... ) for i=1:nbcs]

    # Get connection between periodic BCs

    # Build a map "PeriodicBCMap" that holds the connections of periodic slave BCs to the corresponding master BCs.
    # For a periodic connection two boundary conditions are stored in the list of all boundary conditions.
    # One BC with a negative BC_ALPHA for the slave and one BC with a positive BC_ALPHA (of same absolute value) for
    # the master sides. To connect the two periodic boundary conditions the map "PeriodicBCMap" is build, which stores
    # for all BC indices an integer with following meaning:
    #   -2 : initial value (if map contains any -2, after building the map, then there are unconnected periodic BCs!)
    #   -1 : BC is not periodic (wall, inflow, ...) OR it is the master side of a periodic BC
    #   >0 : BC is the slave of a periodic BC. The value is the index of the master BC.
    # This map is used below to overwrite the BCindex of all periodic slave side to the BCindex of the corresponding master side.
    # Therewith slave and master side have the same BCindex.
    periodicbcmap = fill(-2, nbcs) # connected periodic BCs
    for i=1:nbcs
        if bcs[i].bctype == 1
            if     bcs[i].alpha > 0 # master of periodic BC
                periodicbcmap[i] = -1
            elseif bcs[i].alpha < 0 # slave of periodic BC
                for j = 1:nbcs # iterate over all other BCs and find corresponding master BC
                  if (bcs[j].alpha == -bcs[i].alpha) && bcs[j].bctype == 1
                      periodicbcmap[i] = j # save index of master BC
                  end
                end
            end
        else
           periodicbcmap[i] = -1 # not periodic
        end
    end
    if any(periodicbcmap .== -2)
      safeerror("Periodic connection not found.")
    end
    bcs, periodicbcmap
end

"""
Partition the mesh by numbers of processors. Elements are distributed equally to all processors.
"""
function buildpartition(nglobalelems, nprocs, rank)
  @assert nglobalelems ≥ nprocs "No. of elements $nglobalelems is smaller than no. of procs $nprocs"
  nelems = nglobalelems / nprocs
  ielem  = nglobalelems - nelems * nprocs
  mpielemoffset = zeros(IT, nprocs+1)
  for i = 0:nprocs-1
      mpielemoffset[i+1] = floor(nelems * i + min(i,ielem))
  end
  mpielemoffset[nprocs+1] = nglobalelems
  nelems = mpielemoffset[rank+2] -  mpielemoffset[rank+1]
  myelemoffset = mpielemoffset[rank+1]

  mpielemoffset, myelemoffset, nelems
end

"""
Find the id of a processor on which an element with a given elem index lies, based on the MPI element offsets defined earlier.
Use a bisection algorithm for faster search.
"""
function elemiproc(elem, mpielemoffset::AbstractVector)
    inrange(el, pos) = el in (mpielemoffset[pos]+1 : mpielemoffset[pos+1])
    low, up   = 1, length(mpielemoffset)-1
    if     inrange(elem, low)
        return low - 1
    elseif inrange(elem, up)
        return up - 1
    end
    #bisection
    #maxsteps = Int(log(nprocs)÷log(2))+1    #1/log(2.)=1.4426950408889634556
    for i = 1:100 # allows for a very laaaarge number of procs
        mid = (up-low)÷2 + low
        if low == up || mid > up || mid < low
            return -1
        end
        if  inrange(elem, mid)
            return mid - 1  #index found!
        elseif elem > mpielemoffset[mid+1] # seek in upper half
            low = mid + 1
        else
            up  = mid
        end
    end
end

# Element data type, containing element mesh information (mesh readin only)
mutable struct Elem{ I <: Integer}
    ind    :: I            # global element index
    eltype :: I            # element type (linear/bilinear/curved)
    zone   :: I            # zone of elements (unused)
    side                   # sides connected to element
end
Elem() = Elem(0,0,0, Array{Any,1}())

# Side data type (mesh readin only)
mutable struct Side{ I <: Integer }
    ind        :: I        # global side ID
    sideid     :: I        # local side ID on Proc
    tmp        :: I
    nbproc     :: I        # neighbor processor (if applicable)
    bcindex    :: I        # index in BC array!
    flip       :: I        # flip of side (0 if master, 1-4 if slave)
    mortartype :: I        # type of mortar: Type1 : 1-4 , Type 2: 1-2 in eta, Type 2: 1-2 in xi
    mortars    :: Array{Side,1} # array of side pointers to slave mortar sides
    elem       :: Elem{I}  # pointer to connected element
    connection :: Side{I}   # pointer to connected neighbour side
    Side{I}(ind::I, sideid::I, tmp::I, nbproc::I, bcindex::I, flip::I, mortartype::I, mortars, elem) where I =
     new(ind, sideid, tmp, nbproc, bcindex, flip, mortartype, mortars, elem)
end
Side(elem) = Side{Int}(0, 0, 0, -1, 0, -1, 0, Array{Side,1}(), elem)

function readarray(f,name::String,dims)
    @assert exists(f,name) "Dataset $name does not exists in HDF5 file $(f.filename)!"
    aglob = USEMPI ? f[name,"dxpl_mpio", HDF5.H5FD_MPIO_COLLECTIVE] : f[name]
    a     = aglob[dims...]
end



function readconnectivity(f, bcs)
    #--------------------------------------------------------------------------
    #                              ELEMENTS
    #
    # Partition the domain, build a list of all local elements and set:
    # global index, type and zone but not the pointers to the sides
    #--------------------------------------------------------------------------
    eleminfoglob = USEMPI ? f["ElemInfo","dxpl_mpio", HDF5.H5FD_MPIO_COLLECTIVE] : f["ElemInfo"]
    nglobalelems = size(eleminfoglob)[2]
    mpielemoffset, myelemoffset, nelems = buildpartition(nglobalelems, nmpiprocs, myrank)
    elemrange    = (myelemoffset+1):(myelemoffset+nelems)
    eleminfo     = Array{Int,2}(eleminfoglob[:,elemrange]) # here the data is actually read

    # Create list of elems
    elems = [ Elem( i+myelemoffset, eleminfo[ELEM_TYPE,i], eleminfo[ELEM_ZONE,i], Array{Side,1}() ) for i = 1:nelems ]

    #--------------------------------------------------------------------------
    #                              SIDES
    #
    # Iterate over all elements and within each element over all sides (6 for hexas).
    # For each side set the following informations:
    #  - nMortars   (Number of small sides attached to a big Mortar side)
    #  - MortarType (zero for non Mortar sides; =1,2 or 3 for big Mortar side;
    #                -1 for small sides belonging to a Mortar)
    #  - Ind        (global index of the side)
    #  - flip
    #  - For the big Mortar sides also iterate over the mortars sides and set:
    #    - Ind
    #    - flip
    #--------------------------------------------------------------------------

    # get offset of local side indices in the global sides
    offsetside = eleminfo[ELEM_FIRSTSIDE, 1] # hdf5 array starts at 0-> -1
    nsides     = eleminfo[ELEM_LASTSIDE , nelems]-eleminfo[ELEM_FIRSTSIDE, 1]
    # read local sideinfo from mesh file
    siderange  = offsetside+1 : offsetside+nsides
    sideinfo   = Array{Int,2}(readarray(f,"SideInfo",[:,siderange]))

    mortarsbytype = [4,2,2]
    # iterate over all local elements and within each element over all sides
    for (i,elem) in zip(1:nelems, elems)
        iside = eleminfo[ELEM_FIRSTSIDE, i] - offsetside #first index -1 in sideinfo
        elem.side = [Side(elem) for i=1:6]
        # build up sides of the element according to CGNS standard
        # assign flip
        for side in elem.side
            iside += 1
            nbelemid = sideinfo[SIDE_NBELEM, iside] # get neighboring element index of element adjacent to this side

            if DIM == 2
              # In 2D check that there is only one layer of elements in z-direction
              if side in elem.side[[1,6]]
                bcindex = sideinfo[SIDE_BCID, iside]
                if elem.ind ≠ nbelemid && bcindex ≤ 0
                  merror("Mesh not oriented in z-direction or more than one layer of elements in z-direction!",
                         "Please set 'orientz = t' or change number of element in z-direction in HOPR parameter file.")
                end
              end
            end
            # If nbElemID <0, this marks a mortar master side.
            # The number (-1,-2,-3) is the Type of mortar
            if nbelemid < 0 # mortar Sides attached!
              side.mortartype = abs(nbelemid)
            end
            if sideinfo[SIDE_TYPE, iside] < 0
              side.mortartype = -1 #marks side as belonging to a mortar
            end

            if side.mortartype ≤ 0 # side is not a big Mortar side
              side.elem = elem
              oriented  = sideinfo[SIDE_ID, iside] > 0
              side.ind  = abs(sideinfo[SIDE_ID, iside])
              if oriented #oriented side
                side.flip = 1
              else #not oriented
                side.flip = mod(sideinfo[SIDE_FLIP, iside],10) + 1
                if side.flip < 1 || side.flip > 5
                    merror("NodeID doesnt belong to side")
                end
              end
            else # side is a big Mortar side
              side.mortars = [Side(elem) for i=1:mortarsbytype[side.mortartype]]
              side.flip = 1
              for mortar in side.mortars # iterate over virtual small Mortar sides
                iside += 1
                if sideinfo[SIDE_ID, iside] < 0
                    merror("Problem in Mortar readin,should be flip=1")
                end
                mortar.flip = 1
                mortar.ind  = abs(sideinfo[SIDE_ID, iside])
              end
            end
        end
    end

    #----------------------------------------------------------------------------------------------------------------------------
    #                              CONNECTIONS
    #
    # Iterate over all elements and within each element over all sides (6 for hexas) and for each big Mortar side over all
    # small virtual sides.
    # For each side do:
    #   - if BC side:
    #       Reset side and do not connect
    #   - if neighboring element is on local processor:
    #       loop over all sides of neighboring element and find side with same index as local side  =  connect
    #   - if neighboring element is on other processor:
    #       create a new virtual neighboring side and element
    #----------------------------------------------------------------------------------------------------------------------------
    for (i,elem) in zip(1:nelems, elems)
      iside = eleminfo[ELEM_FIRSTSIDE, i] - offsetside #first index -1 in Sideinfo
      for locside in elem.side, side in [locside; locside.mortars]
        iside += 1
        nbelemid     = sideinfo[SIDE_NBELEM, iside]
        side.bcindex = sideinfo[SIDE_BCID  , iside]

        # BC sides don't need a connection, except for periodic (BC_TYPE=1) and "dummy" inner BCs (BC_TYPE=100).
        # For all other BC sides: reset the flip and mortars settings, do not build a connection.
        if side.bcindex ≠ 0 # BC
          if !(bcs[side.bcindex].bctype in [1,100])
            side.flip = 1
            side.mortartype = 0
            continue
          end
        end

        if side.mortartype > 0
            continue    # no connection for mortar master
        end
        if isdefined(side,:connection)
           continue # already connected
        end

        # neighboring element exists, since it has a valid global index
        if nbelemid in elemrange # check if neighbor on local proc or MPI connection
          # connect sides with neighbor side with the same index
          for nbside in elems[nbelemid-myelemoffset].side
            for bside in [nbside; nbside.mortars]
              # check for matching indices of local side and side of the neighboring element
              if bside.ind == side.ind
                side.connection = bside
                bside.connection = side
                break
              end
            end
          end
        end
        if nbelemid in 1:nglobalelems && !(nbelemid in elemrange) # check if neighbor on local proc or MPI connection
          side.connection      = Side(Elem())
          side.connection.flip = side.flip
          side.nbproc = elemiproc(nbelemid, mpielemoffset)
        end
      end
    end
    return elems, mpielemoffset
end

"""
Read the node coords and discard curved nodes if requested
"""
function readcoords(f, elems, usecurveds)
    atr = attrs(f)
    ng = read(atr["Ngeo"])[1]+1
    noderange = (elems[1].ind-1)*ng^3+1 : elems[end].ind*ng^3
    nodecoords = reshape(readarray(f,"NodeCoords",[:,noderange]), 3, ng, ng, ng, size(elems,1))
    if !usecurveds
      # throw away all nodes except the 8 corner nodes of each hexa
      nodecoordstmp = zeros(3,2,2,2,size(elems,1))
      nodecoordstmp[:,1,1,1,:] = nodecoords[:,1, 1, 1, :]
      nodecoordstmp[:,2,1,1,:] = nodecoords[:,ng,1, 1, :]
      nodecoordstmp[:,1,2,1,:] = nodecoords[:,1, ng,1, :]
      nodecoordstmp[:,2,2,1,:] = nodecoords[:,ng,ng,1, :]
      nodecoordstmp[:,1,1,2,:] = nodecoords[:,1, 1, ng,:]
      nodecoordstmp[:,2,1,2,:] = nodecoords[:,ng,1, ng,:]
      nodecoordstmp[:,1,2,2,:] = nodecoords[:,1, ng,ng,:]
      nodecoordstmp[:,2,2,2,:] = nodecoords[:,ng,ng,ng,:]
      nodecoords = nodecoordstmp
    end

    if DIM == 2 && any(nodecoords[3,1,1,1,:] > nodecoords[3,1,1,2,:])
        safeerror("Zeta is oriented in negative z-direction, has to be oriented in positive z-direction instead\n.",
                  "Please set 'orientZ = T' in HOPR parameter file.")
    end
    nodecoords
end

"""
Get Mortar specific additional arrays concering Octrees.
General idea of the octrees:
  Starting from a conform mesh each element can be halved in each reference direction leading to
  2, 4 or 8 smaller elements. This bisection can be repeated on the smaller elements. The original
  coarsest element of the conform mesh is the root of a octree and in the following called a 'tree-element'
  or just 'tree'. Only the coordinates of the smallest elements are necessary for a valid mesh representation.
  Nevertheless the mesh format allows to store additional information of the tree-elements (coordinates)
  and the position of the regular elements inside the trees.

  If a mesh is build by refining a conform baseline mesh the refining octree can be
  stored in the mesh file in the following attibutes and arrays:
    'isMortarMesh' : if present and ==1 indicates a non-conforming Mortar mesh
    'NgeoTree'     : polynomial degree of the tree-elements
    'nTrees'       : number of tree-elements
    'nNodeTrees'   : total number of nodes of the tree-elements (NgeoTree+1)^3 * nTrees
    'ElemToTree'   : mapping from global element index (ElemID) to octree index (TreeID), which allows to
                     find to each element the corresponding tree.
                     size = (1:nElems)
    'xiMinMax'     : element bounds of an element inside the reference space [-1,1]^3 of its tree-element
                     size = (1:3, 1:2, 1:nElems)
                     first index = coordinate
                     second index =1 for minimum corner node, =2 for maximum corner node
    'TreeCoords'   : coordinates of all nodes of the tree-elements
"""
function readmortars(f,elems)

    atr = attrs(f)
    ismortarmesh = exists(atr,"isMortarMesh") ? read(atr["isMortarMesh"])[1] : 0
    if ismortarmesh == 1
        ngeotree     = read(atr["NgeoTree"])[1]+1
        #nglobaltrees = read(at["nTrees"])[1]
        ximinmax     = readarray(f,"xiMinMax",[:,:,elems[1].ind:elems[end].ind])
        elemtotree   = readarray(f,"ElemToTree",  [elems[1].ind:elems[end].ind])

        # only read trees, connected to a procs elements
        offsettree = minimum(elemtotree)-1
        elemtotree = elemtotree - offsettree
        ntrees     = maximum(elemtotree)

        noderange = (offsettree*ngeotree^3+1) : (offsettree+ntrees)*ngeotree^3
        treecoords = reshape(readarray(f,"TreeCoords",[:,noderange]), 3, ngeotree, ngeotree, ngeotree, ntrees)
        tree = Tree(ntrees, treecoords,                   ximinmax,                 elemtotree             )
    else
        tree = Tree(0,      Array{FT,5}(undef,0,0,0,0,0), Array{FT,3}(undef,0,0,0), Array{Int,1}(undef, 0) )
    end
    tree
end

function getstatisitics(elems,bcs,ngeo)
    # Iterate over all elements and within each element over all sides (6 for hexas in 3D, 4 for quads in 2D)
    # and for each big Mortar side over all small virtual sides
    nsides = nperiodicsides = nbcsides = nmpisides = nanalyzesides = nmortarsides = nmpiperiodics = 0
    for elem in elems, locside in elem.side, side in [locside; locside.mortars]
      if side.tmp == 0 # if side not counted so far
        nsides += 1
        side.tmp=-1 # mark side as counted
        if isdefined(side,:connection)
           side.connection.tmp=-1 # mark connected side as counted
        end
        if side.bcindex ≠ 0 #side is BC or periodic side
          nanalyzesides += 1
          if isdefined(side,:connection)
            if bcs[side.bcindex].bctype == 1 # periodic side
              nperiodicsides += 1
              if side.nbproc ≠ -1
                nmpiperiodics += 1
              end
            end
          else
            if side.mortartype == 0 #really a BC side
              nbcsides += 1
            end
          end
        end
        if side.mortartype > 0
            nmortarsides += 1
        end
        if side.nbproc ≠ -1 # count total number of MPI sides and number of MPI sides for each neighboring processor
          nmpisides += 1
        end
      end
    end
    ninnersides = nsides - nbcsides - nmpisides - nmortarsides #periodic side count to inner side!!!

    # For each side set "tmp" marker to zero, except for big Mortar sides which have
    # at least one mortar with a neighbor on another processor (set "tmp" to -1).
    nmortarinnersides = nmortarmpisides = 0
    for elem in elems, locside in elem.side
      locside.tmp = 0
      if length(locside.mortars) > 0   # only if side has small virtual sides
        for mortar in locside.mortars # check if mortars have neighbors on other procs
          if mortar.nbproc ≠ -1
            locside.tmp = -1
            break
          end
        end
        if locside.tmp == -1        # if at least one small virtual side has neighbor on another processor
          nmortarmpisides   += 1 # then count big side as a Mortar-MPI-side
        else
          nmortarinnersides += 1 # else count big side as a Mortar-Inner-side
        end
      end # nMortars>0
    end
    if nmortarinnersides + nmortarmpisides  ≠ nmortarsides
       terminate("Sum of inner and MPI mortar sides nInner+nMPI mortars <> nMortars")
    end

    nelems = length(elems)
    nnodes = nelems*ngeo^3 # total number of nodes on this processor

    # Sum up all element, side and node numbers over all processors (just for stdout)
    tmp = [ nelems, nsides, nnodes, nbcsides, ninnersides, nmpisides, 0, 0, nperiodicsides,
            nmpiperiodics, nmortarsides, nmortarinnersides, nmortarmpisides, nanalyzesides ]
    nl = Counters(tmp...)

    if USEMPI
      out = similar(tmp)
      MPI.Allreduce!(tmp, out, MPI.SUM, MPI.COMM_WORLD)
      tmp = out
    end
    ng = Counters(tmp...)

    if MPIROOT
      print(" | "); fillpost(30,"nElems");               print(" | "); fillpost(6,ng.elems);              print("\n")
      print(" | "); fillpost(30,"nNodes");               print(" | "); fillpost(6,ng.nodes);              print("\n")
      print(" | "); fillpost(30,"nSides");               print(" | "); fillpost(6,ng.sides-ng.mpisides÷2);print("\n")
      print(" | "); fillpost(30,"nSides");               print(" | "); fillpost(6,ng.sides-ng.mpisides÷2);print("\n")
      print(" | "); fillpost(30,"nSides,BC");            print(" | "); fillpost(6,ng.bcsides);            print("\n")
      print(" | "); fillpost(30,"nSides,MPI");           print(" | "); fillpost(6,ng.mpisides÷2);         print("\n")
      print(" | "); fillpost(30,"nSides,Inner");         print(" | "); fillpost(6,ng.innersides);         print("\n")
      print(" | "); fillpost(30,"nSides,Mortar");        print(" | "); fillpost(6,ng.mortarsides);        print("\n")
      print(" | "); fillpost(30,"nSides,MortarInner");   print(" | "); fillpost(6,ng.mortarinnersides);   print("\n")
      print(" | "); fillpost(30,"nSides,MortarMPI");     print(" | "); fillpost(6,ng.mortarmpisides);     print("\n")
      print(" | "); fillpost(30,"nSidesPeriodic,Total"); print(" | "); fillpost(6,ng.periodicsides-ng.mpiperiodics÷2);print("\n")
      print(" | "); fillpost(30,"nSidesPeriodic,Inner"); print(" | "); fillpost(6,ng.periodicsides-ng.mpiperiodics);  print("\n")
      print(" | "); fillpost(30,"nSidesPeriodic, MPI");  print(" | "); fillpost(6,ng.mpiperiodics÷2);     print("\n")
      print(" | "); fillpost(30,"nAnalyzeSides");        print(" | "); fillpost(6,ng.analyzesides);       print("\n")
    end
    nl, ng
end
