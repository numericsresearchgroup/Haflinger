@reexport module Restart

export restartinitisdone, dorestart, restart

const dorestart = false
const restartinitisdone = true

# Add restart code here
function restart()
  if !dorestart 
    return
  end

  return
end

end
