module Globals

using ..Haflinger

export DIM, FT, IT, MT, BFT
export N, NODETYPE, USEPARABOLIC, USESPLIT
export Data3D
export safeerror, terminate
export initstart, initdone
export AbstractData, Data
export rn, rc, rd, r2, r3
export @d2, @d3
export @bench, @mpi, @root
export add_dimr, add_diml
export fillpre, fillpost
export needsgc, rungc

export XI_MINUS, XI_PLUS, ETA_MINUS, ETA_PLUS, ZETA_MINUS, ZETA_PLUS
export S2E_ELEM, S2E_NBELEM, S2E_LOCSIDE, S2E_NBLOCSIDE, S2E_FLIP
export E2S_SIDE, E2S_FLIP
export SP_BC, SP_MORTAR, SP_ANALYZE
export MI_SIDE,  MI_FLIP
export BC_TYPE,  BC_STATE,  BC_ALPHA
export SEND, RECV

const BENCH        = false
const DIM          = 3
const FT           = Float64  # general floats
const BFT          = BigFloat # extended accuracy datatype for building basis routines
const IT           = Int64    # general integers
const MT           = UInt8    # compact integers (e.g. for mappings)
const N            = 4
const NODETYPE     = :nodetypegl
const USEPARABOLIC = true
const USESPLIT     = false


# Side names
const ZETA_MINUS = 1
const ETA_MINUS  = 2
const XI_PLUS    = 3
const ETA_PLUS   = 4
const XI_MINUS   = 5
const ZETA_PLUS  = 6

# Entry position in SideToElem
const S2E_ELEM      = 1
const S2E_NBELEM    = 2
const S2E_LOCSIDE   = 3
const S2E_NBLOCSIDE = 4
const S2E_FLIP      = 5

# Entry position in SideProps
const SP_BC      = 1
const SP_MORTAR  = 2
const SP_ANALYZE = 3

# Entry position in ElemToSide
const E2S_SIDE = 1
const E2S_FLIP = 2

# Entry position in BC
const MI_SIDE = 1
const MI_FLIP = 2

# Entry position in BC
const BC_TYPE  = 1
const BC_STATE = 2
const BC_ALPHA = 3

# Entry position in BC
const SEND = 1
const RECV = 2

macro bench(args)
    if BENCH
         s=String(Symbol(args))
         :(println($s); @time $(esc(args)))
    else
        :($(esc(args)))
    end
end
macro mpi(args)
    if USEMPI && nmpiprocs > 1
        :($(esc(args)))
    end
end
macro root(args)
    if MPIROOT
        :($(esc(args)))
    end
end

abstract type AbstractData end
struct Data{T <: AbstractFloat, V, N} <: AbstractData
    v::Val{V}
    n::Val{N}
    vol::Array{T}
    master::Array{T}
    slave::Array{T}
    face::Array{T} # used as tmp
end
function Data(T, nvar, n, nsides, nelems)
    vol    = zeros(T, nvar, n, n, n, nelems)  # the volume
    master = zeros(T, nvar, n, n,    nsides) # left of the interface
    slave  = similar(master)                 # right of the interface
    face   = zeros(T, nvar, n, n, 2*DIM)
    Data{T,nvar,n}(Val(nvar), Val(n), vol, master, slave, face)
end
Data(T) = Data(T, 1, 1, 1, 1)

@inline rc(x) = rn(x, Val(-1))  # squash first dims, mainly used for comm
@inline rd(x) = rn(x, Val(0))  # only squash dofs
@inline r2(x) = rn(x, Val(2))
@inline r3(x) = rn(x, Val(3))
# V > 0 squashes last V dims, V < 0 squashes first V dims (TODO: make this type stable...)
#@inline rn(a::AbstractArray, i::Val{V}) where V = V > 0 ? reshape(a, i) : reshape(a, :, size(a)[abs(V-1):end]...)
@inline rn(a::AbstractArray, i::Val{V}) where V = reshape(a, i)
@inline rn(a::AbstractArray,  ::Val{0})  = reshape(a, size(a,1), :, size(a, ndims(a)) )
@inline rn(a::AbstractArray,  ::Val{-1}) = reshape(a,            :, size(a, ndims(a)) )
@inline rn(a::AbstractArray,  ::Val{-2}) = reshape(a,            :, size(a)[3:ndims(a)]...)
@inline rn(d::Data{T,V,N}, i) where {T,V,N} =
    Data{T,V,N}(d.v, d.n, rn(d.vol, i), rn(d.master, i), rn(d.slave, i), rn(d.face, i))

macro d2(args)
    return DIM == 2 ? :($(esc(args))) : nothing
end
macro d3(args)
    return DIM == 3 ? :($(esc(args))) : nothing
end

add_diml(x::AbstractArray{T,N}) where { T,N } = reshape(x, (1,size(x)...  ))
add_dimr(x::AbstractArray{T,N}) where { T,N } = reshape(x, (  size(x)...,1))

fillpre( l,strs...) = for s in strs mprint(" "^(max(1,l-length("$s"))) * "$s") end
fillpost(l,strs...) = for s in strs mprint("$s" * " "^(max(1,l-length("$s")))) end

"""
Check whether GC needs to be run (memory is above specified threshold)
"""
needsgc(mem) = (Base.gc_total_bytes(Base.gc_num()) - mem > 500000000)
"""
Run the garbage collector manually (required to prevent unnecessary GCing in parallel)
"""
function rungc()
    GC.enable(true)
    GC.gc()
    GC.enable(false)
    mem = Base.gc_total_bytes(Base.gc_num())
end

"""
Safely raise an error with "soft" MPI finalization, keeping only the root will print the message:
- MPI mode and no interactive session:
  Finalize MPI and raise an error. This will generate a stack trace and terminate Julia
- MPI mode and interactive session:
  Raise an error. Keep MPI running.
- Serial mode: simply raise an error.

This function can only be used if ALL processes are guaranteed to generate the same error at the same time!
The intended use is to raise an error without terminating Julia by calling MPI.Abort, especially in interactive sessions.
This provides a clean stack trace and without MPI error spaming, e.g. if the initialization fails or a file is not found.

Criteria where saferror may be used:
0. In case of doubt stick with terminate, which is always safe!
1. In functions which are BY DESIGN (!) called by all MPI processes, i.e. will not be called by single processes or subgroups.
2. The criteria for calling safeerror must be identical among all processors.
3. The function is only used during the init phase.
4. The error must not originate from MPI errors (e.g. during MPI init)
5. The error must not originate from checking roundof errors (e.g. accuracy of interpolation matrices)
"""
function safeerror(msg...; err::Integer=2)
    if USEMPI && !isinteractive() && MPI.Initialized() && !MPI.Finalized()
        MPI.Finalize()
    end
    MPIROOT ? error(msg...) : ( isinteractive() ? error() : exit(err) )
end

"""
A severe error has ocurred:
- in single mode, raise an error exception.
- in MPI mode (nmpiprocs > 1), call MPI.Abort.
  ------------------------------------------------------
  THIS FUNCTION WILL TERMINATE THE SOLVER AND JULIA !!!!
  ------------------------------------------------------
"""
function terminate(msg...; err::Integer=2)
    if USEMPI
    # we cannot use error, since it will prevent us from calling MPI.Abort
        @warn "$(msg...)\n          Program abort caused on Proc $myrank \n"
        show(STDOUT, "text/plain", stacktrace())
        print("\n")
        MPI.Abort(MPI.COMM_WORLD, err)
    else
        error(msg...)
    end
end

initstart(x::String)           = ( Parallel.mprint("-"^100 * "\n");       mprint(" INIT $x ...\n")  )
initdone(x::String)            = ( Parallel.mprint(" INIT $x DONE!\n"))
initdone(x::String, t::Number) = ( Parallel.mprint(" INIT $x DONE! [ $t sec ]\n"))

"""
Create a timestamp, consistent of a filename (project name + processor) and current time niveau
"""
timestamp(file::String, time::Number)                = "$(file)_$(@sprintf("%017.9f",time))"
timestamp(file::String, time1::Number, time2::Number) = "$(file)_$(@sprintf("%017.9f",time1))-$(@sprintf("%017.9f",time2))"

end
