__precompile__()
module Constants

export DIM, FT, IT, MT, BFT
export N,NODETYPE, USEPARABOLIC, USESPLIT
export Data3D
export safeerror, terminate
export initstart, initdone
export Data, EmptyData
export rn, rc, rd, r2, r3
export @d2, @d3
export add_dimr, add_diml
export fillpre, fillpost

export XI_MINUS, XI_PLUS, ETA_MINUS, ETA_PLUS, ZETA_MINUS, ZETA_PLUS
export S2E_ELEM, S2E_NBELEM, S2E_LOCSIDE, S2E_NBLOCSIDE, S2E_FLIP
export E2S_SIDE, E2S_FLIP
export MI_SIDE,  MI_FLIP
export BC_TYPE,  BC_STATE,  BC_ALPHA
export SEND, RECV


const DIM          = 3
const FT           = Float64  # general floats
const BFT          = BigFloat # extended accuracy datatype for building basis routines
const IT           = Int64    # general integers
const MT           = UInt8    # compact integers (e.g. for mappings)
const N            = 5
const NODETYPE     = :nodetypegl
const USEPARABOLIC = false
const USESPLIT     = false


# Side names
const ZETA_MINUS = 1
const ETA_MINUS  = 2
const XI_PLUS    = 3
const ETA_PLUS   = 4
const XI_MINUS   = 5
const ZETA_PLUS  = 6

# Entry position in SideToElem
const S2E_ELEM      = 1
const S2E_NBELEM    = 2
const S2E_LOCSIDE   = 3
const S2E_NBLOCSIDE = 4
const S2E_FLIP      = 5

# Entry position in ElemToSide
const E2S_SIDE = 1
const E2S_FLIP = 2

# Entry position in BC
const MI_SIDE = 1
const MI_FLIP = 2

# Entry position in BC
const BC_TYPE  = 1
const BC_STATE = 2
const BC_ALPHA = 3

# Entry position in BC
const SEND = 1
const RECV = 2

struct Data{T <: AbstractFloat, V, N}
    v::Val{V}
    n::Val{N}
    vol::Array{T}
    master::Array{T}
    slave::Array{T}
    face::Array{T} # used as tmp
end
function Data(T, nvar, n, nsides, nelems)
    vol    = zeros(T, nvar, n, n, n, nelems)  # the volume
    master = zeros(T, nvar, n, n,    nsides) # left of the interface
    slave  = similar(master)                 # right of the interface
    face   = zeros(T, nvar, n, n, 2*DIM)
    Data{T,nvar,n}(Val(nvar), Val(n), vol, master, slave, face)
end
EmptyData(T) = Data(Val(0), Val(0), [Array{T}() for i = 1:4]...)

rc(x) = rn(x, Val(-1))  # squash first dims, mainly used for comm
rd(x) = rn(x, Val(0))  # only squash dofs
r2(x) = rn(x, Val(2))
r3(x) = rn(x, Val(3))
@inline rn(a::AbstractArray, i::Val{V}) where V = reshape(a, i)
@inline rn(a::AbstractArray,  ::Val{0})  = ndims(a) > 2 ? reshape(a, size(a)[1], :, size(a)[end] ) : a
@inline rn(a::AbstractArray,  ::Val{-1}) = ndims(a) > 1 ? reshape(a,             :, size(a)[end] ) : a
@inline rn(d::Data{T,V,N}, i) where {T,V,N} =
    Data{T,V,N}(d.v, d.n, rn(d.vol, i), rn(d.master, i), rn(d.slave, i), rn(d.face, i))

macro d2(args)
    return DIM == 2 ? :($(esc(args))) : nothing
end
macro d3(args)
    return DIM == 3 ? :($(esc(args))) : nothing
end

add_diml(x::AbstractArray{T,N}) where { T,N } = reshape(x, (1,size(x)...  ))
add_dimr(x::AbstractArray{T,N}) where { T,N } = reshape(x, (  size(x)...,1))

fillpre( l,strs...) = for s in strs print(" "^(max(1,l-length("$s"))) * "$s") end
fillpost(l,strs...) = for s in strs print("$s" * " "^(max(1,l-length("$s")))) end

initstart(x::String)           = ( print("-"^100 * "\n"); print(" INIT $x ...\n")  )
initdone(x::String)            = ( print(" INIT $x DONE!\n"))
initdone(x::String, t::Number) = ( print(" INIT $x DONE! [ $t sec ]\n"))

"""
Create a timestamp, consistent of a filename (project name + processor) and current time niveau
"""
timestamp(file::String, time ::Number)                = "$(file)_$(@sprintf("%017.9f",time))"
timestamp(file::String, time1::Number, time2::Number) = "$(file)_$(@sprintf("%017.9f",time1))-$(@sprintf("%017.9f",time2))"

end
