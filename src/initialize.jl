using .Globals
using .Parallel

global projectname = "EMPTY"
global initializationdone = false

export ini, initsolver, initsimulation, runsimulation, @p
export starttime, projectname

const ini = Dict{String,Any}()
macro p(val)
    return :(push!(ini,$val))
end

function initsolver(parameterfile, restartfile="")
    global ini
    include(parameterfile)
    initsimulation(ini, restartfile)
end

"""
Call the init routines of the modules
"""
function initsimulation(ini, restartfile="")
    initstart("HAFLINGER")
    if initializationdone
        minfo(" The simulation has already been initialized, finalizing first before reinit!")
        finalizesimulation()
    end
    #Parallel.initmpi(ini)
    if isinteractive()
        global starttime = mpitime()
    end
    global projectname = getparameter(ini,"projectname"; required=true )

    Interpolation.initinterpolation(ini)
    #initoutput(ini)
    IO_HDF5.initio_hdf5(ini)
    Mesh.initmesh(ini)
    IO_HDF5.initrestart(ini, restartfile)
    #initfilter(ini)
    #initoverintegration(ini)
    #initindicator(ini)
    if Parallel.USEMPI
      Parallel.initmpivars(ini)
    end
    Equation.initequation(ini)
    DG.initdg(ini)
    #initsponge(ini)
    Timedisc.inittimedisc(ini)
    Analyze.initanalyze(ini)
    #initrecordpoints(ini)

    IO_HDF5.restart(restartfile)
    # delete all state files with time stamp > restarttime
    #@root IO_HDF5.flushfiles(projectname, "STATE", max(restarttime, 0.))

    mprint("-"^100 * "\n")
    initdone("HAFLINGER", Parallel.mpitime()-starttime)
end

"""
Start the simulation
"""
function runsimulation()
    # write number of grid cells and dofs only once per computation
    global starttime = isinteractive() ? mpitime() : starttime
    mprintln(" #GridCells : $(Mesh.nglobalelems)")
    mprintln(" #DOFs      : $(Mesh.nglobalelems * N^3)")
    mprintln(" #Procs     : $nmpiprocs")
    mprintln(" #DOFs/Proc : $(Mesh.nglobalelems * N^3 / nmpiprocs)")
    Timedisc.timedisc(starttime)
end

"""
Finalize the simulation
NOTE: We do not have to finalize MPI open exit in Julia, since this is done automatically
"""
function finalizesimulation()
end
