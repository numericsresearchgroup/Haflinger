__precompile__(false)
#"""
#Contains the routines that set up communicators and control non-blocking communication
#"""
module Parallel

using MPI
using ..Haflinger

"Simulation is run using more than one MPI process"
const USEMPI = false
if USEMPI && !MPI.Initialized() && !MPI.Finalized()
    MPI.Init()
end
const COMM   = MPI.COMM_WORLD

"Ranks in COMM_WORLD,COMM_NODE,COMM_LEADERS,COMM_WORKERS"
const myrank    = USEMPI ? MPI.Comm_rank(COMM) : 0
global mylocalrank = global myleaderrank = 0; global myworkerrank = -1
"Number of procs in COMM_WORLD,COMM_NODE,COMM_LEADERS,COMM_WORKERS"
const nmpiprocs = USEMPI ? MPI.Comm_size(COMM) : 1
global nlocalprocs = global nleaderprocs = 1; global nworkerprocs = 0
"Process is MPI root process"
const MPIROOT   = myrank == 0
"Process is MPI NUMA root process"
global MPILOCALROOT = true
"NUMA node MPI communicator"
global MPI_COMM_NODE    = MPI.COMM_WORLD
"NUMA node root communicator"
global MPI_COMM_LEADERS = MPI.COMM_WORLD
"NUMA node worker communicator (not really used)"
global MPI_COMM_WORKERS = MPI.COMM_WORLD


const RECVYOUR = const SENDMINE = 1
const SENDYOUR = const RECVMINE = 2

export MPI, USEMPI, MPIROOT, MPILOCALROOT
export MPI_COMM_NODE, MPI_COMM_LEADERS, MPI_COMM_WORKERS
export myrank, mylocalrank, myleaderrank, myworkerrank
export nmpiprocs, nlocalprocs, nleaderprocs, nworkerprocs

export initmpi, finalizempi, initmpivars
export sendmpi, receivempi
export SENDMINE, SENDYOUR, RECVMINE, RECVYOUR

export mprint, mprintln, minfo, mwarn, mpitime
export iprint, iprintln, iinfo, iwarn, serial

#"""
#Perform basic MPI initialization.
#Calls initialization routine of the MPI library and sets myrank, nmpiprocs and MPIROOT.
#If single mode is requested we set the MPI variables to default values.
#At this point MPI initialization is not yet completed, the MPI communicators are built later.
#"""
#function initmpi(params; comm::MPI.Comm = MPI.COMM_WORLD)
#  initstart("MPI")
#  #global const USEMPI = getparameter(params, "USEMPI"; required=false, default=true)
#
#  initdone("MPI")
#end

"""
Finalize MPI
"""
function finalizempi()
    if USEMPI && !isinteractive() && MPI.Initialized() && !MPI.Finalized()
        MPI.Finalize()
    end
end


"""
Print a warning to the command line (only MPI root)
"""
@inline mprint(  msg...) = MPIROOT ? print(    msg...  ) : nothing
@inline mprintln(msg...) = MPIROOT ? println(  msg...  ) : nothing
@inline minfo(   msg...) = MPIROOT ? (@info "$(msg...)") : nothing
@inline mwarn(   msg...) = MPIROOT ? (@warn "$(msg...)") : nothing
@inline iprint(  msg...) = print(  "Proc $myrank :  $(msg...)")
@inline iprintln(msg...) = println("Proc $myrank :  $(msg...)")
@inline iinfo(   msg...) = (@info  "Proc $myrank :  $(msg...)")
@inline iwarn(   msg...) = (@warn  "Proc $myrank :  $(msg...)")

function serial(f,comm=MPI.COMM_WORLD)
    for i = 0:nmpiprocs-1
        if myrank == i
            f()
        end
        MPI.Barrier(comm)
    end
end
#@inline merror(msg...) = MPIROOT ? error(msg...) : Void  # this will break MPI

"""
Calculate current time (check whether MPI Wtime is available some time in Julia)
"""
mpitime(comm::MPI.Comm = MPI.COMM_WORLD) = USEMPI ? ( MPI.Barrier(comm); MPI.Wtime() ) : time()

"""
Initialize derived MPI variables used for communication
"""
function initmpivars(params)

  # split communicator into smaller groups (e.g. for local nodes)
    groupsize = getparameter(params, "GroupSize"; required = false, default = 0)
    if groupsize < 1 # group procs by node
        global MPI_COMM_NODE = MPI.Comm_split(MPI.COMM_WORLD, myrank, myrank)
    else # use groupsize
        color = myrank / groupsize
        global MPI_COMM_NODE = MPI.Comm_split(MPI.COMM_WORLD, color, myrank)
    end
    global mylocalrank  = MPI.Comm_rank(MPI_COMM_NODE)
    global nlocalprocs  = MPI.Comm_size(MPI_COMM_NODE)
    global MPILOCALROOT = ( mylocalrank  ==  0 )

  # now split global communicator into small group leaders and the others
    global MPI_COMM_LEADERS = MPI.COMM_NULL
    global MPI_COMM_WORKERS = MPI.COMM_NULL
    if mylocalrank == 0
        global MPI_COMM_LEADERS = MPI.Comm_split(MPI.COMM_WORLD, 0, myrank)
        global myleaderrank = MPI.Comm_rank(MPI_COMM_LEADERS)
        global nleaderprocs = MPI.Comm_size(MPI_COMM_LEADERS)
        global nworkerprocs = nmpiprocs - nleaderprocs
    else
        global MPI_COMM_WORKERS = MPI.Comm_split(MPI.COMM_WORLD, 1, myrank)
        global myworkerrank = MPI.Comm_rank(MPI_COMM_WORKERS)
        global nworkerprocs = MPI.Comm_size(MPI_COMM_WORKERS)
        global nleaderprocs = nmpiprocs - nworkerprocs
    end
end


"""
Function that controls the receive operations for the face data that has to be exchanged between processors.
SendID                                   #< defines the send / receive direction -> 1=send MINE
                                         #< / receive YOUR, 2=send YOUR / receive MINE
"""
function receivempi(data, sendid, mc)
    nbproc, offsets = mc.nbproc, mc.offsetmpisides_rec
    #mprintln("recv")
    #serial() do
    #    iprintln("nbproc")
    #    iprintln(nbproc[:])
    #    iprintln("offsets")
    #    iprintln(offsets[:])
    #    iprintln("sidesrec")
    #    iprintln(mc.nmpisides_rec[:])
    #end
    request = fill(MPI.REQUEST_NULL, length(nbproc))
    for i = 1:length(nbproc)
        if mc.nmpisides_rec[i,sendid] > 0
            range      = offsets[i,sendid] + 1:offsets[i + 1,sendid]
            request[i] = MPI.Irecv!(view(data, :,range), nbproc[i], 0, MPI.COMM_WORLD)
        end
    end
    request
end

"""
Function that performs the send operations for the face data that has to be exchanged between processors.
"""
function sendmpi(data, sendid, mc)
    nbproc, offsets = mc.nbproc, mc.offsetmpisides_send
    #mprintln("send")
    #serial() do
    #    iprintln("nbproc")
    #    iprintln(nbproc[:])
    #    iprintln("offsets")
    #    iprintln(offsets[:])
    #    iprintln("sidessend")
    #    iprintln(mc.nmpisides_send[:])
    #end
    request = fill(MPI.REQUEST_NULL, length(nbproc))
    for i = 1:length(nbproc)
        if mc.nmpisides_send[i,sendid] > 0
            range      = offsets[i,sendid] + 1:offsets[i + 1,sendid]
            request[i] = MPI.Isend(view(data, :, range), nbproc[i], 0, MPI.COMM_WORLD)
        end
    end
    request
end

end
