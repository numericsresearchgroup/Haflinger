@reexport module Analyze

using ..Haflinger

export AnalyzeBasis, analyze!

"""
Initializes variables necessary for analyze subroutines
- provides basic quantities like global domain volume, surface area of boundary conditions
  or precomputed surface and volume integration weights
- initializes other specific analysis and benchmarking routines
"""
function initanalyze(params)
    initstart("ANALYZE")
    # Check if all the necessary initialization is done before
    if !interpolationinitisdone || !meshinitisdone
      safeerror("initanalyze not ready to be called.")
    end
    # Get the various analysis/output variables
    global docalcerrornorms = getparameter(params,"docalcerrornorms"; required=false, default=true )
    global doanalyzetofile  = getparameter(params,"doanalyzetofile" ; required=false, default=false )
    global analyzeexactfunc = getparameter(params,"analyzeexactfunc"; required=false, default=iniexactfunc )
    global analyzerefstate  = getparameter(params,"analyzerefstate" ; required=false, default=inirefstate )
    global NA               = getparameter(params,"nanalyze"        ; required=false, default=2N )
    NAZ = DIM == 3 ? NA : 1

    if doanalyzetofile
        merror("outputtofile init missing")
        varnames = [ ["L2_$(strvarnames[i])"   for i=1:NAR];
                     ["LInf_$(strvarnames[i])" for i=1:NAR];
                     ["timesteps","t_CPU","DOF","Ncells","nProcs"] ]
        #initoutputtofile(filename_errnorm,"Analyze",varnames,lastline)
        #iterrestart     = max(lastline(2*NVAR+1),0.)
        #calctimerestart = max(lastline(2*NVAR+2),0.)
    end

    # only computed supersampled geometry terms if required by some routines
    withgeo = (docalcerrornorms)
    # precompute integration weights, surface areas, volumes...
    global ab = AnalyzeBasis(NA, calc.dg.ip, mesh.vol.sjac, mesh.surf.surfelem, mesh.vol.xgp, withgeo)

    #initanalyzeequation()
    #initbenchmarking()

    analyzeinitisdone=true
    mprintln("Volume of computational domain : $(ab.vol)")
    initdone("ANALYZE")
end

struct AnalyzeBasis{N,NA,T}
  # Interpolation bases of with N and NA
  ip       ::IPBasis{N ,T}
  ipna     ::IPBasis{NA,T}
  # products of integration weights for volume / surf for N and NA
  wgpsurf  ::SMatrix{N,N,T}
  wgpvol   ::SArray{Tuple{N,N,N},T}
  wgpsurfna::SMatrix{NA,NA,T}
  wgpvolna ::SArray{Tuple{NA,NA,NA},T}

  vdm_n_na ::SMatrix{NA,N,T}      # Vandermonde from N to NA (projection of NA<N)

  elemvol  ::Vector{T}            # volume of each element
  vol      ::T                    # volume of domain
  surf     ::Vector{T}            # area of each sides
  #bcsurf   ::Vector{T}            # area of bcs sorted by type
  xgpna    ::Array{T}             # Gauss point positions supersampled to NA
  jacna    ::Array{T}             # Jacobian supersapled to NA
end
function AnalyzeBasis(NA::Integer, ip::IPBasis{N,T}, sjac::AbstractArray{T,DIM+2},
                                            surfelem::AbstractArray{T,DIM+1},
                                            xgp::AbstractArray{T,DIM+2}, withgeo) where {N,T}
    ipna = IPBasis{NA,T}(:nodetypeg)

    # precompute integration weight products
    wgp = ip.wgp
    wgpsurf = zeros(T,N,N)
    wgpvol  = zeros(T,N,N,N)
    for j=1:N, i=1:N
      wgpsurf[i,j] = wgp[i]*wgp[j]
    end
    for k=1:N, j=1:N, i=1:N
      wgpvol[i,j,k] = wgp[i]*wgp[j]*wgp[k]
    end

    wgpna = ipna.wgp
    wgpsurfna = zeros(T,NA,NA)
    wgpvolna  = zeros(T,NA,NA,NA)
    for j=1:NA, i=1:NA
      wgpsurfna[i,j] = wgpna[i]*wgpna[j]
    end
    for k=1:NA, j=1:NA, i=1:NA
      wgpvolna[i,j,k] = wgpna[i]*wgpna[j]*wgpna[k]
    end

    # precompute volume of the domain
    nelems=size(sjac)[end]
    elemvol = zeros(T,nelems)
    @inbounds for ielem=1:nelems
      for k=1:N, j=1:N, i=1:N
        elemvol[ielem] += wgpvol[i,j,k]/sjac[1,i,j,k,ielem]
      end
    end
    vol=sum(elemvol)

    # compute surface of each boundary and sum over BCs
    nsides = size(surfelem)[end]
    surf = zeros(T,nsides)
    @inbounds for iside=1:nsides
      for j=1:N, i=1:N
        surf[iside] += wgpsurf[i,j] * surfelem[1,i,j,iside]
      end
    end

    vdm_n_na = getvandermonde(N, ip.nodetype, NA, ipna.nodetype; projection = true)
    ##bcsurf = zeros(T,nbcs)
    ##for iside=1:nsides
    ##  isurf = analyzeside[iside]
    ##  if isurf ≠ 0
    ##    bcsurf[isurf] += surf[iside]
    ##  end
    ##end do

    if USEMPI
        volloc = vol
        vol  = MPI.Allreduce( volloc , + , MPI.COMM_WORLD )
        #bcsurf = MPI.Allreduce( bcsurfloc, + , MPI.COMM_WORLD )
    end
    if withgeo
        xgpna  = zeros(T, DIM, NA, NA, NA, nelems)
        jacna  = zeros(T, 1  , NA, NA, NA, nelems)
        xgptmp = zeros(T, DIM, N , N , N         )
        jactmp = zeros(T, 1  , N , N , N         )
        @views for ielem=1:nelems
          jel     = sjac[:,:,:,:,ielem]
          jactmp .=  1 ./ jel
          xgptmp .= xgp[:,:,:,:,ielem]
          changebasisvolume(vdm_n_na, xgptmp, xgpna[:,:,:,:,ielem])
          changebasisvolume(vdm_n_na, jactmp, jacna[:,:,:,:,ielem])
        end
    else
        xgpna=Array{T}()
        jacna=Array{T}()
    end

    AnalyzeBasis{N,NA,T}(ip,  ipna,
                         SMatrix{N, N }(wgpsurf),   SArray{Tuple{N ,N ,N }}(wgpvol),
                         SMatrix{NA,NA}(wgpsurfna), SArray{Tuple{NA,NA,NA}}(wgpvolna),
                         vdm_n_na, elemvol, vol, surf, xgpna, jacna)
end

"""
Controls analysis routines and is called at analyze time levels
- calls generic error norm computation
- calls equation system specific analysis
"""
function analyze!(time, iter)
    # Calculate error norms
    if docalcerrornorms
      l2_error, linf_error = calcerrornorms(time, ab, calc.u.vol)
      if MPIROOT
        println(" L_2        : $(formatarr(l2_error)...)")
        println(" L_Inf      : $(formatarr(linf_error)...)")
        if doanalyzetofile
            outputtofile("out.$projectname", time, l2_error, linf_error,
                         iter + iterrestart, runtime + calctimerestart,
                         nglobalelems*n^3, nglobalelems, nprocessors)
        end
      end
    end

    #analyzeequation(time)
    #benchmarking()
end
@inline formatarr(arr) = [(@sprintf " %1.8e " arr[i]) for i=1:length(arr)]

"""
Calculates L2 and Linf error norms of state variables using the prescribed exact solution
"""
function calcerrornorms(time, ab::AnalyzeBasis{N,NA,T}, u::AbstractArray{T,DIM+2}) where {N,NA,T}
    nelems, ndofs = size(u,DIM+2), NA^3
    l2_error, linf_error  = zeros(T, NVAR), fill(T(-1e10), NVAR)
    una = zeros(T, NVAR, NA, NA, NA); una2 = r2(una)
    uex = similar(una2)
    jac, xgp = rd(ab.jacna), rd(ab.xgpna)
    wgp, wgpjac = reshape(ab.wgpvolna, :), zeros(T,ndofs)

    @views @inbounds for ielem=1:nelems
        changebasisvolume(ab.vdm_n_na,u[:,:,:,:,ielem],una) # Interpolate the solution to the analyze grid
        for i=1:ndofs
            exactfunc(uex[:,i], xgp[:,i,ielem], time)
        end
        uex    .= abs.(una2 .- uex)
        wgpjac .= jac[1,:,ielem] .* wgp
        for i=1:ndofs
            linf_error .= max.(linf_error, uex[:,i])
            # To sum over the elements, We compute here the square of the L_2 error
            l2_error  .+= uex[:,i].^2 .* wgpjac[i]
        end
    end

    if USEMPI
        l2, linf   = l2_error, linf_error
        l2_error   = MPI.Reduce(l2  , +   , 0 , MPI.COMM_WORLD)
        linf_error = MPI.Reduce(linf, max , 0 , MPI.COMM_WORLD)
    end
    # We normalize the L_2 Error with the Volume of the domain and take into account that we have to use the square root
    if MPIROOT
        l2_error .= sqrt.(l2_error ./ ab.vol)
    end

    l2_error, linf_error
end

end
