@reexport module IO_HDF5

using  ..Haflinger
using  HDF5
import HDF5.Hmpih
import Base.convert

export initio_hdf5, openfile, h5comm, writestate
export STATEDATA

const STATEDATA = Array{Tuple{String, Array{String}, Any},1}()


const H5MPIH = HDF5.mpihandles[sizeof(MPI.CComm)]
H5MPIH(c::MPI.CComm) = reinterpret(H5MPIH, c)
H5MPIH(i::MPI.CInfo) = reinterpret(H5MPIH, i)
H5MPIH(c::MPI.Comm)  = reinterpret(H5MPIH, MPI.CComm(c))
H5MPIH(i::MPI.Info)  = reinterpret(H5MPIH, MPI.CInfo(i))

"""
Intialize HDF5 IO (mainly the MPI related parts)
"""
function initio_hdf5(params)
    # Create type to store file system specific IO information
    if USEMPI
        global H5CCOMM, H5COMM_NODE, H5COMM_LEADERS
        H5CCOMM, H5CCOMM_NODE, H5CCOMM_LEADERS = H5MPIH.([MPI.COMM_WORLD, MPI_COMM_NODE, MPI_COMM_LEADERS])
    end
    global DXPL_MPIO       = USEMPI ? p_create(HDF5.H5P_DATASET_XFER, false, "dxpl_mpio", HDF5.H5FD_MPIO_COLLECTIVE)  : HDF5.DEFAULT_PROPERTIES

    global dogatheredwrite = USEMPI ? getparameter(params, "dogatheredwrite" ; required=false, default=false )     : false
    global filesystem      = USEMPI ? getparameter(params, "filesystem"      ; required=false, default="default" ) : ""

    # Lustre file systems
    if filesystem=="lustre"
        # Disables ROMIO's data-sieving
        MPI.Info_set(info, "romio_ds_read", "disable")
        MPI.Info_set(info, "romio_ds_write","disable")
        # ROMIO buffering of collectives
        MPI.Info_set(info, "romio_cb_read", "enable")
   end
end

userblockh5size(size)=512*ceil(size/512) # userblock size is multiple of 512 bytes

"""
Open HDF5 file with with different communicators and in parallel
 - "r"  read only
 - "r+" read and write
 - "cw" read and write, create file if not existing, do not truncate
 - "w"  read and write, create a new file (destroys any existing contents)
"""
function openfile(name::String; mode::String="cw", mpio::Bool=true, comm::MPI.Comm = MPI.COMM_WORLD, userblocksize = 0)
    props=Array{Any,1}()
    if mode in ["w","cw"] && userblocksize > 0
        push!(props, "userblock", userblockh5size(userblocksize))
    end
    if USEMPI && mpio
        ccomm = H5MPIH(comm)
        cinfo = H5MPIH(MPI.Info())
        push!(props, "fapl_mpio", (ccomm, cinfo))
    end
    f=h5open(name, mode, props...)
end
function openfile(fn::Function, name::String ; args...)
    f = openfile(name; args...)
    try
        fn(f)
    finally
        close(f)
    end
end
openfile(name::String, mode::String, mpio::Bool, comm::MPI.Comm,  userblocksize = 0 ) = openfile(name, mode, mpio, H5MPIH(comm), userblocksize )
openfile(name::String, mode::String, mpio::Bool, comm::MPI.CComm, userblocksize = 0 ) = openfile(name, mode, mpio, H5MPIH(comm), userblocksize )

include("hdf5_input.jl")
include("hdf5_output.jl")

function finalizeio_hdf5()
    
end

end
