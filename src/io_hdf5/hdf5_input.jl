export dorestart, restarttime

global dorestart   = false
global restarttime = -1.
"""
Initialize all necessary information to perform the restart.
The routine checks if the restart file is valid and checks wheter the size of the dataset is
consistent with the present mesh. We also check, if the node type and polynomial degree of the
restart file is the same than in the current simulation.
"""
function initrestart(params, restartfile="")

    global restarttime = getparameter(params, "restarttime" ; required=false, default=-1.)
    if !isfile(restartfile)
        return
    end
    initstart("RESTART")
    mprintln("Restarting from file $restartfile")
    if !isvalidsolution(restartfile)
        safeerror("Restart file is not a valid state file!")
    end
    global dorestart = true
    openfile(restartfile; mode="r", mpio=true) do f
        restartdims = size(f["DG_Solution"])
        @assert restartdims[1]   == NVAR         "Restart file has different number of variables!"
        @assert restartdims[end] == nglobalelems "Restart file has different number of elements!"
        nrestart = restartdims[2]
        if restarttime < 0
            restarttime = attrs(f)["t"]
        end
        nodetyperestart = attrs(f)["NodeType"]
    end
    if (n ≠ nrestart || Nodetype ≠ nodetyperestart) && min(n, nrestart) < ngeo
      mwarn("The geometry is or was underresolved (N < NGeo) and may potentially change on restart!")
    end

    initdone("RESTART")
end

function isvalidsolution(file)
    #openfile(file; mode="r", mpio=false) do f
    #end
   true
end

"""
Perform the actual restart. Read the state from the file and interpolate it to the current polynomial
degree or node set, if any of these have changed.
TODO: Read additional arrays present in state file.
"""
function restart(restartfile)
    if !dorestart
        return
    end
    initstart("RESTART")
    openfile(restartfile; mode="r", mpio=true) do f
        urestart = f["DG_Solution"][:,:,:,:,(myelemoffset+1):(myelemoffset+nelems)]
    end
    #if (n ≠ nrestart || NODETYPE ≠ nodetyperestart)
    #    mprintln("Interpolating solution from restart grid with N=',N_restart,' to computational grid with N=$N")
    #    vdmnrestart = getvandermonde(nrestart, nodetyperestart, n, nodetype; projection=true)
    #    getvandermonde(nrestart, nodetyperestart, n, nodetype; projection=true)
    #    changebasisvolume(vdmnrestart, urestart, calc.u.vol )
    #else
    #    calc.u.vol = urestart
    #end
    initdone("RESTART")
end
