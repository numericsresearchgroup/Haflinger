"""
Write statefile in a Flexi compatbile way
"""
function writestate(calc, t; outputdata::Array{Tuple{String, Array{String}, Any},1}=STATEDATA , iserrorfile = false)
    filetype = iserrorfile ? "_ERROR_State_" : "_State_"
    filename = projectname * filetype * (@sprintf "%017.9f" t) * ".h5"

    # Create the skeleton and the basics (only MPIROOT)
    @root openfile(filename; mode="w", mpio=false) do f
        createskeleton(f, calc, t, "State")
        for (name, vars, data) in outputdata
            adddataset(f, name, vars, data)
        end
    end
    # Reopen in parallel, write the actual data
    tloc = mpitime()
    openfile(filename; mode="r+", mpio=true) do f
        for (name, vars, data) in outputdata
            dset = d_open(f, name)#, HDF5.DEFAULT_PROPERTIES, DXPL_MPIO )
            dset[:,:,:,:,(myelemoffset+1):(myelemoffset+nelems)] = data
        end
        attrs(f)["Timestamp"] = tloc # write time attrib last, as marker that all data has been written
    end
    # TODO: write additional field and and element-wise data
end

"""
This routine is a wrapper routine for WriteArray and first gathers all output arrays of an MPI sub group,
then only the master will write the data. Significantly reduces IO overhead for a large number of processes!
TODO: The parallel performance of metadata IO has vastly improved with HDF5 1.10.2.
      Check whether this is still necessary.
"""
function gatheredwrite(u, datasetname)
    ug = MPI.Gather(u, MPIROOT, MPI_COMM_NODE)
    if MPILOCALROOT
        openfile(filename; mode="r+", mpio=true, comm=H5CCOMM_LEADERS, info=H5CINFO) do f
            dset = d_open(f, datasetname, HDF5.DEFAULT_PROPERTIES, DXPL_MPIO )
            dset[:,:,:,:,(myelemoffset+1):(myelemoffset+nelems)] = u
        end
    end
end

"""
Create the output file and fill with standard properties on a single process
TODO: The parallel performance of metadata IO has vastly improved with HDF5 1.10.2.
      Check whether this is still necessary.
"""
function createskeleton(f, calc, t, filetype)
    attrs(f)["Program"] = "Haflinger"
    attrs(f)["File_Type"] = filetype
    attrs(f)["Project_Name"] = projectname
    attrs(f)["File_Version"] = 0.1
    attrs(f)["NodeType"] = uppercase(Interpolation.nodetypestr)
    attrs(f)["N"] = N
    attrs(f)["Time"] = t
    attrs(f)["MeshMode"] = Mesh.meshmode
    attrs(f)["useCurveds"] = Mesh.usecurveds ? 1 : 0 # HDF5 does not support bool attributes
    attrs(f)["Meshfile"] = Mesh.meshfile
    attrs(f)["Dimension"] = 3
end
function adddataset(f, dsetname::String, dsetvarnames, dsetdata; globalsize=nglobalelems)
    attrs(f)["VarNames_$dsetname"] = dsetvarnames
    @show "geht noc"
    dset = d_create(f, dsetname, datatype(eltype(dsetdata)), dataspace(size(dsetdata)[1:ndims(dsetdata)-1]..., globalsize))
    @show "geht noc2"
end

"""
Remove files present from previous simulations with a timestamp larger than the current time
"""
function flushfiles(projectname, filetype, flushtime = 0.)
    println(" DELETING OLD HDF5 FILES...")
    rfile = Regex("^$(projectname)_$(filetype)_[0-9]{7}.[0-9]{9}.h5")
    rtime = Regex("[0-9]{7}.[0-9]{9}")
    tmp   = readdir()
    files = tmp[isfile.(tmp)]
    states = filter(x -> match(rfile, x) ≠ nothing , files)
    for f in states
        if parse(match(rtime, f).match) ≥ flushtime
            rm(f)
        end
    end
    println(" ...DONE")
end
